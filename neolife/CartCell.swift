//
//  CartCell.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/03/02.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//

import UIKit

protocol CartCellDelegate {
    func didAdd(sender : UIButton)
    func didSubtract(sender : UIButton)
}

class CartCell: UITableViewCell {
    
    var delegate : CartCellDelegate?
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var prodName: UILabel!
    @IBOutlet weak var prodType: UILabel!
//    @IBOutlet weak var numItems: UITextField!
    
    @IBOutlet weak var numberOfItems: UITextField!
    @IBOutlet weak var itemNum: UILabel!
    @IBOutlet weak var prodPriceLbl: UILabel!
    @IBOutlet weak var unitPriceLbl: UILabel!
    @IBOutlet weak var addQuantityButton: UIButton!
    @IBOutlet weak var subtractQuantityButton: UIButton!
    
    

    @IBAction func addButton(_ sender: UIButton) {
        delegate?.didAdd(sender: sender)
    }
    
    @IBAction func subtractButton(_ sender: UIButton) {
        delegate?.didSubtract(sender: sender)
    }
    
    
}
