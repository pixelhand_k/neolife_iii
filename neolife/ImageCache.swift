//
//  ApiService.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/06/22.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//

import UIKit

let imagCache = NSCache<AnyObject, AnyObject>()

class APIService {
    
    var imageUrlString : String?
    let imageCache = NSCache<AnyObject, AnyObject>()
    
    func loadImages(_ urlString: String?, imageView: UIImageView){
        
        imageUrlString = urlString!
        
        imageView.image = #imageLiteral(resourceName: "neolifeLogo")
        
        if let imageFromCache = imageCache.object(forKey: urlString! as AnyObject) as? UIImage {
            imageView.image = imageFromCache
            return
        }
        
        let url = URL(string: urlString!)
        let task = URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            
            if error != nil{
                print("Error: \(String(describing: error)), \(String(describing: error?.localizedDescription))")
            } else {
                
                //get it back onto main queue
                DispatchQueue.main.async {
                    if let imageToCache = UIImage(data: data!) {
                        self.imageCache.setObject(imageToCache, forKey: urlString as AnyObject)
                        imageView.image = imageToCache
                    }
                }
            }
        })
        
        task.resume()
    }
}

extension UIImageView {
    /// Loads image from cache. First check if the image exist in cache, if it does, it places that image in the image propery.
    /// Otherwise it uses the param to download the image and places it in the image property and stores url in the cache
    /// - Parameter url: Image URL
    func loadImageFromCacheWithUrl(_ url: String) {
        self.image = #imageLiteral(resourceName: "neolifeLogo")
        
        //check if image is cached first
        if let cachedImage = imagCache.object(forKey: url as AnyObject) as? UIImage {
            self.image = cachedImage
            return
        }
        
        let imageUrl = URL(string: url)
        URLSession.shared.dataTask(with: imageUrl!) { (data, response, error) in
            if error != nil {
                print(error!)
                return
            }
            
            DispatchQueue.main.async {
                if let imageData = UIImage(data: data!) {
                    imagCache.setObject(imageData, forKey: url as AnyObject) // adds image to cache
                    self.image = imageData
                }
            }
            }.resume()
    }
}

