//
//  SubMenuCell.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/03/21.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//

import UIKit

class SubMenuCell: UICollectionViewCell {
    @IBOutlet weak var subMenuImage: UIImageView!
    @IBOutlet weak var subMenuLbl: UILabel!
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    
//    var subCategory : Catalog? {
//        didSet{
//            self.subMenuLbl.text = Catalog().catName
//        }
//    }
}
