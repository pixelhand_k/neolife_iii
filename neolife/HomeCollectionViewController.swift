//
//  HomeCollectionViewController.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/02/24.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//

import UIKit
import Crashlytics
import paper_onboarding
import Firebase

//private let reuseIdentifier = "cell"

class HomeCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var promo = [PromotionItem(title: "Pro Vitality", coverImage: #imageLiteral(resourceName: "Pro Vitality (1)")),
                 PromotionItem(title: "Active People", coverImage: #imageLiteral(resourceName: "Active_people_copy")),
                 PromotionItem(title: "Children Nutrition", coverImage: #imageLiteral(resourceName: "Children_Nutrition")),
                 PromotionItem(title: "Formula IV Plus", coverImage: #imageLiteral(resourceName: "Formula_IV_Plus_copy")),
                 PromotionItem(title: "Home Care", coverImage: #imageLiteral(resourceName: "Homecare-1")),
                 PromotionItem(title: "Be Your Best", coverImage: #imageLiteral(resourceName: "Be Your Best")),
                 PromotionItem(title: "Shake Party", coverImage: #imageLiteral(resourceName: "Shake_Party_copy"))]
    
    
    @IBOutlet weak var profileBtn: UIBarButtonItem!
    
    var isUserMember = Bool()
    var idNumber = Int()
    var isFirstTime = false
    let cartBadge = CartBadge()
    
    let notificationCenter = NotificationCenter.default
    
//    MARK: - View Functions
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        cartBadge.controller = self
        cartBadge.countString = "\(cartBadge.cartCount())"
        cartBadge.segueId = ""
        cartBadge.addBadgeToBtn()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView?.allowsMultipleSelection = false
        self.navigationItem.titleView = UIImageView(image: #imageLiteral(resourceName: "neolife_logo"))
        
        notificationCenter.addObserver(self, selector: #selector(appMovedToForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        DispatchQueue.main.async {
            self.getAppStoreVersion()
        }

        userLoggedIn()

    }
    
    @objc func appMovedToForeground() {
        DispatchQueue.main.async {
            self.getAppStoreVersion()
        }
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return promoItems.count
        return promo.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PromotionCollectionViewCell
        
        cell.promoImageView.image = promo[indexPath.row].coverImage
        cell.titleLbl.text = promo[indexPath.row].title
        
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOpacity = 1.0
        cell.layer.shadowRadius = 2.0
        cell.layer.shadowOffset = CGSize.zero
        cell.layer.shouldRasterize = false
        cell.layer.masksToBounds = false
        cell.backgroundColor = UIColor.white
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: view.frame.width - 10, height: 180)
        return CellStructure().homeCollectionCellSize(collectionView)
    }
    
//    MARK: - Actions
    @IBAction func profileActionBtn(_ sender: Any) {
        //add if statement to check if user is guest or member/distributer
        let neolifeID = UserFunctions.getNeolifeId()
        
        if Auth.auth().currentUser != nil {
            
            if neolifeID != "" {
                let ref = Database.database().reference(fromURL: "https://neolife-d9754.firebaseio.com/")
                ref.child("users").child(neolifeID).observe(.value, with: { (snap) in
                    if let value = snap.value as? [String : AnyObject] {
                        let distributor = value["is_distributor"] as? Bool
                        //                    let member = value["is_member"] as? Bool
                        
                        if distributor == false {
                            // redeem distributor here
                            
                        } else {
                            self.performSegue(withIdentifier: "profileSegue", sender: nil)
                        }
                    }
                }, withCancel: nil)
            } else {
                Alerts.idAlert(on: self)
            }
        } else {
            //user is guest
            Alerts.alert(on: self, title: "Register with NeoLife", and: "Register to be a NeoLife Distributor on www.shopneolife.com.")
        }
    }
    
    // MARK: UICollectionViewDelegate
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let sb = UIStoryboard(name: "Product", bundle: nil)
        
        switch indexPath.row {
        case 0:
            guard let vc = sb.instantiateViewController(withIdentifier: "product_detail") as? MoreDetailTVC else {
                return
            }
            vc.productTitle = "Pro Vitality"
            vc.productCode = "2595"
            vc.isFromHome = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        case 1:
            guard let vc = sb.instantiateViewController(withIdentifier: "product_category") as? ProductVC else {
                return
            }
            vc.categoryId = "energy_fitness"
            vc.categoryName = "Energy & Fitness"
            vc.isFromHome = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        case 2:
            guard let vc = sb.instantiateViewController(withIdentifier: "product_category") as? ProductVC else {
                return
            }
            vc.categoryId = "children_health"
            vc.categoryName = "Childrens Health"
            vc.isFromHome = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        case 3:
            guard let vc = UIStoryboard(name: "Product", bundle: nil).instantiateViewController(withIdentifier: "product_detail") as? MoreDetailTVC else {
                print("didnt work")
                return
            }
            vc.productTitle = "Formula IV Plus (60 Sachets)"
            vc.productCode = "2557"
            vc.isFromHome = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        case 4:
            guard let vc = UIStoryboard(name: "Product", bundle: nil).instantiateViewController(withIdentifier: "product_category") as? ProductVC else {
                print("didnt work")
                return
            }
            vc.categoryId = "home_care"
            vc.categoryName = "Home Care"
            vc.isFromHome = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        case 5:
            guard let vc = UIStoryboard(name: "Product", bundle: nil).instantiateViewController(withIdentifier: "pdfViewer") as? PDFViewerVC else {
                print("didnt work")
                return
            }
            vc.title = "Be Your Best Challange"
//            vc.pdfContent = "BYB challenge flyer 2017 (LR)"
            vc.pdfContent = "BYB"
            self.navigationController?.pushViewController(vc, animated: true)
            
        case 6:
            guard let vc = UIStoryboard(name: "Product", bundle: nil).instantiateViewController(withIdentifier: "pdfViewer") as? PDFViewerVC else {
                print("didnt work")
                return
            }
            vc.title = "Shake Party"
            vc.pdfContent = "NeoLifeClub ShakeParty Manual (LR)"
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            Alerts.alert(on: self, title: "Not available", and: "This offer has expired!")
        }
    }
    
    
//    MARK: - User Functions
    
    func isFirstTimeIn() -> Bool {
        if (!UserDefaults.standard.bool(forKey: "firstTimeIn")) {
            UserDefaults.standard.set(true, forKey: "firstTimeIn")
            UserDefaults.standard.synchronize()
            
            Alerts.alertWithImage(on: self, title: "You're new! Welcome", and: "Things you need to know, to be able to make purchases as a member or distributor, make sure that you have an active email account in your settings. This can be found in 'Settings > Accounts & Passwords'")
            return true
        }
        
        return false
    }
    
    func userLoggedIn() {
        if Auth.auth().currentUser != nil {
            let neolifeID = UserFunctions.getNeolifeId()
            self.priceAlert()
            //            print("NEOLIFE USER: \(String(describing: Auth.auth().currentUser?.email))")
            if neolifeID == "" {
                Alerts.idAlert(on: self)
            }
        } else {
            print("No user")
        }
    }
    
//    MARK: - Alerts
    func priceAlert() {
        if (!UserDefaults.standard.bool(forKey: "priceAlertedUser")) {
            UserDefaults.standard.set(true, forKey: "priceAlertedUser")
            UserDefaults.standard.synchronize()
            
//            Alerts.alert(self, "FREE DELIVERY!", "All orders R1000 and above qualify for free delivery", nil)
            Alerts.alert(on: self, title: "FREE DELIVERY!", and: "All orders R1000 and above qualify for free delivery.")
        }
    }
    
    
    func getAppStoreVersion() {
        let appVersionKey = "CFBundleShortVersionString"
        let currentVersion = Bundle.main.infoDictionary![appVersionKey] as! String
        
        let itunesLink = AppDelegate().itunesAppLink
        guard let appstoreURL = URL(string: itunesLink) else {
            return
        }
        
        DispatchQueue.main.async {
            AppDelegate().getAppStoreVersion { appStoreVersion in
              
                DispatchQueue.main.async {
                    if currentVersion < appStoreVersion {
                        Alerts.alert(on: self, message: "Press OK to update app in order to continue using the app", title: "App out of date", with: {
                            UIApplication.shared.open(appstoreURL, options: [:], completionHandler: nil)
                        })
                    }
                }
            }
        }
    }
}










