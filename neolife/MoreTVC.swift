//
//  MoreTVC.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/01/20.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//


import UIKit
import Parse
import Firebase

class MoreTVC: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.titleView = UIImageView(image: #imageLiteral(resourceName: "neolife_logo"))
        tableView.tableFooterView = UIView()
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func logoutBtn(_ sender: Any) {
        print("logout")
        guard let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginVC") as? ViewController else {
            print("didnt work")
            return
        }
        
        let firebaseAuth = Auth.auth()
        
        do {
            try firebaseAuth.signOut()
            
        } catch let error as NSError {
            print("Error signing out: \(error.localizedDescription)")
        }
        
        self.present(vc, animated: true, completion: nil)
        /*self.present(vc, animated: true) {
//            PFUser.logOut()
//            let firebaseAuth = Auth.auth()
//            do {
//                try firebaseAuth.signOut()
//            } catch let signOutError as NSError {
//                print ("Error signing out: %@", signOutError)
//            }
        }*/
    }
    
    @IBAction func appInfoAction(_ sender: UIBarButtonItem) {
        Alerts.alert(on: self, title: "Current Version", and:  "\(AppDelegate.getCurrentAppVersion())")
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 6
    }


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
//        let indexPath = IndexPath()
        
        if segue.identifier == "aboutSegue" {
            let indexpath = tableView.indexPathForSelectedRow
            let cell = tableView.cellForRow(at: indexpath!)
            let avc : AboutVC = segue.destination as! AboutVC
            
//            print("CEll: \(cell?.textLabel?.text)")
            
            avc.title = cell?.textLabel?.text
            
        }
    }


}
