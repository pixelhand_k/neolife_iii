//
//  CartViewController+.swift
//  neolife
//
//  Created by Karabo Moloi on 2018/02/13.
//  Copyright © 2018 Karabo Moloi. All rights reserved.
//

import UIKit
import Firebase
import Crashlytics

extension CartViewController {
    
//    MARK: - Checkout process functions
    
    /// When user presses the chekout button, this method is called. it presents two options, online payment or bank deposit.
    /// Online payment runs the Payment gateway SDK and the bank deposit option segues to another controller as part of the checkout process
    /// - Parameter total: the total user is going to pay, passed to the Payment gateway parameter
    func paymentMethodActionSheet(_ total: String) {
        let alertController = UIAlertController(title: nil, message: "Select payment method", preferredStyle: .actionSheet)

        let indicator = Loading.init(fromView: self)

        let credit = UIAlertAction(title: "Online Payment", style: .default, handler: {action in
            if Auth.auth().currentUser != nil {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
                indicator.createIndicator()
//                indicator.showIndicator()
                Request.requestCheckoutID(controller: self, amount: total, email: self.userEmail, name: self.userName + self.userSurname, neolifeId: self.idNumber, invoiceNum: /*self.generateInvoiceNum()*/self.invoiceNumber, completion: { (checkoutID) in
                    CLSLogv("%@", getVaList(["CheckoutID error",total, self.userEmail, self.userName, self.userSurname, self.idNumber, self.invoiceNumber]))
                    DispatchQueue.main.async {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        indicator.hideIndicator()
                        guard let checkoutID = checkoutID else {
                            Alerts.alert(on: self, title: "Failed", and: "Checkout ID is empty")
                            return
                        }
                        
                        print("CheckoutID: \(String(describing: checkoutID))")
                        
                        self.checkoutID = checkoutID
                        self.checkoutProvider = self.configureCheckoutProvider(checkoutID: checkoutID)
                        self.checkoutProvider?.presentCheckout(forSubmittingTransactionCompletionHandler: { (transaction, error) in
                            if error != nil {
                                print("Error over here: \(String(describing: error?.localizedDescription))")
                                CLSLogv("Payment error: %@", getVaList([error.debugDescription]))
                            }
                            DispatchQueue.main.async {
//                                print("Transaction: \(String(describing: transaction))")
                                self.handleTransactionSubmission(transaction: transaction, error: error)
                            }
                            //                            self.handleTransactionSubmission(transaction: transaction, error: error)
                        }, paymentBrandSelectedHandler: nil, cancelHandler: nil) // cancel handler is fired when user decides to cancel transaction
                    }
                })
            } else {
                Alerts.alert(on: self, title: "Login", and: "Please login to complete the order")
            }
        })
                
        let deposit = UIAlertAction(title: "Bank Deposit", style: .default, handler: {action in
            if Auth.auth().currentUser != nil {
//                self.bagItems()
                
                //ask for location
                let cell = self.totalTableView.cellForRow(at: self.indexPath) as! TotalCell
                if cell.pickSwitch.isOn {
                    self.addressSelectionActionSheet(segueID: "depositSegue")
                } else {
                    self.deliveryAddressAlert(self, segueID: "depositSegue")
                    //                    self.performSegue(withIdentifier: "depositSegue", sender: self.deliveryAddressAlert(self)) // add user address here, create function
                }

            } else {
                Alerts.alert(on: self, title: "Login", and: "Please login to complete the order")
            }
            
        })
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        
        //        alertController.addAction(eft)
        alertController.addAction(credit)
        alertController.addAction(deposit)
        alertController.addAction(cancel)
        alertController.view.tintColor = UIColor.greenNeolife()
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func addressSelectionActionSheet(segueID: String){
        let alertController = UIAlertController(title: "Select pick up location", message: "Which Distributor Centre would you like to pick up your items?", preferredStyle: .actionSheet)
        
        let jhb = UIAlertAction(title: "JOHANNESBURG", style: .default, handler: { action in
            
            self.performSegue(withIdentifier: segueID, sender: "Johannesburg Office")
        })
        
        let capeTown = UIAlertAction(title: "CAPE TOWN", style: .default, handler: { action in
            
            self.performSegue(withIdentifier: segueID, sender: "Cape Town Office")
        })
        
        let durban = UIAlertAction(title: "DURBAN", style: .default, handler: { action in
            
            self.performSegue(withIdentifier: segueID, sender: "Durban Office")
        })
        
        let bethlehem = UIAlertAction(title: "SKYNET BETHLEHEM", style: .default, handler: { action in
            
            self.performSegue(withIdentifier: segueID, sender: "BETHLEHEM")
        })
        
        let bloem = UIAlertAction(title: "SKYNET BLOEMFONTEIN", style: .default, handler: { action in
            
            self.performSegue(withIdentifier: segueID, sender: "BLOEMFONTEIN")
        })
        
        let george = UIAlertAction(title: "SKYNET GEORGE", style: .default, handler: { action in
            
            self.performSegue(withIdentifier: segueID, sender: "GEORGE")
        })
        
        let grahamstown = UIAlertAction(title: "SKYNET GRAHAMSTOWN", style: .default, handler: { action in
            
            self.performSegue(withIdentifier: segueID, sender: "GRAHAMSTOWN")
        })
        
        let kimberly = UIAlertAction(title: "SKYNET KIMBERLEY", style: .default, handler: { action in
            
            self.performSegue(withIdentifier: segueID, sender: "KIMBERLEY")
        })
        
        let klerksdorp = UIAlertAction(title: "SKYNET KLERKSDORP", style: .default, handler: { action in
            
            self.performSegue(withIdentifier: segueID, sender: "KLERKSDORP")
        })
        
        let ladysmith = UIAlertAction(title: "SKYNET LADYSMITH HUB", style: .default, handler: { action in
            
            self.performSegue(withIdentifier: segueID, sender: "LADYSMITH")
        })
        
        let nelspruit = UIAlertAction(title: "SKYNET NELSPRUIT", style: .default, handler: { action in
            
            self.performSegue(withIdentifier: segueID, sender: "NELSPRUIT")
        })
        
        let newcastle = UIAlertAction(title: "SKYNET NEWCASTLE", style: .default, handler: { action in
            
            self.performSegue(withIdentifier: segueID, sender: "NEWCASTLE")
        })
        
        let polokwane = UIAlertAction(title: "SKYNET POLOKWANE", style: .default, handler: { action in
            
            self.performSegue(withIdentifier: segueID, sender: "POLOKWANE")
        })
        
        let pietermaritzburg = UIAlertAction(title: "SKYNET PIETERMARITZBURG", style: .default, handler: { action in
            
            self.performSegue(withIdentifier: segueID, sender: "PIETERMARITZBURG")
        })
        
        let pe = UIAlertAction(title: "SKYNET PE", style: .default, handler: { action in
            
            self.performSegue(withIdentifier: segueID, sender: "PE")
        })
        
        let queenstown = UIAlertAction(title: "SKYNET QUEENSTOWN", style: .default, handler: { action in
            
            self.performSegue(withIdentifier: segueID, sender: "QUEENSTOWN")
        })
        
        let richardsbay = UIAlertAction(title: "SKYNET RICHARDSBAY", style: .default, handler: { action in
            
            self.performSegue(withIdentifier: segueID, sender: "RICHARDSBAY")
            
        })
        
        let rustenburg = UIAlertAction(title: "SKYNET RUSTENBURG", style: .default, handler: { action in
            
            self.performSegue(withIdentifier: segueID, sender: "RUSTENBURG")
        })
        
        let uppington = UIAlertAction(title: "SKYNET UPPINGTON", style: .default, handler: { action in
            
            self.performSegue(withIdentifier: segueID, sender: "UPPINGTON")
        })
        
        let vryheid = UIAlertAction(title: "SKYNET VRYHEID", style: .default, handler: { action in
            
            self.performSegue(withIdentifier: segueID, sender: "VRYHEID")
            
        })
        
        let welkom = UIAlertAction(title: "SKYNET WELKOM", style: .default, handler: { action in
            
            self.performSegue(withIdentifier: segueID, sender: "WELKOM")
            
        })

        let worcester = UIAlertAction(title: "SKYNET WORCESTER", style: .default, handler: { action in
            
            self.performSegue(withIdentifier: segueID, sender: "WORCESTER")
            
        })

        let el = UIAlertAction(title: "SKYNET EAST LONDON", style: .default, handler: { action in
            
            self.performSegue(withIdentifier: segueID, sender: "EAST LONDON")
            
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        
        alertController.addAction(jhb)
        alertController.addAction(capeTown)
        alertController.addAction(durban)
        alertController.addAction(bethlehem)
        alertController.addAction(bloem)
        alertController.addAction(george)
        alertController.addAction(grahamstown)
        alertController.addAction(kimberly)
        alertController.addAction(klerksdorp)
        alertController.addAction(ladysmith)
        alertController.addAction(nelspruit)
        alertController.addAction(newcastle)
        alertController.addAction(polokwane)
        alertController.addAction(pietermaritzburg)
        alertController.addAction(pe)
        alertController.addAction(queenstown)
        alertController.addAction(richardsbay)
        alertController.addAction(rustenburg)
        alertController.addAction(uppington)
        alertController.addAction(vryheid)
        alertController.addAction(welkom)
        alertController.addAction(worcester)
        alertController.addAction(el)
        alertController.addAction(cancel) // comment if reverting to payment first
        alertController.view.tintColor = UIColor.greenNeolife()
        
        self.present(alertController, animated: true, completion: nil)
    }

    func deliveryAddressAlert(_ controller: UIViewController, segueID: String) {
        
        var deliveryAddress = ""
        var outputAddress = ""
        
        let alertController = UIAlertController(title: "Add Address", message: "Fill in the fields, separate with spaces and press ok or use your current address: '\(self.addLineOne) \(self.addLineTwo) \(self.addLineThree) \(self.postcode)'", preferredStyle: .alert)
        
        alertController.addTextField { (textField: UITextField) in
            textField.placeholder = "Address Line 1"
            textField.keyboardType = .asciiCapable
            textField.autocapitalizationType = .words
            textField.autocorrectionType = .no
        }
        
        alertController.addTextField { (textField: UITextField) in
            textField.placeholder = "Address Line 2"
            textField.keyboardType = .asciiCapable
            textField.autocapitalizationType = .words
            textField.autocorrectionType = .no
        }
        
        alertController.addTextField { (textField: UITextField) in
            textField.placeholder = "City"
            textField.keyboardType = .asciiCapable
            textField.autocapitalizationType = .words
            textField.autocorrectionType = .no
        }
        
        alertController.addTextField { (textField: UITextField) in
            textField.placeholder = "Post Code"
            textField.keyboardType = UIKeyboardType.decimalPad
            textField.autocorrectionType = .no
        }
        
        let ok = UIAlertAction(title: "OK", style: .default, handler: {(action) in
            
            let fieldOne = alertController.textFields?.first?.text
            let fieldTwo = alertController.textFields?[1].text
            let city = alertController.textFields?[2].text
            let code = alertController.textFields?[3].text
            
            let lineOne = fieldOne?.replacingOccurrences(of: ",", with: "")
            let lineTwo = fieldTwo?.replacingOccurrences(of: ",", with: "")

            let enteredAddress = lineOne! + lineTwo! + city! + code!
            let containsComma = enteredAddress.components(separatedBy: ",")
            
//            print("Comma \(containsComma.count)")
            if fieldOne == "" || fieldTwo == "" || city == "" || code == "" || containsComma.count > 1 {
                Alerts.alert(on: self, title: "Invalid Address Entry", and: "Please remove the comma in the address entry and replace it with a space")
            } else  {
                deliveryAddress = lineOne! + " " + lineTwo! + " " + city! + " " + code!
                outputAddress = lineOne! + "," + lineTwo! + "," + city! + "," + code!
                controller.performSegue(withIdentifier: segueID, sender: outputAddress)
            }
        })
        
        let defaultAddress = UIAlertAction(title: "Your Address", style: .default) { (action) in
            deliveryAddress = self.addLineOne + "," + self.addLineTwo + "," + self.addLineThree + "," + self.postcode
            controller.performSegue(withIdentifier: segueID, sender: deliveryAddress)
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        
        if self.addLineThree == "" && self.postcode == "" {
            alertController.addAction(ok)
            alertController.addAction(cancel)
        } else {
            alertController.addAction(defaultAddress)
            alertController.addAction(ok)
            alertController.addAction(cancel)
        }

        controller.present(alertController, animated: true, completion: nil)
    }

    //    MARK: - Gateway Helper Functions
    //reponsible for the checkout view
    func configureCheckoutProvider(checkoutID: String) -> OPPCheckoutProvider? {
//        let provider = OPPPaymentProvider.init(mode: .test)
        let provider = OPPPaymentProvider.init(mode: .live)
        let checkoutSettings = GatewayConfig.configureCheckoutSettings()
        return OPPCheckoutProvider(paymentProvider: provider, checkoutID: checkoutID, settings: checkoutSettings)
    }
    
    func handleTransactionSubmission(transaction: OPPTransaction?, error: Error?) {
        guard let transaction = transaction else {
            Alerts.alert(on: self, title: "Failed", and: (error?.localizedDescription)!)
            print("failed to handle transaction submission")
            CLSLogv("%@", getVaList(["failed to handle transaction submission", error.debugDescription]))
            return
        }
        
        self.transaction = transaction
        if transaction.type == .synchronous {
            // If a transaction is synchronous, just request the payment status
            self.requestPaymentStatus()
            print("transaction is sync")
        } else if transaction.type == .asynchronous {
            // If a transaction is asynchronous, SDK opens transaction.redirectUrl in a browser
            // Subscribe to notifications to request the payment status when a shopper comes back to the app
            NotificationCenter.default.addObserver(self, selector: #selector(self.didReceiveAsynchronousPaymentCallback), name: Notification.Name(rawValue: GatewayConfig.asyncPaymentCompletedNotificationKey), object: nil)
            print("Asynchronous")
        } else {
            Alerts.alert(on: self, title: "Failed", and: "Invalid transaction")
            print("The transaction is invalid")
        }
    }
    
    func requestPaymentStatus() {
        /*guard let resourcePath = self.transaction?.resourcePath else {
            Alerts.alert(on: self, title: "Failed", and: "Resource path is invalid")
            return
        }*/
        
        self.transaction = nil
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        Request.requestPaymentStatus(resourcePath: self.checkoutID) { (success) in
            DispatchQueue.main.async {
//                Log successful transactions
                print("Success?: \(success.description)")
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
//                dismiss the view here after payment
                let message = success ? "Your payment was successful" : "Your payment was not successful"
                self.successFailAlert(controller: self, message: message, success: success)
            }
        }
    }
    
    @objc func didReceiveAsynchronousPaymentCallback() {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: GatewayConfig.asyncPaymentCompletedNotificationKey), object: nil)
        self.checkoutProvider?.dismissCheckout(animated: true) {
            DispatchQueue.main.async {
                self.requestPaymentStatus()
            }
        }
    }
    
    func successFailAlert(controller: UIViewController, message: String?, success: Bool) {
        let title = success ? "Success" : "Failure"
        var ok = UIAlertAction()
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if success == false {
            ok = UIAlertAction(title: "OK", style: .default, handler: nil)
        } else {
            ok = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                
                let cell = self.totalTableView.cellForRow(at: self.indexPath) as! TotalCell
                
                if cell.pickSwitch.isOn {
                    self.addressSelectionActionSheet(segueID: "pdfPreviewSegue")
                } else {
                    self.deliveryAddressAlert(controller, segueID: "pdfPreviewSegue")
                }
            })
        }
        
        alertController.addAction(ok)
        
        controller.present(alertController, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}
