//
//  SubMenuController.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/03/21.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//

import UIKit
import Parse
import MIBadgeButton_Swift
import CoreData
import Firebase

class SubMenuController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

//    var categoriesData = [PFObject?]()
    var categories = [Category]()
    let api = APIService()
    let cartBadge = CartBadge()
    var swipeLabel = UILabel()
    var fingerIconView = UIImageView()
    var arrowIconView = UIImageView()
    var swipeView = UIView()
    let ref = Database.database().reference(fromURL: "https://neolife-d9754.firebaseio.com/")
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)

//        getCategories()
        
//        let callFirebaseNode = CallFirebaseNode("product_catalog", storedIn: categories)
//        callFirebaseNode.productCatalog(in: self)
        
        getCatalog()
        
        cartBadge.controller = self
        cartBadge.countString = "\(cartBadge.cartCount())"
        cartBadge.segueId = "menuToCartSegue"
        cartBadge.addBadgeToBtn()
//        createIndicator()
//        showPrompt()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.titleView = UIImageView(image: #imageLiteral(resourceName: "neolife_logo"))
        self.collectionView?.reloadData()
//        showPrompt()
//        let refresher = UIRefreshControl()
//        refresher.addTarget(self, action: #selector(self.refresh), for: UIControlEvents.valueChanged)
//        refreshControl = refresher
        
//        self.collectionView?.addSubview(refreshControl)
    }
    
//    MARK: Refresh control
    
//    lazy var refreshControl: UIRefreshControl = {
//        let refreshControl = UIRefreshControl()
//        refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControlEvents.valueChanged)
//        return refreshControl
//    }()
    
//    func refresh(refreshControl: UIRefreshControl) {
//        refreshControl.beginRefreshing()
//        let timer = DispatchTime.now() + 3
//        DispatchQueue.main.asyncAfter(deadline: timer) {
//
//            self.getRefreshedCatagories()
//
//            self.navigationItem.prompt = nil
//            refreshControl.endRefreshing()
//        }
//        self.collectionView?.reloadData()
//    }
    
//    func showPrompt() {
//        self.navigationItem.prompt = "Pull to Refresh"
//    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "subMenuSegue" {
            let navController = segue.destination as! UINavigationController
            let pvc : ProductVC = navController.topViewController as! ProductVC
            //        let pvc = segue.destination as! ProductVC
            let indexPath = collectionView?.indexPath(for: sender as! SubMenuCell)
            
            navController.transitioningDelegate = self
            
            if (indexPath?.row)! < self.categories.count {
                if let id = categories[(indexPath?.row)!].catalog_identifier {
                    pvc.categoryId = id
                    print("CATEGORY ID: \(id)")
                }
                
                if let name = categories[(indexPath?.row)!].catalog_name {
                    pvc.categoryName = name
                    print("CATEGORY NAME: \(name)")
                }
            } else {
                pvc.categoryName = "A-Z"
                pvc.categoryId = ""
            }
            
            
//            if (indexPath?.row)! < self.categoriesData.count {
//                if let id = categoriesData[(indexPath?.row)!]?["catalog_identifier"] as? String {
//                    pvc.categoryId = id
//                    print("CATEGORY ID: \(id)")
//                }
//
//                if let name = categoriesData[(indexPath?.row)!]?["catalog_name"] as? String {
//                    pvc.categoryName = name
//                    print("CATEGORY NAME: \(name)")
//                }
//            } else {
//                pvc.categoryName = "A-Z"
//                pvc.categoryId = ""
//            }
        }
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        // allows additional cell (A-Z)
//        return categoriesData.count + 1
        return categories.count + 1
        
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // using if statement to dequeue different cells
        if indexPath.row < self.categories.count {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "subCell", for: indexPath) as! SubMenuCell
            var name = ""
            
            CellStructure().greyBorder(cell)
            
            //retrive object from array of stored queried Objects
            if let n = categories[indexPath.row].catalog_name {
                name = n
            }
            
            cell.subMenuLbl.text = name

            //load image from object
            if let url = categories[indexPath.row].catalog_image {
                cell.subMenuImage.loadImageFromCacheWithUrl(url)
            } else {
                cell.subMenuImage.image = #imageLiteral(resourceName: "neolifeLogo")
            }
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "subCell", for: indexPath) as! SubMenuCell
            
            CellStructure().greyBorder(cell)
            
            cell.subMenuLbl.text = "A-Z"
            cell.subMenuImage.image = #imageLiteral(resourceName: "neolifeLogo")
            
            return cell
        }

//        if indexPath.row < self.categoriesData.count {
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "subCell", for: indexPath) as! SubMenuCell
//            var w = ""
//
//            CellStructure().greyBorder(cell)
//
//            //retrive object from array of stored queried Objects
//            if let x = categoriesData[indexPath.row]?["catalog_name"] as? String {
//                w = x
//            }
//
//            cell.subMenuLbl.text = w
//
//            //Need a placeholder image before loading images
////            cell.subMenuImage.image = #imageLiteral(resourceName: "neolifeLogo")
//
//            //load image from object
//            if let y = categoriesData[indexPath.row]?["catalog_image"] as? String {
//                //            loadImages(y, imageView: cell.subMenuImage)
////                api.loadImages(y, imageView: cell.subMenuImage)
//                cell.subMenuImage.loadImageFromCacheWithUrl(y)
//            } else {
//                cell.subMenuImage.image = #imageLiteral(resourceName: "neolifeLogo")
//            }
//
//            return cell
//        } else {
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "subCell", for: indexPath) as! SubMenuCell
//
//            CellStructure().greyBorder(cell)
//
//            cell.subMenuLbl.text = "A-Z"
//            cell.subMenuImage.image = #imageLiteral(resourceName: "neolifeLogo")
//
//            return cell
//        }
    }
    
//    MARK: Collection View Delegate
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 5, left: 5, bottom: 5, right: 5)
    }
    
//    MARK: Flow layout delegate
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CellStructure().dynamicCollectionCell(collectionView)
    }
    
//    MARK: - Backend Function(s)
    
    func getCatalog() {
        self.categories.removeAll()
        
        ref.child("product_catalog").queryOrdered(byChild: "catalog_name").observe(.childAdded, with: { (snap) in
            if let dict = snap.value as? [String : AnyObject] {
                let category = Category()
                
                category.catalog_identifier = dict["catalog_identifier"] as? String
                category.catalog_image = dict["catalog_image"] as? String
                category.catalog_name = dict["catalog_name"] as? String
                
                DispatchQueue.main.async {
                    self.categories.append(category)
                }
                
                DispatchQueue.main.async {
                    self.collectionView?.reloadData()
                }
            }
        }, withCancel: nil)
    }
    /*
    func getCategories(){
//        let cv = self.collectionView
//        let indexPaths = cv?.indexPathsForVisibleItems
        
        let query = PFQuery(className:"product_catalog")
        query.addAscendingOrder("catalog_name")
        query.fromLocalDatastore()
        
        query.findObjectsInBackground { (objects, error) in
            if error == nil {
                DispatchQueue.global(qos: .userInitiated).async {
                    if let objects = objects {
                        
                        /*for object in objects {
                            let name = object["catalog_name"] as? String
                            let image = object["catalog_image"] as? String
                            
                            DispatchQueue.main.async {
                                for cell in indexPaths! {
                                    let menuCell = cv?.cellForItem(at: cell) as! SubMenuCell
                                    menuCell.subMenuLbl.text = name
                                    self.api.loadImages(image, imageView: menuCell.subMenuImage)
                                    
                                }
                            }
                        }*/

                        
                        DispatchQueue.main.async {
                            self.categoriesData = Array(objects.makeIterator())
                        }
                    }
                    
                    DispatchQueue.main.async {
                        self.collectionView?.reloadData()
                    }
                }
            } else {
                print("Error in Getting PRODUCT CATALOG: \(String(describing: error)), \(String(describing: error?.localizedDescription))")
            }
        }
    }
    */
}


extension SubMenuController {
    /*
    func getRefreshedCatagories() {
        self.categoriesData.removeAll()
        let query = PFQuery(className:"product_catalog")
        query.addAscendingOrder("catalog_name")
        query.fromLocalDatastore()
        
        //unpinning old content from the local datastore
        query.findObjectsInBackground { (objects, error) in
            if error == nil {
                PFObject.unpinAll(inBackground: objects)
            }
        }
        
        let nQuery = PFQuery(className:"product_catalog")
        nQuery.addAscendingOrder("catalog_name")
        
        nQuery.findObjectsInBackground { (objects, error) in
            if error == nil {
                PFObject.pinAll(inBackground: objects)
                
//                print("Num OBJECTS: \(objects?.count)")
                
                if let objects = objects {
                    DispatchQueue.main.async {
                        self.categoriesData = Array(objects.makeIterator())
                    }
                }
                
                DispatchQueue.main.async {
                    self.collectionView?.reloadData()
                }
                
                print("Loaded new content")
            } else {
                print("Error: \(String(describing: error)), \(String(describing: error?.localizedDescription))")
            }
        }
        
        self.collectionView?.reloadData()
        
    }
    */
    
    func isFirstTimeIn() -> Bool {
        if (!UserDefaults.standard.bool(forKey: "firstTimeIn")) {
            UserDefaults.standard.set(true, forKey: "firstTimeIn")
            UserDefaults.standard.synchronize()
            
            // prompt refreshing goes here
            
            
            Alerts.alertWithImage(on: self, title: "You're new! Welcome", and: "Things you need to know, to be able to make purchases as a member or distributor, make sure that you have an active email account in your settings. This can be found in 'Settings > Accounts & Passwords'")
            return true
        }
        
        return false
    }
    
//    MARK: - Swipe Prompt
    
    func createIndicator() {
        swipeView.frame = CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.view.frame.height)
        swipeView.center = CGPoint(x: view.frame.width/2, y: view.frame.height/2)
        swipeView.backgroundColor = UIColor(white: 0.0, alpha: 0.8)
        self.tabBarController?.view.addSubview(swipeView)
        
//        view.addSubview(swipeView)
        
        //        activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.height)) as UIActivityIndicatorView
        swipeLabel.textColor = UIColor.white
        swipeLabel.text = "Swipe to refresh"
        swipeLabel.font = UIFont(name: "Helvetica Neue", size: 17)
        swipeLabel.numberOfLines = 0
        swipeLabel.textAlignment = .center
        swipeLabel.sizeToFit()
        swipeLabel.center = CGPoint(x: fingerIconView.center.x + view.frame.width/2, y: fingerIconView.center.y + view.frame.height/2.5)
//        swipeView.addSubview(swipeLabel)
        
//        fingerIconView.center = self.view.center
        fingerIconView.image = #imageLiteral(resourceName: "finger")
        fingerIconView.frame = CGRect(x: self.view.center.x + view.frame.width/2, y: self.view.center.y, width: 100, height: 100)
//        fingerIconView.
        swipeView.addSubview(fingerIconView)
        
        print("indicator added")
    }
    
}

extension SubMenuController: UIViewControllerTransitioningDelegate {
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return DissmissAnimator()
    }
}


