//
//  Alert.swift
//  neolife
//
//  Created by Karabo Moloi on 2018/02/13.
//  Copyright © 2018 Karabo Moloi. All rights reserved.
//

import UIKit
import Parse
import Firebase

class Alerts {
    
    let user = PFUser.current()
    
    static func alert(on controller: UIViewController, title: String, and message: String?){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alertController.addAction(ok)
        alertController.view.tintColor = UIColor.greenNeolife()
        
        controller.present(alertController, animated: true, completion: nil)
    }
    
    static func alertWithImage(on controller: UIViewController, title: String, and message: String?){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        let imageView = UIImageView(frame: CGRect(x: alertController.view.frame.width/2 - 150, y: 0, width: 150, height: 150))
        imageView.alpha = 0.15
        imageView.image = #imageLiteral(resourceName: "neolifeLogo")
        
        alertController.view.addSubview(imageView)
        alertController.addAction(ok)
        alertController.view.tintColor = UIColor.greenNeolife()
        
        controller.present(alertController, animated: true, completion: nil)
    }
    
    static func dismissAlert(on controller: UIViewController, title: String, and message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default) { (action) in
            controller.dismiss(animated: true, completion: nil)
        }
        
        alertController.addAction(ok)
        alertController.view.tintColor = UIColor.greenNeolife()
        
        controller.present(alertController, animated: true, completion: nil)
    }

    
    static func editUserProfile(on controller: UIViewController) {
        let alertController = UIAlertController(title: "Edit Profile", message: "", preferredStyle: .actionSheet)
        
        let address = UIAlertAction(title: "Change Address", style: .default) { (action) in
            
            let alertController = UIAlertController(title: "Edit Address", message: "Edit address below", preferredStyle: .alert)
            alertController.addTextField(configurationHandler: { (textField: UITextField) in
                textField.placeholder = "Address Line 1"
                textField.keyboardType = .default
                textField.autocapitalizationType = UITextAutocapitalizationType.words
            })
            alertController.addTextField(configurationHandler: { (textField: UITextField) in
                textField.placeholder = "Address Line 2"
                textField.keyboardType = .default
                textField.autocapitalizationType = UITextAutocapitalizationType.words
            })
            alertController.addTextField(configurationHandler: { (textField: UITextField) in
                textField.placeholder = "City"
                textField.keyboardType = .default
                textField.autocapitalizationType = UITextAutocapitalizationType.words
            })
            alertController.addTextField(configurationHandler: { (textField: UITextField) in
                textField.placeholder = "Postcode"
                textField.keyboardType = .numberPad
            })
            
            let confirm = UIAlertAction(title: "Confirm", style: .default) { (action) in
                let lineOne = alertController.textFields?.first?.text
                let lineTwo = alertController.textFields?[1].text
                let city = alertController.textFields?[2].text
                let postcode = alertController.textFields?[3].text
                
                //check for commas
                let noCommaLineOne = lineOne?.replacingOccurrences(of: ",", with: " ")
                let noCommaLineTwo = lineTwo?.replacingOccurrences(of: ",", with: " ")
                
                //updates address
                AlertFunctions().addAddressToUser(noCommaLineOne!, noCommaLineTwo!, city!, postcode!)
                
                controller.dismiss(animated: true, completion: nil)
            }
            
            
            let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
            
            alertController.addAction(confirm)
            alertController.addAction(cancel)
            alertController.view.tintColor = UIColor.greenNeolife()
            
            controller.present(alertController, animated: true, completion: nil)
        }
        
        // change password function
        let password = UIAlertAction(title: "Change Password", style: .default) { (action) in
            let alertController = UIAlertController(title: "Change Password", message: "Change password below", preferredStyle: .alert)
            
            alertController.addTextField(configurationHandler: { (textField: UITextField) in
                textField.placeholder = "New Password"
                textField.keyboardType = .default
                textField.autocapitalizationType = UITextAutocapitalizationType.words
                textField.isSecureTextEntry = true
            })
            
            let confirm = UIAlertAction(title: "Confirm", style: .default) { (action) in
                let newPassword = alertController.textFields?.first?.text
                let userAuth = Auth.auth().currentUser
                
                if let pass = newPassword {
                    if pass != "" {
                        userAuth?.updatePassword(to: pass, completion: { (error) in
                            if error != nil {
                                let myError = error as NSError?
                                let userInfo = myError?.userInfo
                                
                                if let errorDict = userInfo as? [String : Any] {
                                    print("Info Key: \(errorDict)")
                                    let errorType = errorDict["error_name"] as? String
                                    let errorReason = errorDict["NSLocalizedDescription"] as? String
                                    
                                    if errorType == "ERROR_WEAK_PASSWORD" {
                                        Alerts.alert(on: controller, title: "Weak Password", and: errorReason)
                                    }
                                    
                                    if errorType == "ERROR_REQUIRES_RECENT_LOGIN" {
                                        Alerts.alert(on: controller, title: "Before you change your password!", and: "For security reasons, please logout, then log back in before trying to change your password again.")
                                    }
                                }
                            } else {
                                Alerts.alert(on: controller, title: "Success!", and: "Successfully changed password")
                            }
                        })
                    } else {
                        Alerts.alert(on: controller, title: "Empty Field", and: "Please enter a valid password")
                    }
                }
            }
            
            let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
            
            alertController.addAction(confirm)
            alertController.addAction(cancel)
            alertController.view.tintColor = UIColor.greenNeolife()
            
            controller.present(alertController, animated: true, completion: nil)
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        
        alertController.addAction(address)
        alertController.addAction(password)
        alertController.addAction(cancel)
        alertController.view.tintColor = UIColor.greenNeolife()
        controller.present(alertController, animated: true, completion: nil)
    }

    static func idAlert(on controller: UIViewController) {
        let alertController = UIAlertController(title: "Enter NeoLife ID", message: "", preferredStyle: .alert)
        
        alertController.addTextField(configurationHandler: { (textField: UITextField) in
            textField.placeholder = "NeoLife ID"
            textField.keyboardType = .numberPad
        })
        
        let ok = UIAlertAction(title: "OK", style: .default) { (action) in
            let userId = alertController.textFields?.first?.text
            
            if let id = userId {
                UserFunctions.saveIdToUserDefaultsWith(id: id)
            } else {
                Alerts.alert(on: alertController, title: "Empty", and: "Please enter a NeoLife ID")
            }
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        
        alertController.addAction(ok)
        alertController.addAction(cancel)
        alertController.view.tintColor = UIColor.greenNeolife()
        
        controller.present(alertController, animated: true, completion: nil)
    }
    
    static func orderComplete(on controller: UIViewController, with completion: @escaping () -> Void) {
        let alertController = UIAlertController(title: "Order Completed", message: "Thank you for placing an order with NeoLife.", preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default) { (action) in
            completion()
        }
        
        alertController.addAction(ok)
        alertController.view.tintColor = UIColor.greenNeolife()
        
        controller.present(alertController, animated: true, completion: nil)
    }
    
    /// Alert with completion on OK without cancel
    ///
    /// - Parameters:
    ///   - controller: Controller to present alert on
    ///   - message: Message in Alert
    ///   - title: Title of alert
    ///   - completion: What should happen when OK is pressed
    static func alert(on controller: UIViewController, message: String, title: String, with completion: @escaping () -> Void) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default) { (action) in
            completion()
        }
        
        alertController.addAction(ok)
        alertController.view.tintColor = UIColor.greenNeolife()
        
        controller.present(alertController, animated: true, completion: nil)
    }
    
    static func saveOrderAlert(in controller: UIViewController, completion: @escaping () -> Void) {
        let alertController = UIAlertController(title: "Order Saved", message: "To complete your order, complete your order via the Mail App in drafts, from the email 'ordera@za.neolife.com' and press send", preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "Ok", style: .default) { (action) in
            completion()
        }
        
        alertController.addAction(ok)
        alertController.view.tintColor = UIColor.greenNeolife()
        controller.present(alertController, animated: true, completion: nil)
    }
    
    static func userOptions(in controller: UIViewController, segue: @escaping () -> Void) {
        let alertController = UIAlertController(title: "Options", message: "Select an option", preferredStyle: .actionSheet)
        
        let edit = UIAlertAction(title: "Edit Address", style: .default) { (action) in
            Alerts.editUserProfile(on: controller)
        }
        
        let history = UIAlertAction(title: "Order History", style: .default) { (action) in
            segue()
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        
        alertController.view.tintColor = UIColor.greenNeolife()
        alertController.addAction(edit)
        alertController.addAction(history)
        alertController.addAction(cancel)
        
        controller.present(alertController, animated: true, completion: nil)
    }
    
    
    /// Alert with completion and cancel
    ///
    /// - Parameters:
    ///   - controller: The controller that the alert will be presented on
    ///   - title: Title of the alert
    ///   - message: Message of the alert
    ///   - withCompletion: The completion function when Ok is pressed.
    static func alert(_ controller: UIViewController, _ title: String, _ message: String, _ withCompletion: @escaping () -> Void) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "Ok", style: .default) { (action) in
            withCompletion()
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        
        alertController.addAction(ok)
        alertController.addAction(cancel)
        controller.present(alertController, animated: true, completion: nil)
    }
    
}

