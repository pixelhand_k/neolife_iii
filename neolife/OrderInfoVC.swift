//
//  OrderInfoVC.swift
//  neolife
//
//  Created by Karabo Moloi on 2018/09/01.
//  Copyright © 2018 Karabo Moloi. All rights reserved.
//

import UIKit

class PopupInfoVC: UIViewController {

    var info = String()
    @IBOutlet weak var messageLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        messageLbl.numberOfLines = 0
        messageLbl.text = info
    }

}
