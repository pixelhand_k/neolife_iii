//
//  AlertFunctions.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/06/22.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//

import UIKit
import Parse
//import CoreDbata
import Firebase

class AlertFunctions {
    
    //    let member = Member()
    let user = PFUser.current()

    func loginFailedAlert(_ controller: UIViewController) {
        let alertController = UIAlertController(title: "Incorrect Credentials", message: "Pin/Password is incorrect", preferredStyle: .alert)
        
        let cancel = UIAlertAction(title: "Ok", style: .default, handler: nil)
        
//        let ok = UIAlertAction(title: "Yes", style: .default, handler: { action in
//            self.registerAlert(controller)
//        })
        
//        alertController.addAction(ok)
        alertController.addAction(cancel)
        alertController.view.tintColor = UIColor.greenNeolife()
        controller.present(alertController, animated: true, completion: nil)
        
    }

    /// validates email
    ///
    /// - Parameter candidate: this is the email that will be traversed through
    /// - Returns: returns true if it contails characters in emailRegex
    func validateEmail(candidate: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: candidate)
    }
    
    func userProfileActionSheet(_ controller: UIViewController) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let redeem = UIAlertAction(title: "Redeem ID", style: .default, handler: {(action) in
            let redeemAlertController = UIAlertController(title: "Redeem ID", message: "Please enter your NeoLife ID to redeem", preferredStyle: .alert)
            
            redeemAlertController.addTextField(configurationHandler: { (textfield: UITextField) in
                textfield.placeholder = "Distributer ID"
                textfield.keyboardType = .numberPad
            })
            
            //add parse function for redeeming
            let redeemId = UIAlertAction(title: "Redeem Now", style: .default, handler: {(action) in
                let idField = redeemAlertController.textFields?.first?.text
                
                if idField != "" {
                    let query = PFQuery(className: "distributors")
                    query.whereKey("id_number", equalTo: Int(idField!)!)
                    
                    query.findObjectsInBackground(block: { (objects, error) in
                        if error == nil {
                            print("OBJECT: \(String(describing: objects))")
                            
                            for object in objects! {
                                let name = object["name"] as! String
                                let title = object["Title"] as! Int
                                let id = object["id_number"] as! Int
                                let cc = object["cc"] as! Int
                                let gpv = object["GPV"] as! Int
                                let ppv = object["PPV"] as! Int
                                let qpv = object["QPV"] as! Int
                                
                                self.user?["Name"] = name
                                self.user?["Title"] = String.fromInt(integer: title)
                                self.user?["id_number"] = String.fromInt(integer: id)
                                self.user?["cc"] = String.fromInt(integer: cc)
                                self.user?["GPV"] = String.fromInt(integer: gpv)
                                self.user?["PPV"] = String.fromInt(integer: ppv)
                                self.user?["QPV"] = String.fromInt(integer: qpv)
                                self.user?["is_distributor"] = true
                                //                                self.user?.saveEventually()
                                self.user?.saveInBackground()
                            }
                            
                        } else {
                            print("Error: \(String(describing: error?.localizedDescription))")
                            Alerts.alert(on: controller, title: "Error", and: (error?.localizedDescription)!)
                        }
                    })
                }
                //query distributors here with idfield as param
            })
            
            let cancelRedeem = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
            
            redeemAlertController.addAction(redeemId)
            redeemAlertController.addAction(cancelRedeem)
            redeemAlertController.view.tintColor = UIColor.greenNeolife()
            
            controller.present(redeemAlertController, animated: true, completion: nil)
            
        })
        
        let view = UIAlertAction(title: "View Profile", style: .default, handler: {(action) in
            controller.performSegue(withIdentifier: "profileSegue", sender: nil)
        })
        
        //        let logout = UIAlertAction(title: "Logout", style: .default, handler: {(action) in
        //            controller.performSegue(withIdentifier: "logoutUnwindSegue", sender: self)
        //        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        
        
        alertController.addAction(redeem)
        alertController.addAction(view)
        //        alertController.addAction(logout)
        alertController.addAction(cancel)
        alertController.view.tintColor = UIColor.greenNeolife()
        
        controller.present(alertController, animated: true, completion: nil)
    }
    
    func distributerProfileActionSheet(_ controller: UIViewController) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let viewProfile = UIAlertAction(title: "View Profile", style: .default, handler: {(action) in
            controller.performSegue(withIdentifier: "profileSegue", sender: nil)
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        
        alertController.addAction(viewProfile)
        alertController.addAction(cancel)
        alertController.view.tintColor = UIColor.greenNeolife()
        
        controller.present(alertController, animated: true, completion: nil)
    }
    
    func guestProfileActionSheet(_ controller: UIViewController) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let register = UIAlertAction(title: "Register to be member", style: .default, handler: {(action) in
            /*let redeemAlertController = UIAlertController(title: "Redeem Distributer ID", message: "Please enter your Distributer ID to redeem", preferredStyle: .alert)
             
             redeemAlertController.addTextField(configurationHandler: { (textfield: UITextField) in
             textfield.placeholder = "Distributer ID"
             textfield.keyboardType = .numberPad
             })
             
             //add parse function for redeeming
             let redeemId = UIAlertAction(title: "Redeem Now", style: .default, handler: nil)
             
             let cancelRedeem = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
             
             redeemAlertController.addAction(redeemId)
             redeemAlertController.addAction(cancelRedeem)
             
             controller.present(redeemAlertController, animated: true, completion: nil)*/
            
            let alertCon = UIAlertController(title: "Register", message: "Logout and register to become member", preferredStyle: .alert)
            
            let logout = UIAlertAction(title: "Logout", style: .default, handler: {(action) in
                controller.performSegue(withIdentifier: "logoutUnwindSegue", sender: nil)
            })
            
            alertCon.addAction(logout)
            alertController.view.tintColor = UIColor.greenNeolife()
            controller.present(alertCon, animated: true, completion: nil)
            
        })
        
        //        let view = UIAlertAction(title: "View Profile", style: .default, handler: {(action) in
        //            controller.performSegue(withIdentifier: "profileSegue", sender: nil)
        //        })
        
        //        let logout = UIAlertAction(title: "Logout", style: .default, handler: {(action) in
        //            controller.performSegue(withIdentifier: "logoutUnwindSegue", sender: self)
        //        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        
        
        alertController.addAction(register)
        //        alertController.addAction(view)
        //        alertController.addAction(logout)
        alertController.addAction(cancel)
        alertController.view.tintColor = UIColor.greenNeolife()
        
        controller.present(alertController, animated: true, completion: nil)
    }
    
    func addAddressAlert(_ controller: UIViewController) {
        
        let alertController = UIAlertController(title: "Add Address", message: "Fill in the fields, separate with spaces and press ok to add address to your profile.", preferredStyle: .alert)
        
        alertController.addTextField { (textField: UITextField) in
            textField.placeholder = "Address Line 1"
            textField.keyboardType = .default
            textField.autocapitalizationType = .words
            textField.autocorrectionType = .no
        }
        
        alertController.addTextField { (textField: UITextField) in
            textField.placeholder = "Address Line 2"
            textField.keyboardType = .default
            textField.autocapitalizationType = .words
            textField.autocorrectionType = .no
        }
        
        alertController.addTextField { (textField: UITextField) in
            textField.placeholder = "City"
            textField.keyboardType = .default
            textField.autocapitalizationType = .words
            textField.autocorrectionType = .no
        }
        
        alertController.addTextField { (textField: UITextField) in
            textField.placeholder = "Post Code"
            textField.keyboardType = UIKeyboardType.decimalPad
            textField.autocorrectionType = .no
        }
        
        let ok = UIAlertAction(title: "OK", style: .default, handler: {(action) in
            
            let fieldOne = alertController.textFields?.first?.text
            let fieldTwo = alertController.textFields?[1].text
            let city = alertController.textFields?[2].text
            let code = alertController.textFields?[3].text
            
            //remove special characters

            self.addAddressToUser(fieldOne!, fieldTwo!, city!, code!)
            //            print("Saved Address: \(fieldOne!), \(fieldTwo!), \(city!), \(code!)")
//            controller.dismiss(animated: true, completion: nil)
            Alerts.alert(on: controller, title: "", and: "Address Added")
        })
        
        
//        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        
        alertController.addAction(ok)
//        alertController.addAction(cancel)
        alertController.view.tintColor = UIColor.greenNeolife()
        controller.present(alertController, animated: true, completion: nil)
        
    }
    
    func addAddressToUser(_ addressOne: String, _ addressTwo: String, _ city: String, _ code: String) {
        let address = addressOne + "," + addressTwo + "," + city + "," + code
        //        print("Address: \(address)")
        let ref = Database.database().reference(fromURL: "https://neolife-d9754.firebaseio.com/")
        let id = UserFunctions.getNeolifeId()
        
        if id != "" {
            ref.child("users").child(id).updateChildValues(["address" : address])
        } else {
            print("NO ID PRESENT")
        }
        
        
    }
}
