//
//  CartInfoVC.swift
//  neolife
//
//  Created by Karabo Moloi on 2019/03/08.
//  Copyright © 2019 Karabo Moloi. All rights reserved.
//

import UIKit

class CartInfoVC: UIViewController {

    var message: String = ""
    @IBOutlet weak var infoLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        infoLbl.text = message
        
    }

}
