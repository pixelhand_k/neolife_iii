//
//  PreviewVC+.swift
//  neolife
//
//  Created by Karabo Moloi on 2018/02/22.
//  Copyright © 2018 Karabo Moloi. All rights reserved.
//

import UIKit
import Firebase
import SendGrid
import CoreData

extension PreviewVC {
    func deleteSelectedFromCD(_ name: String){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let prodFetch : NSFetchRequest<ShoppingBag> = ShoppingBag.fetchRequest()
        
        prodFetch.predicate = NSPredicate(format: "name == %@", name)
        
        let item = try! managedContext.fetch(prodFetch)
        
        for object in item {
            managedContext.delete(object)
        }
        
        do {
            try managedContext.save()
            print("removed \(name) form CD")
            self.dismiss(animated: true, completion: nil)
        } catch let error as NSError {
            print("couldnt delete, error: \(error), \(error.userInfo)")
            Alerts.alert(on: self, title: "Error", and: "\(String(describing: error.localizedFailureReason))")
        }
    }
    //    MARK: Gateway Helper Functions - if pay after order
    func successFailAlert(controller: UIViewController, message: String?, success: Bool) {
        let title = success ? "Success" : "Failure"
        var ok = UIAlertAction()
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if success == false {
            ok = UIAlertAction(title: "OK", style: .default, handler: nil)
        } else {
            ok = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                controller.performSegue(withIdentifier: "previewUnwind", sender: nil)
            })
        }
        
        alertController.addAction(ok)
        
        controller.present(alertController, animated: true, completion: nil)
    }
    //reponsible for the checkout view
    func configureCheckoutProvider(checkoutID: String) -> OPPCheckoutProvider? {
//        let provider = OPPPaymentProvider.init(mode: .test)
        let provider = OPPPaymentProvider.init(mode: .live)
        let checkoutSettings = GatewayConfig.configureCheckoutSettings()
        //        return OPPCheckoutProvider.init(paymentProvider: provider, checkoutID: checkoutID, settings: checkoutSettings)
        return OPPCheckoutProvider(paymentProvider: provider, checkoutID: checkoutID, settings: checkoutSettings)
    }
    
    func handleTransactionSubmission(transaction: OPPTransaction?, error: Error?) {
        guard let transaction = transaction else {
            //            Utils.showResult(presenter: self, success: false, message: error?.localizedDescription)
            Alerts.alert(on: self, title: "Failed", and: (error?.localizedDescription)!)
            print("failed to handle transaction submission")
            return
        }
        
        self.transaction = transaction
        if transaction.type == .synchronous {
            // If a transaction is synchronous, just request the payment status
            self.requestPaymentStatus()
            print("transaction is sync")
        } else if transaction.type == .asynchronous {
            // If a transaction is asynchronous, SDK opens transaction.redirectUrl in a browser
            // Subscribe to notifications to request the payment status when a shopper comes back to the app
            NotificationCenter.default.addObserver(self, selector: #selector(self.didReceiveAsynchronousPaymentCallback), name: Notification.Name(rawValue: GatewayConfig.asyncPaymentCompletedNotificationKey), object: nil)
            print("Asynchronous")
        } else {
            Alerts.alert(on: self, title: "Failed", and: "Invalid transaction")
            print("The transaction is invalid")
        }
    }
    
    func requestPaymentStatus() {
        self.transaction = nil
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        Request.requestPaymentStatus(resourcePath: self.checkoutID) { (success) in
            DispatchQueue.main.async {
                print("Success?: \(success.description)")
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                //                dismiss the view here after payment
                let message = success ? "Your payment was successful" : "Your payment was not successful"
                self.successFailAlert(controller: self, message: message, success: success)
            }
        }
    }
    
    // MARK: - Async payment callback
    @objc func didReceiveAsynchronousPaymentCallback() {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: GatewayConfig.asyncPaymentCompletedNotificationKey), object: nil)
        self.checkoutProvider?.dismissCheckout(animated: true) {
            DispatchQueue.main.async {
                self.requestPaymentStatus()
            }
        }
    }
    
    //    MARK: - Storage to Firebase
//    func saveOrder(using invoiceNum: String, total: String, time: String, date: String, isSent: Bool, neolifeID: String, csv: String, pdf: String) {
//        let ref = Database.database().reference(fromURL: "https://neolife-d9754.firebaseio.com/")
//        let values = ["orderNum": invoiceNum, "invoice": pdf, "total": total, "time": time, "date": date, "isSent": isSent, "csv": csv, "neolifeID": neolifeID, "paymentType": "Online"] as [String : Any]
//
//        ref.child("orders").child("\(neolifeID)").child("\(Date.toMilliseconds())").updateChildValues(values)
//        print("Saved To Store")
//    }
    
    func uploadUserOrderIf(isSent: Bool) {
//        let orderName = invoiceNumber
        let neolifeId = UserFunctions.getNeolifeId()
    
        let pdfRef = Storage.storage().reference().child("orders").child("\(Date.toMilliseconds()).pdf")
        let csvRef = Storage.storage().reference().child("orders").child("\(Date.toMilliseconds()).csv")
        
        // time and date
        let date = Date()
        let dateFormatter = DateFormatter()
        let timeFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        dateFormatter.timeZone = TimeZone(identifier: "en_ZA")
        dateFormatter.locale = Locale(identifier: "en_ZA")
        timeFormatter.timeStyle = .medium
        timeFormatter.locale = Locale(identifier: "en_ZA")
        timeFormatter.timeZone = TimeZone(identifier: "en_ZA")
        
        let time = timeFormatter.string(from: date)
        let today = dateFormatter.string(from: date)
        
        composer.exportHTMLContentToPDF(HTMLContent: htmlContent) // converst html string to pdf and saves it
        
        if let orderPdf = try? NSData(contentsOfFile: composer.pdfFileName) as Data {
            if let csvData = try? NSData(contentsOf: csvComposer.exportCSV(csvContent) as URL) as Data {
                
                pdfRef.putData(orderPdf, metadata: nil) { (pdfMeta, error) in
                    if error != nil {
                        CLSLogv("%@", getVaList(["PDF SAVE Error: \(error.debugDescription)"]))
//                        print("PDF SAVE Error: \(error)")
                        return
                    }
                    
                    csvRef.putData(csvData, metadata: nil, completion: { (csvMeta, error) in
                        if error != nil {
                            CLSLogv("%@", getVaList(["CSV SAVE Error: \(error.debugDescription)"]))
//                            print("CSV SAVE Error: \(error)")
                            return
                        }
                        
                        pdfRef.downloadURL(completion: { (pdfUrl, pdfError) in
                            csvRef.downloadURL(completion: { (csvUrl, csvError) in
                                
                                if pdfError == nil {
                                    if csvError == nil {
                                        if let pdfPath = pdfUrl?.absoluteString {
                                            if let csvPath = pdfUrl?.absoluteString {
                                                SaveOrder.saveOrder(withTotal: self.grossTotal, time: time, date: today, isSent: isSent, neolifeID: neolifeId, csv: csvPath, pdf: pdfPath, image: "")
                                                
                                                
//                                                self.saveOrder(using: self.invoiceNumber, total: self.grossTotal, time: time, date: today, isSent: isSent, neolifeID: neolifeId, csv: csvPath, pdf: pdfPath)
                                            }
                                        }
                                    } else {
                                        CLSLogv("%@", getVaList(["csv error: \(csvError.debugDescription)"]))
//                                        print("csv error: \(csvError.debugDescription)")
                                    }
                                } else {
                                    CLSLogv("%@", getVaList(["pdf error: \(pdfError.debugDescription)"]))

//                                    print("pdf error: \(pdfError.debugDescription)")
                                }
                                
                            })
                        })
                    })
                    
                    /*csvRef.putData(csvData, metadata: nil, completion: { (csvMeta, error) in
                        if error != nil {
                            print("CSV SAVE Error: \(error)")
                            return
                        }
                        
                        if let pdfurl = pdfMeta?.downloadURL()?.absoluteString {
                            if let csvurl = csvMeta?.downloadURL()?.absoluteString {
                                
                                self.saveOrder(using: self.invoiceNumber, total: self.grossTotal, time: time, date: today, isSent: isSent, neolifeID: neolifeId, csv: csvurl, pdf: pdfurl)
                            }
                        }
                    })*/
                }
            }
        }
    }
}
