//
//  CaseCell.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/02/27.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//

import UIKit

class CaseCell: UICollectionViewCell {
    
    
    @IBOutlet weak var caseTitleLbl: UILabel!
    
    @IBOutlet weak var caseSizeLbl: UILabel!
    
}
