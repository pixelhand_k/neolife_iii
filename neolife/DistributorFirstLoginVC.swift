//
//  DistributorFirstLoginVC.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/07/02.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//

import UIKit
import Firebase

class DistributorFirstLoginVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var idTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var proceedButton: UIButton!
    @IBOutlet weak var guestButton: UIButton!
    var username = ""
    var email = ""
    
    var activityView = UIView()
    var activityIndicator = UIActivityIndicatorView()
    var messageLabel = UILabel()
//    let effectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
    let loadDistributors = CallFirebaseNode("distributors", storedIn: nil)

//    MARK: - Load Functions
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        createIndicator()
        showIndicator()
//        Distributors().allDistributors()
        loadDistributors.getClass()
        hideIndicator()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        createIndicator()
        idTextField.delegate = self
        
        //add to view extension
        let tap : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(DistributorFirstLoginVC.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
//        log users out if this page is present
        do {
            try Auth.auth().signOut()
            
        } catch let error as NSError {
            print("Error signing out: \(error.localizedDescription)")
        }
//        findDistributor()
//        findUsers()
    }

    @objc func dismissKeyboard() {
        idTextField.endEditing(true)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "userExistsSegue" {
            let vc = segue.destination as! LoginVC
            vc.emailString = email
            vc.distributorName = username
            UserFunctions.saveIdToUserDefaultsWith(id: idTextField.text!)
        } else if segue.identifier == "userNewSegue" {
            let vc = segue.destination as! RegistrationVC
            vc.idNumber = idTextField.text!
//            vc.email = emailTextField.text!
            vc.username = username
            UserFunctions.saveIdToUserDefaultsWith(id: idTextField.text!)
        }
        
    }

    @IBAction func proceedBTn(_ sender: UIButton) {
        //run validate email function here - later
        self.createUserIndicator()
        if idTextField.text == "" {
            print("Please enter a valid email. Or is your ID field empty?")
            Alerts.alert(on: self, title: "ID Not Found", and: "Please enter a valid email. Or is your ID field empty?")
        } else {
            isUserRegistered(withID: idTextField.text!)
        }
    }
    
    
//    MARK: - Backend functions
    //finds the Neolife ID and uses that to determine if they are distributors
    func isUserRegistered(withID id: String) {
        self.showIndicator()
        let ref = Database.database().reference(fromURL: "https://neolife-d9754.firebaseio.com/")
        ref.child("users").child(id).observeSingleEvent(of: .value, with: { (snap) in
            if snap.exists() == false {
//                self.findDistributor(withID: id) // finds their details and segues to registration page
                self.distributor(withID: id)
            } else {
                print("User exist")
                self.hideIndicator()
                guard let value = snap.value as? [String : AnyObject] else {
                    return
                }
                
                self.username = value["name"] as! String
                self.email = value["email"] as! String
                UserFunctions.saveIdToUserDefaultsWith(id: id)
                self.performSegue(withIdentifier: "userExistsSegue", sender: nil)
            }
            
        }, withCancel: nil)
    }
    

    func distributor(withID id: String) {
        print("Started searching for distributor..")
        let ref = Database.database().reference(fromURL: "https://neolife-d9754.firebaseio.com/")
        ref.child("distributors").queryOrdered(byChild: "id_number").queryEqual(toValue: String.toInt(string: id)).queryLimited(toFirst: 1).observeSingleEvent(of: .childAdded, with: { (snap) in
            
            if snap.exists() == true {
                if let dict = snap.value as? [String: AnyObject] {
                    let distributorName = dict["Name"] as? String

                    if let name = distributorName {
                        self.username = name
                        UserFunctions.saveIdToUserDefaultsWith(id: id)
                        self.hideIndicator()
                        self.performSegue(withIdentifier: "userNewSegue", sender: nil)
                    }
                }
            } else {
                self.hideIndicator()
                Alerts.alert(on: self, title: "ID Not Recognised", and: "Please enter correct Distributor Neolife ID, perhaps enter using guest login?")
            }
        }, withCancel: nil)
    }

    
//    MARK: - Load Indicator
    
    func createUserIndicator() {
        activityView.frame = CGRect(x: 0.0, y: 0.0, width: 160, height: 160)
        activityView.layer.cornerRadius = activityView.frame.width/2
        activityView.center = self.view.center
        activityView.backgroundColor = UIColor.init(white: 0.5, alpha: 0.5)
        activityView.alpha = 0.0
        
        activityIndicator.center = CGPoint(x: activityView.frame.width/2, y: activityView.frame.height/2)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = .whiteLarge
        
        self.view.addSubview(self.activityView)
        self.activityView.addSubview(self.activityIndicator)
        
        UIView.animate(withDuration: 0.6) {
            self.activityView.alpha = 1
        }
    }
    
    func createIndicator() {
        activityView.frame = CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.view.frame.height)
        activityView.backgroundColor = .lightGray
        
        view.addSubview(activityView)
        

        messageLabel.textColor = UIColor.white
        messageLabel.text = "Loading Content, Please Wait..."
        messageLabel.font = UIFont(name: "Helvetica Neue", size: 17)
        messageLabel.sizeToFit()
        messageLabel.center = CGPoint(x: activityIndicator.center.x + view.frame.width/2, y: activityIndicator.center.y + view.frame.height/2.5)
        activityView.addSubview(messageLabel)
        
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = .white
        activityIndicator.backgroundColor = UIColor.lightGray
        activityView.addSubview(activityIndicator)
    }
    
    func showIndicator()  {
        activityIndicator.startAnimating()
    }
    
    func hideIndicator() {
        activityIndicator.stopAnimating()
        activityIndicator.removeFromSuperview()
        activityView.removeFromSuperview()
    }
    
    /*func activityIndicatorWith(message: String) {
        
        messageLabel.removeFromSuperview()
        activityIndicator.removeFromSuperview()
        effectView.removeFromSuperview()
        
        messageLabel = UILabel(frame: CGRect(x: 50, y: 0, width: 160, height: 50))
        messageLabel.text = message
        messageLabel.textColor = .white
//        messageLabel.font = UIFont(name: <#T##String#>, size: <#T##CGFloat#>)
        
        effectView.frame = CGRect(x: view.frame.midX - messageLabel.frame.width/2, y: view.frame.midY - messageLabel.frame.height/2 , width: 200, height: 100)
        effectView.layer.cornerRadius = 15
        effectView.layer.masksToBounds = true

        activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.height)) as UIActivityIndicatorView
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = .white
        activityIndicator.backgroundColor = UIColor.lightGray
        
//        activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.height)) as UIActivityIndicatorView
//        activityIndicator.center = self.view.center
//        activityIndicator.hidesWhenStopped = true
//        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .white)
//        activityIndicator.frame = CGRect(x: 0, y: 0, width: 46, height: 46)
        activityIndicator.startAnimating()
        
        effectView.contentView.addSubview(activityIndicator)
        effectView.contentView.addSubview(messageLabel)
        self.view.addSubview(effectView)
    }*/
    
    
    
    
}
