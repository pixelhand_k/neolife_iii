//
//  HelpPopup.swift
//  neolife
//
//  Created by Karabo Moloi on 2019/03/07.
//  Copyright © 2019 Karabo Moloi. All rights reserved.
//

import UIKit

class HelpPopup {
    static func helpInfoWith(_ message: String, _ button: UIBarButtonItem, in controller: UIViewController) {
        //        instantiate vc from storyboard
        
        //add if sattement with conditions of what storyboard is being used so that you can put both vc variables in this fucntion
        guard let vc = UIStoryboard(name: "Product", bundle: nil).instantiateViewController(withIdentifier: "cartInfo") as? CartInfoVC else {
            print("didnt work")
            return
        }

        //        set content size
        vc.preferredContentSize = CGSize(width: controller.view.frame.width / 1.2, height: controller.view.frame.height / 4)
        
        //        set info
        vc.message = message
        vc.title = "What happens here?"
        
        //        root vc navigation controller
        let rootController = UINavigationController(rootViewController: vc)
        
        //        set modal presentation style
        rootController.modalPresentationStyle = .popover
        
        //        set self as delegate
        let popover = rootController.popoverPresentationController
        popover?.delegate = controller.self as? UIPopoverPresentationControllerDelegate
        popover?.barButtonItem = button
        popover?.backgroundColor = UIColor.greenNeolife()
        
        //        present controller
        controller.present(rootController, animated: true, completion: nil)
    }
}
