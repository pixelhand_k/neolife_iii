//
//  RegistrationVC.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/07/04.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//

import UIKit
//import Parse
import Firebase

class RegistrationVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var promptLabel: UILabel!
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    var idNumber = "" //convert to int
    var username : String?
    var promptText : String?
    let ref = Database.database().reference(fromURL: "https://neolife-d9754.firebaseio.com/")
    var userDetails = [User]()
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let loading = Loading(fromView: self)
        //Loads details of users
//        getDistributor()
        loading.createIndicator()
        loading.showIndicator()
        getDistributorDetails()
        loading.hideIndicator()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
//        findDistributor()
        usernameField.isEnabled = true
        usernameField.text = ""
        usernameField.delegate = self
        passwordField.delegate = self
        promptText = "Hello \(String(describing: username!)), We see you are new to the NeoLife Africa App. Please enter your valid Email address and a password, and get started with the NeoLife South Africa App"
        promptLabel.text = promptText
        
        //add to view extension
        let tap : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(RegistrationVC.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)        
    }

    @objc func dismissKeyboard() {
        usernameField.endEditing(true)
        passwordField.endEditing(true)
    }
    
//    MARK: - Backend Function
    
    func signupUser(withEmail email: String?, _ password: String?){
       
        guard let userEmail = email, let username = email, let userPassword = password else {
                return
        }
        
        Auth.auth().createUser(withEmail: userEmail, password: userPassword) { (user, error) in
            if error != nil {
                Alerts.alert(on: self, title: "Error Registering", and: "\(String(describing: error?.localizedDescription))")
                return
            }
            
            guard let propic = self.userDetails[0].profileImgUrl else {
                return
            }
            
            guard let invoiceCount = self.userDetails[0].invoiceCount else {
                return
            }
            
            guard let name = self.userDetails[0].name else {
                return
            }
            
            guard let gpv = self.userDetails[0].gpv else {
                return
            }
            
            guard let idNum = self.userDetails[0].id else {
                return
            }
            
            guard let isMember = self.userDetails[0].is_member else {
                return
            }
            
            guard let isDistributor = self.userDetails[0].is_distributor else {
                return
            }
            
            guard let address = self.userDetails[0].address else {
                return
            }
            
            guard let ppv = self.userDetails[0].ppv else {
                return
            }
            
            guard let isProfileSet = self.userDetails[0].is_profile_set else {
                return
            }
            
            guard let qpv = self.userDetails[0].qpv else {
                return
            }
            
            guard let title = self.userDetails[0].title else {
                return
            }
            
            let properties = ["username" : username,
                              "email": userEmail,
                              "profileImgUrl" : propic,
                              "cc": self.userDetails[0].cc,
                              "invoiceCount" : invoiceCount,
                              "name" : name,
                              "gpv" : gpv,
                              "id_number" : idNum,
                              "is_member" : isMember,
                              "is_distributor": isDistributor,
                              "address": address,
                              "ppv": ppv,
                              "is_profile_set": isProfileSet,
                              "qpv" : qpv,
                              "title" : title] as [String : Any]
            
            self.registerUserWith(uid: String.fromInt(integer: self.userDetails[0].id), values: properties)
        }
     }
    
    
    func getDistributorDetails() {
        let ref = Database.database().reference(fromURL: "https://neolife-d9754.firebaseio.com/")
        let userProp = User()
        ref.child("distributors").queryOrdered(byChild: "id_number").queryEqual(toValue: String.toInt(string: idNumber)).queryLimited(toFirst: 1).observeSingleEvent(of: .value, with: { (snap) in
            if let dict = snap.value as? [String : AnyObject] {
                userProp.name = dict["Name"] as? String
                userProp.title = dict["Title"] as? Int
                userProp.id = dict["id_number"] as? Int
                userProp.gpv = dict["gpv"] as? Int
                userProp.ppv = dict["ppv"] as? Int
                userProp.qpv = dict["qpv"] as? Int
                userProp.is_distributor = true
                userProp.is_member = false
                userProp.profileImgUrl = ""
                userProp.cc = "26"
                userProp.invoiceCount = 0
                userProp.address = ""
                userProp.is_profile_set = false
                
                self.userDetails.append(userProp)
            }
        }, withCancel: nil)
    }
    
    /*
    func getDistributor() {
//        let user = PFUser.current()
        let userProp = User()
        let query = PFQuery(className: "distributors")
        query.whereKey("id_number", equalTo: String.toInt(string: idNumber))
        
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
//                print("OBJECT: \(String(describing: objects))")
                
                for object in objects! {
                    userProp.name = object["name"] as? String
                    userProp.title = object["Title"] as? Int
                    userProp.id = object["id_number"] as? Int
                    userProp.gpv = object["GPV"] as? Int
                    userProp.ppv = object["PPV"] as? Int
                    userProp.qpv = object["QPV"] as? Int
                    userProp.is_distributor = true
                    userProp.is_member = false
                    userProp.profileImgUrl = ""
                    userProp.cc = "26"
                    userProp.invoiceCount = 0
                    userProp.address = ""
                    userProp.is_profile_set = false
                    
                    
                    self.userDetails.append(userProp)
                    
                    /*let name = object["name"] as! String
                    let title = object["Title"] as! Int
                    let id = object["id_number"] as! Int
                    let cc = object["cc"] as! Int
                    let gpv = object["GPV"] as! Int
                    let ppv = object["PPV"] as! Int
                    let qpv = object["QPV"] as! Int
                    
                    user?["Name"] = name
                    user?["Title"] = String.fromInt(integer: title)
                    user?["id_number"] = String.fromInt(integer: id)
                    user?["cc"] = String.fromInt(integer: cc)
                    user?["GPV"] = String.fromInt(integer: gpv)
                    user?["PPV"] = String.fromInt(integer: ppv)
                    user?["QPV"] = String.fromInt(integer: qpv)
                    
                    if title >= 29 {
                        user?["is_distributor"] = true
                        user?["is_member"] = false
                    } else {
                        user?["is_distributor"] = false
                        user?["is_member"] = true
                    }*/
                    
                    
                    //                                self.user?.saveEventually()
//                    user?.saveInBackground()
                }
                
            } else {
                print("Error: \(String(describing: error?.localizedDescription))")
                Alerts.alert(on: self, title: "Error", and: (error?.localizedDescription)!)
            }
        })
    }
    */
    
    @IBAction func registerBtn(_ sender: UIButton) {
        
        if passwordField.text != "" && usernameField.text != "" {
            signupUser(withEmail: usernameField.text, passwordField.text)
//            redeemDistributor()
        } else {
            Alerts.alert(on: self, title: "No password", and: "Please put in a password")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "newRegisteredSegue" {
            let tabController = segue.destination as! UITabBarController
            let navController = tabController.viewControllers?[0] as! UINavigationController
            let homeVC = navController.topViewController as! HomeCollectionViewController
            
            homeVC.isFirstTime = true
        }
    }
}

extension RegistrationVC {
    
    func registerUserWith(uid: String, values: [String: Any]) {
        let userRef = ref.child("users").child(uid)
        
        userRef.updateChildValues(values) { (error, ref) in
            if error != nil {
                print("Error with registration: \(String(describing: error?.localizedDescription))")
                return
            }
            
            self.performSegue(withIdentifier: "newRegisteredSegue", sender: nil)
        }
    }
    
}




