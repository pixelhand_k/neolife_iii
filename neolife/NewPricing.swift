//
//  newPricing.swift
//  neolife
//
//  Created by Karabo Moloi on 2018/03/04.
//  Copyright © 2018 Karabo Moloi. All rights reserved.
//

import UIKit
import Firebase

class NewPricing {
    static func getPrices() {
        let ref = Database.database().reference(fromURL: "https://neolife-d9754.firebaseio.com/")
        ref.child("pricelist").observe(.childAdded, with: { (snapshot) in
            if let dict = snapshot.value as? [String: AnyObject] {
//                print("NEW PRICELIST: \(dict)")
                let pricelist = Price()
                pricelist.caseBv = dict["caseBv"] as? String
                pricelist.casePv = dict["casePv"] as? String
                pricelist.caseDp = dict["caseDp"] as? String
                pricelist.caseMp = dict["caseMp"] as? String
                pricelist.caseSrp = dict["caseSrp"] as? String
                pricelist.singleBv = dict["singleBv"] as? String
                pricelist.singlePv = dict["singlePv"] as? String
                pricelist.singleDp = dict["singleDp"] as? String
                pricelist.singleMp = dict["singleMp"] as? String
                pricelist.singleSrp = dict["singleSrp"] as? String
                pricelist.units = dict["units"] as? String
                
            }
        })
        print("4. New Pricelist")
    }
}

