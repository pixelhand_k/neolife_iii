//
//  CsvComposer.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/07/26.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//

import UIKit
import CoreData

class CsvComposer: NSObject {
    
    var invoiceNum : String!
    var email : String!
    var filePath : String!
    var fileName = String()
    
    
    override init() {
        super.init()
    }
    
    func createCSV(with products: [NSManagedObject], addressOne: String, addressTwo: String, addressThree: String, code: String, DistributorName: String, neolifeID: String, email: String, invoiceNum: String, subtotal: String, referenceNum: String, paymentType: String) -> String {
        
//        var csvHeader = "Code|Product|Price|Qty|UnitPrice|Subtotal|NeolifeID|Email|DistributerName|DistributorAddress\n"
        
        self.email = email
        self.invoiceNum = invoiceNum
        let date = AppDelegate.getAppDelegate().currentDate()
//        let addressNoComma = address.replacingOccurrences(of: ",", with: " ")
        let subtotalNoComma = subtotal.replacingOccurrences(of: ",", with: "")
        let orderNum = referenceNum.replacingOccurrences(of: "NeoApp", with: "")
        let orderSource = "iOS"
        let mobileNum = ""
        var shipping = 60
        
        //totalVat calculation
        let subTotalFloat = Float(subtotalNoComma)
        let vatPercentage : Float = 15/100; print("%: \(vatPercentage)")
        let addedVAT = subTotalFloat! * vatPercentage
        
        let totalBeforeTax = subTotalFloat! - addedVAT
        
        var totalPv = 0
        for index in 0..<products.count {
            let itemArray = products[index]
            
            let pv = itemArray.value(forKey: "pv") as? String
            let pvInt = String.toInt(string: pv!)
            
            totalPv += pvInt
        }
        
        if totalPv >= 100 {
            shipping = 0
        } else {
            shipping = 60
        }
//        print("TOTAL VAT: \(addedVAT) @ 14%, Total: \(subtotalNoComma)")
//        var csvHeader = "Code|Product|Price|Qty|Type|SubTotal|NeolifeID|Email|DistributerName|DistributorAddress\n"
//        var csvHeader = "OrderDate|NeolifeID|DistributorName|DeliveryAddress|ProductCode|ProductName|Quantity|UOM|UnitPrice|PaymentType|ReferenceNumber\n"
        
//        var header = "\(date)|\(neolifeID)|\(email)|\(DistributorName)|\(paymentType)|\(subtotalNoComma)|\(referenceNum)|\(addressNoComma)\n"
        
        var header = "H|\(date)|\(orderNum)|\(neolifeID)|\(email)|\(mobileNum)|\(orderSource)|\(DistributorName)|\(paymentType)|\(String.fromFloat(totalBeforeTax))|\(String.fromInt(integer: shipping))|\(String.fromFloat(addedVAT))|\(subtotalNoComma)|\(addressOne)|\(addressTwo)|\(addressThree)|\(code)\n"
        
        for index in 0..<products.count {
            let itemArray = products[index]
            
            let code = String.fromInt(integer: itemArray.value(forKey: "code") as? Int)
            let name = itemArray.value(forKey: "name") as? String
            let price = itemArray.value(forKey: "price") as? String
            let qty = itemArray.value(forKey: "numItems") as? String
            let type = itemArray.value(forKey: "type") as? String
            let total = itemArray.value(forKey: "total") as? String
            let pv = itemArray.value(forKey: "pv") as? String
            let bv = itemArray.value(forKey: "bv") as? String
            
            let rawValueTotal = total?.replacingOccurrences(of: "R", with: "")
            let priceNoComma = price?.replacingOccurrences(of: ",", with: "")
            
            let totalInt = Float(rawValueTotal!)
            
            let vatPercentage : Float = 15/100
            let addedVAT = totalInt! * vatPercentage
            let exclVAT = totalInt! / (vatPercentage + 1)
            
            let vat = String.fromFloat(addedVAT)
            let excl = String.fromFloat(exclVAT)
            
            print("VAT: \(addedVAT) & EXCL: \(exclVAT)")
            
            if name != "Delivery" {
                header.append("D|\(code)|\(name!)|\(qty!)|\(type!)|\(pv!)|\(bv!)|\(priceNoComma!)|\(excl)|\(vat)|\(rawValueTotal!)\n")
            }
        }
        
        print("CSV: \(header)")
        
        return header
    }
    
    func exportCSV(_ csv: String) -> NSURL {
        
        fileName = "NeolifeApp\(invoiceNum!).csv"
//        let filePath = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(fileName)
        filePath = NSTemporaryDirectory() + fileName
        let fileUrl = NSURL(fileURLWithPath: filePath)
        
        do {
            try csv.write(to: fileUrl as URL, atomically: true, encoding: .utf8)
            
        } catch {
            print("Error")
        }

        return fileUrl
    }

}
