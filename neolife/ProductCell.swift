//
//  ProductCell.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/01/24.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//

import UIKit

class ProductCell: UITableViewCell {
    
//    MARK: - Outlets
    
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var uspLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
/*    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.addProductBtn.layer.borderColor = UIColor(colorLiteralRed: 97/255, green: 171/255, blue: 34/255, alpha: 1.0).cgColor
        self.addProductBtn.layer.borderWidth = 0.5
        self.addProductBtn.layer.cornerRadius = 5
        self.addProductBtn.backgroundColor = UIColor.white
        
        self.addProductBtn.layer.shadowColor = UIColor.black.cgColor
        self.addProductBtn.layer.shadowRadius = 1.5
        self.addProductBtn.layer.shadowOpacity = 0.5
        self.addProductBtn.layer.shadowOffset.width = 0
        self.addProductBtn.layer.shadowOffset.height = 0
        
    }
   */ 
    

}
