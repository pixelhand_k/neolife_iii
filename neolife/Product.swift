//
//  Product.swift
//  neolife
//
//  Created by Karabo Moloi on 2018/11/01.
//  Copyright © 2018 Karabo Moloi. All rights reserved.
//

import UIKit

class Product {
    var name : String!
    var usp : String?
    var imageUrl : String!
    var code : Int!
    
}
