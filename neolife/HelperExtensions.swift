//
//  UIFunctions.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/04/19.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//

import UIKit
import Parse
import Firebase


extension Date {
    static func toMilliseconds() -> String {
        let millisec = Int64(Date().timeIntervalSince1970 * 1000)
        
        return String(millisec)
    }
}

extension Bool {
    static func validateEmail(candidate: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: candidate)
    }
}

extension UIColor {
    static func greenNeolife() -> UIColor {
        return UIColor(red: 97/255, green: 171/255, blue: 34/255, alpha: 1.0)
    }
    
    static func paleGrey() -> UIColor {
        return UIColor(red: 153/255, green: 153/255, blue: 153/255, alpha: 1.0)
    }
}

extension String {
    static func fromInt(integer: Int?) -> String {
        var value = ""
        
        if let param = integer {
            value = "\(param)"
        }
        
        return value
    }
    
    static func toInt(string: String) -> Int {
        var value = 0
        
        let noCommaString = string.replacingOccurrences(of: ",", with: "")
        let noZaString = noCommaString.replacingOccurrences(of: "ZA", with: "")
//        let noSpaceString = noZaString.replacingOccurrences(of: " ", with: "")
//        let stringToFloat = Float(noSpaceString)
        let stringToFloat = NSString(string: noZaString).floatValue
        
        value = Int(stringToFloat)
        
        return value
    }
    
    static func toIntFromFloat(_ integer: Int) -> Float {
        return Float(integer)
    }
    
    static func toIntFromString(_ string: String) -> Int? {
        return Int(string)
    }

    func toFloatFromString(_ string: String) -> Float {
        
        let noZaString = string.replacingOccurrences(of: "ZA", with: "")
        let commaToDot = noZaString.replacingOccurrences(of: ",", with: ".")
        let stringToFloat = Float(commaToDot)
        
        return stringToFloat!
    }
    
    static func fromFloat(_ number: Float) -> String {
        return String(number)
    }
    
    static func fromCurrency(_ value: String) -> Int{
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencyCode = AppDelegate().currencyCode
        
        var num = 0
        
        if let number = formatter.number(from: value) {
            let val = number.intValue
            print("from curr: \(val)")
            num = val
        }
        return num
    }
    
    
    //separates words in a string
    var words: [String] {
        var words:[String] = []
        enumerateSubstrings(in: startIndex..<endIndex, options: .byWords) {
            guard let word = $0 else {
                return
            }
            print($1,$2,$3)
            words.append(word)
        }
        return words
    }
    func firstWords(_ max: Int) -> [String] {
        return Array(words.prefix(max))
    }
    var firstWord: String {
        return words.first ?? ""
    }
    func lastWords(_ max: Int) -> [String] {
        return Array(words.suffix(max))
    }
    var lastWord: String {
        return words.last ?? ""
    }
    
    var wordCount: Int {
        return words.count
    }
}

extension UIFont {
    static func helveticaNeueContentDetail() -> UIFont {
        return UIFont(name: "HelveticaNeue", size: 12)!
    }
    
    static func helveticaNeueContentTitle() -> UIFont {
        return UIFont(name: "HelveticaNeue-Bold", size: 12)!
    }
    
    static func helveticaNeueProductTitle() -> UIFont {
        return UIFont(name: "HelveticaNeue-Bold", size: 15)!
    }
}




//extension UIViewController {
    //    MARK: - Storage to Firebase - later
//    static func uploadUserOrderIf(isSent: Bool, with invoiceNumber: String, invoiceComposer: InvoiceComposer, csvComposer: CsvComposer) {
//        let orderName = invoiceNumber
//        let neolifeId = UserFunctions.getNeolifeId()
//
//        let pdfRef = Storage.storage().reference().child("orders").child("\(orderName).pdf")
//        let csvRef = Storage.storage().reference().child("orders").child("\(orderName).csv")
//
//        // time and date
//        let date = Date()
//        let dateFormatter = DateFormatter()
//        let timeFormatter = DateFormatter()
//        dateFormatter.dateFormat = "yyyy/MM/dd"
//        dateFormatter.timeZone = TimeZone(identifier: "en_ZA")
//        dateFormatter.locale = Locale(identifier: "en_ZA")
//        timeFormatter.timeStyle = .short
//        timeFormatter.locale = Locale(identifier: "en_ZA")
//        timeFormatter.timeZone = TimeZone(identifier: "en_ZA")
//
//        let time = timeFormatter.string(from: date)
//        let today = dateFormatter.string(from: date)
//
//        invoiceComposer.exportHTMLContentToPDF(HTMLContent: htmlContent) // converst html string to pdf and saves it
//
//        if let orderPdf = try? NSData(contentsOfFile: invoiceComposer.pdfFileName) as Data {
//            if let csvData = try? NSData(contentsOf: csvComposer.exportCSV(csvContent) as URL) as Data {
//
//                pdfRef.putData(orderPdf, metadata: nil) { (pdfMeta, error) in
//                    if error != nil {
//                        print("PDF SAVE Error: \(error)")
//                        return
//                    }
//
//                    csvRef.putData(csvData, metadata: nil, completion: { (csvMeta, error) in
//                        if error != nil {
//                            print("CSV SAVE Error: \(error)")
//                            return
//                        }
//
//                        if let pdfurl = pdfMeta?.downloadURL()?.absoluteString {
//                            if let csvurl = csvMeta?.downloadURL()?.absoluteString {
//
//                                self.saveOrder(using: self.invoiceNumber, total: self.grossTotal, time: time, date: today, isSent: isSent, neolifeID: neolifeId, csv: csvurl, pdf: pdfurl)
//                            }
//                        }
//                    })
//                }
//            }
//        }
//    }
//}



//    func dynamicCollectionCell() -> CGSize {
//        //sets the cell sizes according to the screen size, so it can dynamically adjust
//        var cellNum : CGFloat = 0
//        let heightDiff : CGFloat = 30
//        let sidePadding : CGFloat = 30
//        let iphoneSeScreenWidth : CGFloat = 320
//        let screenWidth = collectionView?.frame.width
//        var width: CGFloat = 0
//        var height: CGFloat = 0
////        let width = ((collectionView?.frame.width)! - sidePadding) / numberOfCells
////        let height = width + heightDifference
//        if screenWidth! <= iphoneSeScreenWidth {
//            cellNum = 2.0
//            width = (screenWidth! - sidePadding) / cellNum
//            height = width + heightDiff
////            result = CGSize(width: width, height: height)
//            return CGSize(width: width, height: height)
//        } else {
//            cellNum = 3.0
//            width = (screenWidth! - sidePadding) / cellNum
//            height = width + heightDiff
////            result = CGSize(width: width, height: height)
//            return CGSize(width: width, height: height)
//        }
//    }
//

