//
//  DownloadFile.swift
//  neolife
//
//  Created by Karabo Moloi on 2018/09/05.
//  Copyright © 2018 Karabo Moloi. All rights reserved.
//

import UIKit
import Crashlytics

class Downloader {
    class func fileFromUrl(url: String, completion: @escaping (Data) -> Void) {
        let stringUrl = URL(string: url)
        
        guard let fileUrl = stringUrl else {
            print("NO URL")
            return
        }
        
        let session = URLSession.shared.dataTask(with: fileUrl) { (data, response, error) in
            if error != nil {
//                let resp = (response as! HTTPURLResponse).statusCode
                CLSLogv("%@", getVaList([error.debugDescription, response!]))

            } else {
                guard let file = data else {
                    return
                }
                completion(file)
            }
        }
        
        session.resume()
    }
}
