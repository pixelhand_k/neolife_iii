//
//  Order.swift
//  neolife
//
//  Created by Karabo Moloi on 2018/08/31.
//  Copyright © 2018 Karabo Moloi. All rights reserved.
//

import UIKit

class Order: NSObject {
    var invoice: String?
    var orderNum: String?
    var isSent: Bool!
    var total: String!
    var date: String?
    var csv: String?
    var time: String?
    var paymentType: String!
}
