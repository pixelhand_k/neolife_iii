//
//  InvoiceComposer.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/07/01.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//

import UIKit
//import Parse
import Firebase
import CoreData

class InvoiceComposer: NSObject {

    var currentInvoiceNo : Int!
    
    //    find the html templates in the main bundle
    let invoiceHTMLPath = Bundle.main.path(forResource: "invoice", ofType: "html")
    
    let singleItemHTMLPath = Bundle.main.path(forResource: "single_item", ofType: "html")
    
    let lastItemHTMLPath = Bundle.main.path(forResource: "last_item", ofType: "html")
    
    //    constant properties that will be on every doc
    
//    let senderInfo = "Distributer Name<br>123 Somewhere Str.<br>10000 - MyCity<br>MyCountry"
    
    let dueDate = ""

    let logoImageURL = "http://static.neolife.com.s3.amazonaws.com/market/us/assets/images/NeoLife_logo_noBox_wo_R.jpg"
    
    //    variable properties that will show the invoice num and pdf name
    
    var invoiceNum : String!
    
    var pdfFileName : String!
    
//    initialise the objecjt class
    override init() {
        super.init()
    }
    
//    enters info to invoice where placeholders are in the html then returns html as a string
    func infoToInvoice(invoiceNum: String, date: String, address: String, recipientInfo: String, items: [NSManagedObject], totalAmount: String, method: String) -> String! {
        
        currentInvoiceNumber()
        self.invoiceNum = invoiceNum
        
        do {
//            convert html to string
            var HTMLContent = try String(contentsOfFile: invoiceHTMLPath!)
            
//            let imageData : NSData = UIImagePNGRepresentation(#imageLiteral(resourceName: "NeoLife_logo_noBox"))! as NSData
//            let stringBase64  = imageData.base64EncodedData(options: .lineLength64Characters)
            // Replace all the placeholders with real values except for the items.
            // The logo image.
            HTMLContent = HTMLContent.replacingOccurrences(of: "#LOGO_IMAGE#", with: logoImageURL)
//            HTMLContent = HTMLContent.replacingOccurrences(of: "#LOGO_IMAGE#", with: stringBase64)
            
            // Invoice number.
            HTMLContent = HTMLContent.replacingOccurrences(of: "#INVOICE_NUMBER#", with: invoiceNum)
            
            // Invoice date.
            HTMLContent = HTMLContent.replacingOccurrences(of: "#INVOICE_DATE#", with: date)
            
            // Due date (we leave it blank by default).
            HTMLContent = HTMLContent.replacingOccurrences(of: "#DUE_DATE#", with: dueDate)
            
            // Sender info.
            HTMLContent = HTMLContent.replacingOccurrences(of: "#SENDER_INFO#", with: address)
            
            // Recipient info.
            HTMLContent = HTMLContent.replacingOccurrences(of: "#RECIPIENT_INFO#", with: recipientInfo.replacingOccurrences(of: "\n", with: "<br>"))
            
            // Payment method.
            HTMLContent = HTMLContent.replacingOccurrences(of: "#PAYMENT_METHOD#", with: method)
            
            // Total amount.
            HTMLContent = HTMLContent.replacingOccurrences(of: "#TOTAL_AMOUNT#", with: totalAmount)
            
//############################ PLACING ITEMS USING THE CORRECT TEMPLATE ###############################################
            
            //items object array
            var cartItems = ""
            
            for item in 0..<items.count {
                
                var itemHTMLContent : String!
                
                
                // Determine the proper template file.
                
                if item != items.count - 1 {
                    itemHTMLContent = try String(contentsOfFile: singleItemHTMLPath!)
                }
                else {
                    itemHTMLContent = try String(contentsOfFile: lastItemHTMLPath!)
                }
                
                let itemsArray = items[item]
                // Replace the description and price placeholders with the actual values.
//                itemHTMLContent = itemHTMLContent.replacingOccurrences(of: "#ITEM_DESC#", with: items[i]["item"]!)
                let itemName = itemsArray.value(forKey: "name") as! String
                let numItems = itemsArray.value(forKey: "numItems") as! String
                let itemCode = String.fromInt(integer: itemsArray.value(forKey: "code") as? Int)
                let productType = itemsArray.value(forKey: "type") as! String
                
                itemHTMLContent = itemHTMLContent.replacingOccurrences(of: "#ITEM_DESC#", with: itemCode + " " + itemName + " - " + productType + " - Qty: \(numItems)")
                
                // Format each item's price as a currency value.
//                let formattedPrice = AppDelegate.getAppDelegate().getStringValueFormattedAsCurrency(itemsArray.value(forKey: "price") as? String)
                print("Formated price: \(itemsArray.value(forKey: "price") as! String)")
                let priceFromCD = itemsArray.value(forKey: "price") as! String
                let fromCDFormatted = priceFromCD.replacingOccurrences(of: ",", with: "")
                let formattedPrice = AppDelegate.getAppDelegate().getStringValueFormattedAsCurrency(fromCDFormatted)
//                let formattedPrice = itemsArray.value(forKey: "price") as! String
                itemHTMLContent = itemHTMLContent.replacingOccurrences(of: "#PRICE#", with: formattedPrice)
                
                // Add the item's HTML code to the general items string.
                cartItems += itemHTMLContent
            }
            
            // Set the items from templates to the main invoice template where the items party is #commented#.
            HTMLContent = HTMLContent.replacingOccurrences(of: "#ITEMS#", with: cartItems)
            
            // The HTML code is ready.
            return HTMLContent
            
        } catch  {
            print("Unable to use PDF/HTML")
        }
        
        return nil
    }
    
//    MARK: - Export HTML to PDF
    func exportHTMLContentToPDF(HTMLContent: String) {
        let printPageRenderer = PrintRenderer()
        
        let printFormatter = UIMarkupTextPrintFormatter(markupText: HTMLContent)
        //        printPageRenderer.addPrintFormatter(printFormatter, startingAtPageAtIndex: 0)
        printPageRenderer.addPrintFormatter(printFormatter, startingAtPageAt: 0)
        
        let pdfData = drawPDFUsingPrintPageRenderer(printPageRenderer)
        
        pdfFileName = "\(AppDelegate.getAppDelegate().getDocDir())/Invoice\(invoiceNum!).pdf"
        pdfData?.write(toFile: pdfFileName, atomically: true)
        
        incrementInvoiceNumber()
        
        print(pdfFileName)
    }
    
    /// At first we initialise a mutable data object, so the PDF output data can be written to it. That’s something we actually dictate the code to do upon the creation of the PDF graphics context
    func drawPDFUsingPrintPageRenderer(_ printPageRenderer: UIPrintPageRenderer) -> NSData! {
        
        //where the pdf can be written to
        let data = NSMutableData()
        
        UIGraphicsBeginPDFContextToData(data, CGRect.zero, nil)
        
        UIGraphicsBeginPDFPage()
        
        //        the print page renderer object that comes as an argument to the method will draw its contents inside the frame of the PDF context
        printPageRenderer.drawPage(at: 0, in: UIGraphicsGetPDFContextBounds())
//        printPageRenderer.drawPage(at: <#T##Int#>, in: <#T##CGRect#>)
        
        UIGraphicsEndPDFContext()
        
        return data
    }
    
    func currentInvoiceNumber(){
        let neolifeID = UserFunctions.getNeolifeId()
        if Auth.auth().currentUser != nil {
            if neolifeID != "" {
                let ref = Database.database().reference(fromURL: "https://neolife-d9754.firebaseio.com/")
                ref.child("users").child(neolifeID).observeSingleEvent(of: .value, with: { (snap) in
                    if let value = snap.value as? [String : AnyObject] {
                        let count = value["invoiceCount"] as? Int
                        
                        self.currentInvoiceNo = count
                    }
                }, withCancel: nil)
            } else {
                print("ID NOT PRESENT")
            }
        }
    }
    
    func incrementInvoiceNumber(){
        if Auth.auth().currentUser != nil {
            let neolifeID = UserFunctions.getNeolifeId()
            
            if neolifeID != "" {
                let ref = Database.database().reference(fromURL: "https://neolife-d9754.firebaseio.com/")
                var invoice = currentInvoiceNo
                invoice = invoice! + 1
                
                ref.child("users").child(neolifeID).updateChildValues(["invoiceCount" : invoice!])
            } else {
                print("NO ID PRESENT")
            }
        }
    }
}
