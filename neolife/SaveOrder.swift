//
//  SaveOrders.swift
//  neolife
//
//  Created by Karabo Moloi on 2018/09/06.
//  Copyright © 2018 Karabo Moloi. All rights reserved.
//

import UIKit
import Firebase

class SaveOrder {
//    class func saveOrder(invoiceNum: String, total: String, time: String, date: String, isSent: Bool, neolifeID: String, csv: String, pdf: String) {
//        let ref = Database.database().reference(fromURL: "https://neolife-d9754.firebaseio.com/")
//        let values = ["orderNum": invoiceNum, "invoice": pdf, "total": total, "time": time, "date": date, "isSent": isSent, "csv": csv, "neolifeID": neolifeID, "paymentType": "Online"] as [String : Any]
//        
//        ref.child("orders").child("\(invoiceNum)").updateChildValues(values)
//        print("Saved To Store")
//    }
    class func saveOrder(withTotal total: String, time: String, date: String, isSent: Bool, neolifeID: String, csv: String, pdf: String, image: String) {
        let ref = Database.database().reference(fromURL: "https://neolife-d9754.firebaseio.com/")
        var values = [String : Any]()
        
        if image != "" {
            values = ["orderNum": "\(Date.toMilliseconds())", "isDownloaded": false, "invoice": pdf, "total": total, "time": time, "date": date, "isSent": isSent, "csv": csv, "neolifeID": neolifeID, "paymentType": "Bank Deposit", "popImage": image] as [String : Any]
        } else {
            values = ["orderNum": "\(Date.toMilliseconds())", "isDownloaded": false, "invoice": pdf, "total": total, "time": time, "date": date, "isSent": isSent, "csv": csv, "neolifeID": neolifeID, "paymentType": "Online"] as [String : Any]
        }

        ref.child("orders").child("\(neolifeID)").child("\(Date.toMilliseconds())").updateChildValues(values)
        print("Saved To Store")
    }
    
}
