//
//  BagItems.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/01/31.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//

import Foundation
import UIKit
import Parse

class BagItem {
    
    var name : String
    var numItems : String
    var type: String?
    var total : String
    var owner : PFUser
    
    
    init(name: String, numItems: String, type: String?, total: String) {
        self.name = name
        self.numItems = numItems
        self.type = type
        self.total = total
        self.owner = PFUser.current()!
    }
}
