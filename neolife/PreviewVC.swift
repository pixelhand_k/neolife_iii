//
//  PreviewVC.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/07/01.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//

import UIKit
import CoreData
import MessageUI
import Firebase
import Crashlytics
import SendGrid


class PreviewVC: UIViewController, MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var pdfPreviewWebView: UIWebView!
    
    var checkoutProvider: OPPCheckoutProvider? //chekcout page
    var transaction: OPPTransaction?
    var paymentProvider: OPPPaymentProvider?
    var checkoutID = ""
    
    var cartInfo = [NSManagedObject]()
    
    var composer : InvoiceComposer!
    var csvComposer : CsvComposer!
    
    var htmlContent : String!
    
    //pass on total in segue to vc
    var grossTotal = ""
    var senderInfo = ""
    var recipientInfo = ""
    var invoiceNumber = ""
    var fullinvoiceNum = ""
    var method = ""
    var csvContent = ""
    var userEmail = ""
    var userID = ""
//    var addressNoBreaks = ""
    var subTotal = ""
    var addressOne = ""
    var addressTwo = ""
    var addressThree = ""
    var code = ""
    
//    MARK: - Methods Begin Here
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        load()
        invoiceToHTML()
        invoiceToCSV()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    func invoiceToCSV() {
        csvComposer = CsvComposer()
        
        csvContent = csvComposer.createCSV(with: cartInfo, addressOne: addressOne, addressTwo: addressTwo, addressThree: addressThree, code: code, DistributorName: recipientInfo, neolifeID: userID, email: userEmail, invoiceNum: invoiceNumber, subtotal: subTotal, referenceNum: "NeoApp" + invoiceNumber, paymentType: method)
    }

    /// This is where the info is put on the PDF
    func invoiceToHTML() {
        composer = InvoiceComposer()
        
        let date = AppDelegate.getAppDelegate().currentDate()
        
        if let invoiceHTML = composer.infoToInvoice(invoiceNum: invoiceNumber,
                                                    date: date,
                                                    address: senderInfo,
                                                    recipientInfo: recipientInfo,
                                                    items: cartInfo,
                                                    totalAmount: grossTotal,
                                                    method: method) {
            
            pdfPreviewWebView.loadHTMLString(invoiceHTML, baseURL: NSURL(string: composer.invoiceHTMLPath!)! as URL)
            htmlContent = invoiceHTML
        }
    }
    
    func totalBv() -> String {
        let array = cartInfo
        var points = 0
        
        for item in array {
            let bv = item.value(forKey: "bv") as! String
            let bvInt = String.toInt(string: bv)
            points += bvInt
        }
        
        return String.fromInt(integer: points)
    }
    
//    MARK: - IBActions
    @IBAction func closeVCButton(_ sender: UIBarButtonItem) {
        // alert user about cancelling
        Alerts.alert(self, "Are you sure?", "If you cancel, you will not be able to get back the money you have already paid for your order.") {
            self.deleteSelectedFromCD("Delivery")
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func exportToPDF(_ sender: UIBarButtonItem) {
//        toEmail()
        emailOrder()
    }
    
    
    //    MARK: - Core Data Functions
    func load() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchReq = NSFetchRequest<NSFetchRequestResult>(entityName: "ShoppingBag")
        
        //        let req = Cart.fetchRequest()
        
        do {
            let results = try managedContext.fetch(fetchReq)
            cartInfo = results as! [NSManagedObject]
        } catch let error as NSError {
            print("Could not load: \(error), \(error.userInfo)")
        }
    }
    
    //    MARK: - created function(s)

    func optionsAlert(){
        /*let alertController = UIAlertController(title: "Preview/Send", message: "Successfully printed PDF, What would you like to do?", preferredStyle: .alert)
        
        //        let preview = UIAlertAction(title: "Preview", style: .default) { (action) in
        //            let request = NSURLRequest(url: NSURL(string: self.invoiceComposer.pdfFileName)! as URL)
        //            self.webPreview.loadRequest(request as URLRequest)
        //        }
        
        let email = UIAlertAction(title: "Email", style: .default) { (action) in
            DispatchQueue.main.async {
                self.toEmail()
            }
        }
        
        
//        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
//            self.dismiss(animated: true, completion: nil)
//        }
        
        //        alertController.addAction(preview)
        alertController.addAction(email)
        alertController.addAction(cancel)
        self.present(alertController, animated: true, completion: nil)*/
    }
    
//    MARK: - Send Order
    
    func emailOrder() {
        print("Sending...")
        
        let personalization = Personalization(recipients: "ordera@za.neolife.com")
//        let personalization = Personalization(recipients: "moloikarabo3@gmail.com")
        let plainText = Content(contentType: ContentType.plainText, value: "Use your order number: \(invoiceNumber) as refrerence to make your payment")
        let htmlText = Content(contentType: ContentType.htmlText, value: "<h1>Use your order number: \(invoiceNumber) as refrerence to make your payment</h1>")
        
        let email = Email(
            personalizations: [personalization],
            from: Address(email: "no-reply@neolifeafrica.co.za"),
            content: [plainText, htmlText],
            subject: "NeoApp" + "\(invoiceNumber)"
        )
        
        //attachments
        composer.exportHTMLContentToPDF(HTMLContent: htmlContent)
        if let pdfData = NSData(contentsOfFile: composer.pdfFileName) {
            if let csvData = NSData(contentsOf: csvComposer.exportCSV(csvContent) as URL) {
                let pdfAttachment = Attachment(filename: "order\(invoiceNumber).pdf", content: pdfData as Data, disposition: .attachment, type: .pdf, contentID: nil)
                
                let csvAttachment = Attachment(filename: csvComposer.fileName, content: csvData as Data, disposition: .attachment, type: .csv, contentID: nil)
                
                email.attachments = [pdfAttachment, csvAttachment]
            }
        }
        
        do {
            try Session.shared.send(request: email)
            uploadUserOrderIf(isSent: true)
            Alerts.orderComplete(on: self, with: {
                print("Sent")
                self.performSegue(withIdentifier: "previewUnwind", sender: nil)
            })
            
        } catch {
            print(error)
            uploadUserOrderIf(isSent: false)
            CLSLogv("%@", getVaList(["Send Grid error: \(error)"]))
            self.dismiss(animated: true) {
                Alerts.alert(on: self, title: "Order failed to complete", and: "The invoice failed to send, please check order history or email support@neolifeafrica.co.za")
            }
        }
    }
    
//    this would be the final step
    /*func toEmail(){
        
//        convert csv/pdf to NSData
//        contruct email structure
//        add csv/pdf as attachment
        
        
        if MFMailComposeViewController.canSendMail() {
            let mailComposerVC = MFMailComposeViewController()
            mailComposerVC.setSubject("NeoApp" + "\(invoiceNumber)")
            mailComposerVC.mailComposeDelegate = self
            mailComposerVC.setToRecipients(["ordera@za.neolife.com"])
            //            mailComposerVC.setToRecipients(["karabo93@hotmail.co.uk"])
            mailComposerVC.setMessageBody("Use your order number: \(invoiceNumber) as refrerence to make your payment", isHTML: false)
            do {
                composer.exportHTMLContentToPDF(HTMLContent: htmlContent)
                try mailComposerVC.addAttachmentData(NSData(contentsOfFile: composer.pdfFileName) as Data, mimeType: "application/pdf", fileName: "Invoice.pdf")
                if let csvData = NSData(contentsOf: csvComposer.exportCSV(csvContent) as URL) {
                    mailComposerVC.addAttachmentData(csvData as Data, mimeType: "txt/text", fileName: csvComposer.fileName)
                }
            } catch {
                print("Not Found")
                Alerts.alert(on: self, title: "", and: "Invoice not found")
            }
            
            present(mailComposerVC, animated: true, completion: nil)
        } else {
            print("Fail to send")
            
            Alerts.alert(on: self, title: "Order failed to complete", and: "Please go to 'Settings > Accounts & Passwords' and enable Mail in your accounts or 'Add Account' to add an Email account and return to the app to complete the order")
            //log this to firebase
        }
    }*/
    

//    MARK: - Mail Delegate Method
    /*func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        switch result {
        case .sent:
            uploadUserOrderIf(isSent: true)
            controller.dismiss(animated: true) {
                Alerts.orderComplete(on: self, with: {
                    print("Sent")
                    self.performSegue(withIdentifier: "previewUnwind", sender: nil)
                })
            }
            
        case .cancelled:
            print("Cancelled")
            Alerts.alert(on: controller, title: "Cannot Cancel", and: "Please proceed with your order to complete")
            
        case .failed:
            print("failed")
            uploadUserOrderIf(isSent: false)

            controller.dismiss(animated: true) {
                Alerts.alert(on: self, message: "The invoice failed to send, please check order history or email support@neolifeafrica.co.za", title: "Failed", with: {
                    self.performSegue(withIdentifier: "previewUnwind", sender: nil)
                })
            }
            
        case .saved:
            print("Saved")
            uploadUserOrderIf(isSent: true)
            controller.dismiss(animated: true) {
                Alerts.saveOrderAlert(in: self, completion: {
                    print("Saved")
                    self.performSegue(withIdentifier: "previewUnwind", sender: nil)
                })
            }
            
            if error != nil {
                uploadUserOrderIf(isSent: false)
                CLSLogv("%@", getVaList(["Mail compose error: \(error.debugDescription)"]))
                controller.dismiss(animated: true) {
                    Alerts.alert(on: self, title: "Email failed to load", and: error?.localizedDescription)
                }
            }
        }
    }*/
    
}

//MARK: - Extended
