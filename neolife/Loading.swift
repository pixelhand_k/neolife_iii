//
//  Loading.swift
//  neolife
//
//  Created by Karabo Moloi on 2018/09/05.
//  Copyright © 2018 Karabo Moloi. All rights reserved.
//

import UIKit

class Loading {
//    add initialiser for view
    var controller = UIViewController()
    
    init(fromView view: UIViewController) {
        controller = view
    }
    
    private var activityView = UIView()
    private var activityIndicator = UIActivityIndicatorView()
    private var messageLabel = UILabel()
    private let effectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
    
    func createIndicator() {
        activityView.frame = CGRect(x: 0.0, y: 0.0, width: controller.view.frame.width, height: controller.view.frame.height)
        activityView.backgroundColor = .lightGray
        
        controller.view.addSubview(activityView)
        
        //        activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.height)) as UIActivityIndicatorView
        messageLabel.textColor = UIColor.white
        messageLabel.text = "Please Wait..."
        messageLabel.font = UIFont(name: "Helvetica Neue", size: 17)
        messageLabel.sizeToFit()
        messageLabel.center = CGPoint(x: activityIndicator.center.x + controller.view.frame.width/2, y: activityIndicator.center.y + controller.view.frame.height/2.5)
        activityView.addSubview(messageLabel)
        
        activityIndicator.center = controller.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = .white
        activityIndicator.backgroundColor = UIColor.lightGray
        //        activityIndicator.startAnimating()
        //        self.view.addSubview(activityIndicator)
        activityView.addSubview(activityIndicator)
        //        print("indicator added")
    }
    
    func showIndicator()  {
        print("Started Indicator")
        activityIndicator.startAnimating()
    }
    
    func hideIndicator() {
        activityIndicator.stopAnimating()
        activityIndicator.removeFromSuperview()
        activityView.removeFromSuperview()
        print("Stopped Indicator")
    }
}
