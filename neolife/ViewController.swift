//
//  ViewController.swift
//  neolife
//
//  Created by Karabo Moloi on 2016/12/19.
//  Copyright © 2016 Karabo Moloi. All rights reserved.
//

/* Changes made from previous swift code, the float values in UIColor RGB have to be divided by 255 to get the actual colour. Also, symbolic breakpoint UIColorBreak..(see breakpoint navigator) added to see where the color issue is in the stack */

import UIKit
import Firebase

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var loginBtn: UIButton!
//    @IBOutlet weak var countryPicker: UIPickerView!
//    @IBOutlet weak var countryLbl: UILabel!
//    @IBOutlet weak var selectCountryBtn: UIButton!
    @IBOutlet weak var guestBtn: UIButton!
//    @IBOutlet weak var registerBtn: UIButton!
    @IBOutlet weak var pinTextField: UITextField!
    @IBOutlet weak var idTextField: UITextField!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var logoImg: UIImageView!
    @IBOutlet weak var forgotBtn: UIButton!
    @IBOutlet weak var registeerBtn: UIButton!
    var userEmail : String?
    let userDetail = [User]()    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loginBtn.alpha = 0.0
        guestBtn.alpha = 0.0
        pinTextField.alpha = 0.0
        idTextField.alpha = 0.0
        forgotBtn.alpha = 0.0
        registeerBtn.alpha = 0.0
        //        log in button
        self.loginBtn.layer.borderColor = UIColor.paleGrey().cgColor
        
//        getUserWithId()
//        searchUserProfileWith(id: "1011")

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
//        makes sure users are logged out
        
        
        UIView.animate(withDuration: 0.8) {
            //        log in button
            self.loginBtn.layer.borderColor = UIColor.paleGrey().cgColor
            self.loginBtn.alpha = 1.0
            self.guestBtn.alpha = 1.0
            self.pinTextField.alpha = 1.0
            self.idTextField.alpha = 1.0
            self.forgotBtn.alpha = 1.0
            self.registeerBtn.alpha = 1.0
            self.guestBtn.center.y -= 10
            self.loginBtn.center.y -= 10
//            self.registerBtn.center.y -= 10

            self.titleLbl.center.y += 10
            self.logoImg.center.y += 10
 
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        idTextField.delegate = self
        pinTextField.delegate = self
        
        //add to view extension
        let tap : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(ViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        isUserLoggedIn()
    }
    
    func isUserLoggedIn() {
        if Auth.auth().currentUser != nil {
            print("User logged in")
            do {
                try Auth.auth().signOut()
            } catch let error as NSError {
                print("Error signing out: \(error.localizedDescription)")
            }
        } else {
            print("No user")
        }
    }
    
//    MARK: - Actions
    @IBAction func forgotBtn(_ sender: UIButton) {
//        AlertFunctions().resetPasswordAlert(self)
        let ac = UIAlertController(title: "Do you want to reset your password?", message: "Please enter your email address", preferredStyle: .alert)
        
        ac.addTextField { (textfield: UITextField) in
            textfield.placeholder = "Email"
        }
        
        let reset = UIAlertAction(title: "Reset", style: .default) { (action) in
            let emailField = ac.textFields?.first?.text
            
            if let userMail = emailField {
                if userMail != "" {
                    //reset password via email
                    Auth.auth().sendPasswordReset(withEmail: userMail, completion: { (error) in
                        if error != nil {
                            Alerts.alert(on: self, title: "Error Resetting Password", and: error?.localizedDescription)
                        } else {
                            Alerts.alert(on: self, title: "Link Sent", and: "Reset password link has been sent to your email, please check your inbox.")
                        }
                    })
                } else {
                    Alerts.alert(on: self, title: "Email Field Empty", and: "Please enter a valid email.")
                }
            }
            
            /*if AlertFunctions().validateEmail(candidate: emailField!) == false {
                Alerts.alert(on: self, title: "Not Registered", and: "Please enter valid email")
            }*/
            
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        
        ac.addAction(reset)
        ac.addAction(cancel)
        
        self.present(ac, animated: true, completion: nil)
    }
    
    @IBAction func registerBtn(_ sender: UIButton) {
        guard let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "initialVC") as? DistributorFirstLoginVC else {
            print("didnt work")
            return
        }
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func loginBtnAction(_ sender: Any) {
        
        if idTextField.text == "" || pinTextField.text == "" {
            Alerts.alert(on: self, title: "Incorrect Details", and: "Email and/or Pin fields are empty")
        } /*else if AlertFunctions().validateEmail(candidate: idTextField.text!) == false && pinTextField.text != "" || pinTextField.text == "" {
             
             AlertFunctions().alert(self, title: "Email not valid", message: "Please enter a valid email")
         }*/ else {
            
            if let validEmail = userEmail {
                Auth.auth().signIn(withEmail: validEmail, password: pinTextField.text!, completion: { (user, error) in
                    if error == nil {
                        print("SUCCESS")
                        UserFunctions.saveIdToUserDefaultsWith(id: self.idTextField.text!)
                        self.pinTextField.text = ""
                        self.performSegue(withIdentifier: "signedInSegue", sender: nil)
                    } else {
                        Alerts.alert(on: self, title: "Username/Password Incorrect", and: "Please check that your NeoLife ID is correct. Enter correct password.")
                    }
                })
            }
            
            /*if let validEmail = userEmail {
             PFUser.logInWithUsername(inBackground: validEmail, password: pinTextField.text!, block: { (user, error) in
             if user != nil {
             print("Success")
             //                    print("USER: \(String(describing: user))")
             //send is user distributer val to controller
             self.performSegue(withIdentifier: "signedInSegue", sender: nil)
             self.pinTextField.text = ""
             
             } else {
             //                print("Error: \(error), \(error?.localizedDescription)")
             //                        AlertFunctions().loginFailedAlert(self)
             Alerts.alert(on: self, title: "Invalid", and: error?.localizedDescription)
             }
             })
             } else {
             Alerts.alert(on: self, title: "User not found", and: "Is your Neolife ID correct? Did you register with the app?")
             }*/
            
            
        }
    }

    @objc func dismissKeyboard () {
//        self.countryCodeTextField.endEditing(true)
        self.pinTextField.endEditing(true)
        self.idTextField.endEditing(true)
    }
    
//    MARK: - Text Field Delegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == idTextField {
            if idTextField.text != "" {
                idTextField.text = ""
                pinTextField.text = ""
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if textField == idTextField {
            if idTextField.text != "" {
//                isUserRegistered(withID: idTextField.text!)
                searchUserProfileWith(id: idTextField.text!)
            } else if idTextField.text == "" {
//                isUserRegistered(withID: idTextField.text!)
//                searchUserProfileWith(id: idTextField.text!)
                Alerts.alert(on: self, title: "ID Field Empty", and: "Please enter your NeoLife ID")
            } else {
                Alerts.alert(on: self, title: "ID Field Empty", and: "Please enter your NeoLife ID")
            }
        }
    }

//    MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
//        let navController = segue.destination as! UINavigationController
        let tabController = segue.destination as! UITabBarController
//        let homeCollectionVC: HomeCollectionViewController = navController.topViewController as! HomeCollectionViewController
        let navController = tabController.viewControllers?[0] as! UINavigationController
        let homeCollectionVC: HomeCollectionViewController = navController.topViewController as! HomeCollectionViewController
        
        if segue.identifier == "signedInSegue" {
            homeCollectionVC.isUserMember = true
            homeCollectionVC.idNumber = String.toInt(string: idTextField.text!)
        } else if segue.identifier == "guestSegue" {
            homeCollectionVC.isUserMember = false
        }
    }
    
    //    MARK: - Search User
    func searchUserProfileWith(id: String) {
        let ref = Database.database().reference(fromURL: "https://neolife-d9754.firebaseio.com/")
        ref.child("users").child(id).observeSingleEvent(of: .value, with: { (snap) in
            guard let value = snap.value as? [String: Any] else {
                print("HHHHHHHHHHHHHHH")
                self.userEmail = "nonexistantuser@nobody.com"
                return
            }
            let email = value["email"] as? String
            print("email: \(String(describing: email))")
            self.userEmail = email
            
        }, withCancel: nil)
    }
    
//    MARK: - Parse Functions
    /*func isUserRegistered(withID id: String) {
        let query = PFUser.query()
        query?.whereKey("id_number", equalTo: id)
        
        query?.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                //                print("USER: \(String(describing: objects))")
                if let object = objects {
                    for objects in object {
//                        let name = objects["Name"] as! String
                        let email = objects["username"] as! String
//                        self.username = name
                        self.userEmail = email
                        print("Email: \(self.userEmail)")
                    }
                }
                
                if objects?.count == 0 {
                    self.userEmail = nil
                }
                
                
                /*if objects?.count != 0 {
                    self.performSegue(withIdentifier: "userExistsSegue", sender: nil)
                } else {
                    self.findDistributor(withID: id)
                }*/
                
            } else {
                print("Error: \(String(describing: error)), \(String(describing: error?.localizedDescription))")
            }
        })
        
    }*/
    
    /*func allUsers() {
        let query = PFUser.query()
        query?.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                print("USERS: \(objects!)")
            } else {
                print("Nothing found")
            }
        })
    }

    func getUserWithId() {
        let query = PFUser.query()
        query?.getObjectInBackground(withId: "2hHLD4Y72N", block: { (object, error) in
            if error == nil {
                print("USER: \(object)")
//                object?.deleteInBackground()
//                print("deleted user")
            } else {
                print("no user by that id")}
        })
    }*/
}

