//
//  ShoppingBagManager.swift
//  neolife
//
//  Created by Karabo Moloi on 2018/08/20.
//  Copyright © 2018 Karabo Moloi. All rights reserved.
//

import Foundation
import CoreData

public class ShoppingBagManager {
    let persistanceContainer : NSPersistentContainer!
    
    // allows commit changes in bg thread
    //set up managed context
    lazy var backgroundContext: NSManagedObjectContext = {
        return self.persistanceContainer.newBackgroundContext()
    }()
    
    init(container: NSPersistentContainer) {
        self.persistanceContainer = container
        self.persistanceContainer.viewContext.automaticallyMergesChangesFromParent = true
    }
    
    convenience init() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
             fatalError("Can't get app delegate")
//            print("Error couldn't get App delegate")
        }
        
        self.init(container: appDelegate.persistentContainer)
    }
    
   
    
    func saveToCart(_ code: Int, _ name: String, _ type: String?, _ quantity: String, _ total: String, _ imageUrl: String, itemPrice: String, _ bvPoints: String, _ pvPoints: String ) -> ShoppingBag? {
        
        guard let shoppingBagItem = NSEntityDescription.insertNewObject(forEntityName: "ShoppingBag", into: backgroundContext) as? ShoppingBag else {
            return nil
        }
        
        shoppingBagItem.code = Int32(code)
        shoppingBagItem.name = name
        shoppingBagItem.numItems = quantity
        shoppingBagItem.type = type
        shoppingBagItem.price = itemPrice
        shoppingBagItem.total = total
        shoppingBagItem.bv = bvPoints
        shoppingBagItem.pv = pvPoints
        shoppingBagItem.imageUrl = imageUrl
        
        return shoppingBagItem
    }
    
//    Other CRUD operations to be installed here
    
    
    func save() {
        if backgroundContext.hasChanges {
            do {
                try backgroundContext.save()
            } catch {
                print("Save error \(error)")
            }
        }
    }
    
}













