//
//  Price.swift
//  neolife
//
//  Created by Karabo Moloi on 2018/02/06.
//  Copyright © 2018 Karabo Moloi. All rights reserved.
//

import UIKit

class Price: NSObject {
    var caseSrp : String?
    var caseDp : String?
    var caseMp : String?
    var casePv : String?
    var caseBv :  String?
    var singleSrp : String?
    var singleDp : String?
    var singleMp : String?
    var singlePv : String?
    var singleBv : String?
    var units : String?
}
