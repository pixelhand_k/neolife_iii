//
//  ProfileTVC.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/01/18.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//

import UIKit
import Parse
import Firebase

class ProfileTVC: UITableViewController {
    
//    MARK: - Properties
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var roleLbl: UILabel!
    @IBOutlet weak var idLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var pvLbl: UILabel!
    @IBOutlet weak var bvLbl: UILabel!
    @IBOutlet weak var qvLbl: UILabel!
    @IBOutlet weak var gvLbl: UILabel!
    @IBOutlet weak var addressOneCellLbl: UILabel!
    @IBOutlet weak var addressTwoCellLbl: UILabel!
    @IBOutlet weak var cityCellLbl: UILabel!
    @IBOutlet weak var postCodeCellLbl: UILabel!
    let ref = Database.database().reference(fromURL: "https://neolife-d9754.firebaseio.com/")
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        
//        profileImgView.layer.borderWidth = 0.5
//        profileImgView.layer.borderColor = UIColor.lightGray.cgColor
//        profileImgView.layer.shouldRasterize = false
//        profileImgView.layer.cornerRadius = self.profileImgView.frame.height / 2
//        profileImgView.clipsToBounds = true
        
        userDetails()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.allowsSelection = false
        
//        print("USER: \(PFUser.current())")
        
    }
    
//    MARK: - Actions

    @IBAction func editProfileAction(_ sender: UIBarButtonItem) {
//        Alerts.changeAddress(on: self)
        Alerts.editUserProfile(on: self)
    }
    
    @IBAction func close() {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addDetails(_ sender: Any) {
        AlertFunctions().addAddressAlert(self)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    
//    MARK: - Parse func
    func userDetails() {
        let id = UserFunctions.getNeolifeId()
        
        if id != "" {
            ref.child("users").child(id).observe(.value, with: { (snap) in
                if let value = snap.value as? [String : AnyObject] {
                    let name = value["name"] as? String
                    let id = value["id_number"] as? Int
                    let email = value["email"] as? String
                    let personal = value["ppv"] as? Int
                    let group = value["gpv"] as? Int
                    let qualifying = value["qpv"] as? Int
                    let country = value["cc"] as? Int
                    let address = value["address"] as? String
                    let distributor = value["is_distributor"] as? Bool
                    var role = ""
                    
                    // check if a comma exists
                    let addressArr = address?.components(separatedBy: ",")
                    
                    var addressOne = ""
                    var addressTwo = ""
                    var city = ""
                    var code = ""
                    
                    
                    if let addressArray = addressArr {
                        if addressArray.count > 1 {
                            addressOne = addressArray[0]
                            addressTwo = addressArray[1]
                            city = addressArray[2]
                            code = addressArray[3]
                        } else if addressArray.count == 0 {
                            //                        Alerts.alert(on: self, title: "No Address Found", and: "Please edit address to fill in address")
                            //put empty fields
                            self.addressOneCellLbl.text = ""
                            self.addressTwoCellLbl.text = ""
                            self.cityCellLbl.text = ""
                            self.postCodeCellLbl.text = ""
                        } else {
                            addressOne = addressArray[0]
                        }
                    }
                    
                    if distributor == true {
                        role = "Distributor"
                    } else {
                        role = "Member"
                    }
                    
                    DispatchQueue.main.async {
                        self.nameLbl.text = name!
                        self.idLbl.text = String.fromInt(integer: id)
                        self.emailLbl.text = email
                        self.pvLbl.text = String.fromInt(integer: personal)
                        self.bvLbl.text = String.fromInt(integer: country)
                        self.qvLbl.text = String.fromInt(integer: qualifying)
                        self.gvLbl.text = String.fromInt(integer: group)
                        self.roleLbl.text = role
                        self.addressOneCellLbl.text = addressOne
                        self.addressTwoCellLbl.text = addressTwo
                        self.cityCellLbl.text = city
                        self.postCodeCellLbl.text = code
                    }
                }
            }, withCancel: nil)
        } else {
            print("NO ID PRESENT")
        }
    }
}
