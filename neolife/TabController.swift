//
//  TabController.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/01/24.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//

import UIKit
import paper_onboarding
import Parse
import Firebase

class TabController: UITabBarController, PaperOnboardingDataSource, PaperOnboardingDelegate {
    
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var onboardingView: PaperOnboarding!
    @IBOutlet weak var dismissButton: UIButton!
    let titleFont = UIFont(name: "HelveticaNeue-Bold", size: 19)
    let buttonFont = UIFont(name: "HelveticaNeue", size: 17)
    let descriptionFont = UIFont(name: "HelveticaNeue-Light", size: 17)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        onboardingView.dataSource = self
        onboardingView.delegate = self
        dismissButton.titleLabel?.font = buttonFont
        dismissButton.layer.borderColor = UIColor.white.cgColor
        dismissButton.layer.borderWidth = 1
        dismissButton.layer.cornerRadius = 5
        dismissButton.bounds.size.width = view.bounds.width - 100
        
        isFirstTimeIn()
//        presentOnboard()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func presentOnboard() {
        parentView.bounds.size.width = view.bounds.width
        parentView.bounds.size.height = view.bounds.height
        parentView.center = view.center
        parentView.alpha = 0
        onboardingView.clipsToBounds = true
        view.addSubview(parentView)
        
        UIView.animate(withDuration: 1, delay: 1, options: [], animations: {
            self.parentView.alpha = 1
//            print("Presented")
        }, completion: nil)
    }
    
    
    func onboardingItem(at index: Int) -> OnboardingItemInfo {
        return [OnboardingItemInfo(informationImage: #imageLiteral(resourceName: "neolife_logo"), title: "Welcome to NeoLife!", description: "Your all-in-one product shopping solution", pageIcon: #imageLiteral(resourceName: "iconName"), color: UIColor.greenNeolife(), titleColor: UIColor.white, descriptionColor: UIColor.white, titleFont: titleFont!, descriptionFont: descriptionFont!),
                
                OnboardingItemInfo(informationImage: #imageLiteral(resourceName: "credit-card-regular"), title: "Payment made easy!", description: "We accept all major credit/ debit card payments as well as bank deposits & EFT’s", pageIcon: #imageLiteral(resourceName: "iconName"), color: UIColor.greenNeolife(), titleColor: UIColor.white, descriptionColor: UIColor.white, titleFont: titleFont!, descriptionFont: descriptionFont!),
            
                OnboardingItemInfo(informationImage: #imageLiteral(resourceName: "camera-regular"), title: "Bank deposit? No hassle!", description: "To complete your order, simply take a photo of your deposit slip or if it’s already saved, upload from your photos", pageIcon: #imageLiteral(resourceName: "iconName"), color: UIColor.greenNeolife(), titleColor: UIColor.white, descriptionColor: UIColor.white, titleFont: titleFont!, descriptionFont: descriptionFont!),
                
                OnboardingItemInfo(informationImage: #imageLiteral(resourceName: "Mail"), title: "Enable your email", description: "To make purchases as a Distributor, simply activate your email address in Settings > Accounts & Passwords - and you’re ready!", pageIcon: #imageLiteral(resourceName: "iconName"), color: UIColor.greenNeolife(), titleColor: UIColor.white, descriptionColor: UIColor.white, titleFont: titleFont!, descriptionFont: descriptionFont!),
              
                OnboardingItemInfo(informationImage: #imageLiteral(resourceName: "telephone-regular"), title: "Issue? Contact Us", description: "Experiencing issues with an order or the app?  Go to Contacts and we’ll gladly assist you!", pageIcon: #imageLiteral(resourceName: "iconName"), color: UIColor.greenNeolife(), titleColor: UIColor.white, descriptionColor: UIColor.white, titleFont: titleFont!, descriptionFont: descriptionFont!),
               
                OnboardingItemInfo(informationImage: #imageLiteral(resourceName: "neolife_logo"), title: "Welcome to our global community!", description: "Thank you for being part of the NeoLife family – enjoy!", pageIcon: #imageLiteral(resourceName: "iconName"), color: UIColor.greenNeolife(), titleColor: UIColor.white, descriptionColor: UIColor.white, titleFont: titleFont!, descriptionFont: descriptionFont!)][index]
    }
    
    
    func onboardingItemsCount() -> Int {
        return 6
    }
    
    func onboardingDidTransitonToIndex(_ index: Int) {
        if index == 5 {
            UIView.animate(withDuration: 0.4) {
                self.dismissButton.alpha = 1
            }
        }
    }
    
    func onboardingWillTransitonToIndex(_ index: Int) {
        if index <= 4 {
            if self.dismissButton.alpha == 1 {
                UIView.animate(withDuration: 0.3, animations: {
                    self.dismissButton.alpha = 0
                })
            }
        }
    }
    
    func onboardingConfigurationItem(_ item: OnboardingContentViewItem, index: Int) {
//        <#code#>
    }
    
    //calls for user to set up their profile after dismissing
    fileprivate func dismissOnboarding() {
        UIView.animate(withDuration: 0.1, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 0, options: [], animations: {
            self.parentView.transform = CGAffineTransform(translationX: 0, y: 10)
        }) { (success) in
            UIView.animate(withDuration: 0.3, animations: {
                self.parentView.transform = CGAffineTransform(translationX: 0, y: -800)
            })
            self.setUserAddress()
        }
    }
    
    @IBAction func dismissOnboardAction(_ sender: UIButton) {
        dismissOnboarding()
    }
    
    @IBAction func skipOnboardAction(_ sender: UIButton) {
        dismissOnboarding()
    }
    

//    MARK: - User Settings
    func isProfileSet() -> Bool {
        var isSet = false

        if PFUser.current() != nil {
            let user = PFUser.current()
            let profileSet = user?["is_profile_set"] as! Bool
            isSet = profileSet
//            print("Setting: \(isSet)")
        }
        
        return isSet
    }
    
    func setUserAddress() {
        if PFUser.current() != nil {
            if isProfileSet() == false {
//                print("User profile is not set")
                AlertFunctions().addAddressAlert(self)
            }
        } else {
            return
        }
    }
    
//    MARK: - First time login
    func isFirstTimeIn() {
        if (!UserDefaults.standard.bool(forKey: "firstTimeIn")) {
            UserDefaults.standard.set(true, forKey: "firstTimeIn")
            UserDefaults.standard.synchronize()
            
            presentOnboard()
//            Alerts.alertWithImage(on: self, title: "You're new! Welcome", and: "Things you need to know, to be able to make purchases as a member or distributor, make sure that you have an active email account in your settings. This can be found in 'Settings > Accounts & Passwords'")
//            return true
        }
        
//        return false
    }


}
