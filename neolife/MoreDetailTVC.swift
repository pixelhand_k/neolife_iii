//
//  MoreDetailTVC.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/01/25.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//

import UIKit
import CoreData
import Parse
import Firebase

class MoreDetailTVC: UITableViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITextFieldDelegate, UIGestureRecognizerDelegate {
    
//    MARK: - Variables
    var productDetail = [PFObject?]()
    var pricesData = [PFObject?]()
    var prices = [Price]()
    let ref = Database.database().reference(fromURL: "https://neolife-d9754.firebaseio.com/")
    var api = APIService()
    var total : Int = 0
    var productTitle = String() //product_name
    var productCode = String() //code
    var price : Int = 0 //pric
    var type = String()
    var pvPoints = String()
    var bvPoints = String()
    var singlePrice : Int = 0
    var isFromHome = false
    var isTrackingPanLocation = false
//    var prodFile: PFFile?
    var pdfUrl : String?
    var isDistributor : Bool?
    var isMember : Bool?
    var imgUrl : String!
    var neolifeLogoUrl = "https://firebasestorage.googleapis.com/v0/b/neolife-d9754.appspot.com/o/images%2FneolifeLogo%403x.png?alt=media&token=a0a00ed9-f3a4-41eb-8a5e-1dc291308722"
    var selectedIndex = Int()

//    MARK: - Outlets
    @IBOutlet weak var overviewLbl: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var caseCollectionView: UICollectionView!
    @IBOutlet weak var priceMultiplierLbl: UILabel!
    @IBOutlet weak var quantityTextField: UITextField!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var benefitsLbl: UILabel!
    @IBOutlet var pullDismissGesuture: UIPanGestureRecognizer!
    @IBOutlet weak var labelBtn: UIButton!
    @IBOutlet weak var overviewTitleLbl: UILabel!
    @IBOutlet weak var benefitsTitleLbl: UILabel!
    @IBOutlet weak var qtyTitleLbl: UILabel!
    @IBOutlet weak var totalTitleLbl: UILabel!
    
    var cart = [NSManagedObject]()
    
//    MARK: - View Functions
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        typeUser()
        self.title = productTitle
//        getProductDetails()
        getNewProductDetails()
//        getProductPrice()
        getPriceUsingCode(code: productCode)
//        print("test distributor function: \(isDistributor)")
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        print("test distributor function: \(isDistributor)")
//        newProductPrice() remove
        
        //        removes empty cells
        tableView.tableFooterView = UIView()
        //total calculation
        self.totalLbl.text = "\(total)"
        
        self.priceMultiplierLbl.text = "0"
        
        caseCollectionView.delegate = self
        caseCollectionView.dataSource = self
        caseCollectionView.allowsMultipleSelection = false
        caseCollectionView.allowsSelection = true
        quantityTextField.delegate = self
        
        labelBtn.layer.borderWidth = 0.8
        labelBtn.layer.borderColor = UIColor.greenNeolife().cgColor
        labelBtn.layer.cornerRadius = labelBtn.frame.height/2
        labelBtn.layer.backgroundColor = UIColor.greenNeolife().cgColor
        labelBtn.tintColor = .white
        labelBtn.layer.shadowColor = UIColor.black.cgColor
        labelBtn.layer.shadowRadius = 2.5
        labelBtn.layer.shadowOpacity = 0.5
        labelBtn.layer.shadowOffset = CGSize(width: 0.5, height: 0)
        
        //add to view extension
        let tap : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(MoreDetailTVC.dismissKey))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        let logoImage = #imageLiteral(resourceName: "neolifeLogo")
        
        if productImage.image == nil {
            productImage.image = logoImage
        }
        
        // pull to dismiss
        pullDismissGesuture = UIPanGestureRecognizer(target: self, action: #selector(MoreDetailTVC.panGestureRecognised(recogniser:)))
        pullDismissGesuture.delegate = self
        tableView.addGestureRecognizer(pullDismissGesuture)
        
        //fonts
        benefitsLbl.font = UIFont.helveticaNeueContentDetail()
        overviewLbl.font = UIFont.helveticaNeueContentDetail()
        overviewTitleLbl.font = UIFont.helveticaNeueContentTitle()
        benefitsTitleLbl.font = UIFont.helveticaNeueContentTitle()
        qtyTitleLbl.font = UIFont.helveticaNeueContentTitle()
        totalTitleLbl.font = UIFont.helveticaNeueContentTitle()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    MARK: - Action button(s)
    
    @IBAction func pdfLabelViewer(_ sender: UIButton) {
        guard let vc = UIStoryboard(name: "Product", bundle: nil).instantiateViewController(withIdentifier: "pdfViewer") as? PDFViewerVC else {
            print("didnt work")
            return
        }
        vc.title = "Label"
        vc.pdfContent = ""
//        vc.prodFile = prodFile
        vc.pdfUrl = pdfUrl
        vc.prodTitle = productTitle
        vc.title = productTitle
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
//    MARK: - Gesture
    @objc func panGestureRecognised(recogniser: UIPanGestureRecognizer) {
        if recogniser.state == .began && tableView.contentOffset.y == 0 {
            recogniser.setTranslation(CGPoint.zero, in: tableView)
            isTrackingPanLocation = true
        } else if recogniser.state == .recognized {
            let panOffset = recogniser.translation(in: tableView)
            
            let panRangeToDismiss = panOffset.y > 200
            
            if panRangeToDismiss {
                recogniser.isEnabled = true
                
                if isFromHome == false {
                    self.dismiss(animated: true, completion: nil)
                } else {
                    self.navigationController?.popViewController(animated: true)
                }
            }
            
            if panOffset.y < 0 {
                isTrackingPanLocation = false
            }
        } else {
            isTrackingPanLocation = false
        }
    }
    
//    MARK: - Gesture Delegate
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
//    MARK: - Navigation Bar Actions
    
    @IBAction func dismissBtn(_ sender: UIBarButtonItem) {
        
        if isFromHome == false {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    @IBAction func addToCartBtn(_ sender: UIBarButtonItem) {
        
//        let ctvc : CartTVC = navigationController?.topViewController as! CartTVC
 
        
//        let items = [BagItems(name: productTitle, retailPrice: rrp)]
        
//        addProductsToCart(items: items)
        
//        self.ctvc.cartItems = items
        
//        navigationController?.pushViewController(ctvc, animated: false)
        
//        print("Added \(items.count) items")
        
        
//        let item = BagItem(name: productTitle, numItems: quantityTextField.text!, type: type, total: totalLbl.text!)
        
        
        if Int(totalLbl.text!) == 0 || quantityTextField.text == "" || totalLbl.text == "R0" {
//            toCartAlert(title: "No Items/Packs", message: "Did you select a case/single or how much you wanted?")
            Alerts.alert(on: self, title: "You Can't Add This Item", and: "Did you say how many you wanted or did you select case or single?")
        } else {
//            print("Cart:\n  Name: \(item.name), Items: \(item.numItems), type: \(item.type!), total: \(item.total)")
//            save(name: productTitle, numOfItems: quantityTextField.text!, total: totalLbl.text!, type: type, image: productImage.image!)
//            self.dismiss(animated: true, completion: nil)
//            saveBagItems(code: productCode, type: type, quantity: quantityTextField.text!)
//            saveBagItems(Int(productCode)!, productTitle, type, quantityTextField.text!, totalLbl.text!, productImage.image!)
            
            
            saveBagItems(Int(productCode)!,
                         productTitle,
                         type,
                         quantityTextField.text!,
                         totalLbl.text!,
                         self.imgUrl ?? self.neolifeLogoUrl,
                         itemPrice: priceMultiplierLbl.text!)
        }
    }
    
//    MARK: - Table View Delegate
    
    //    MARK: - Resize cells
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    
//    MARK: - Collection View Data Source

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return pricesData.count
        return 2
    }

    //type_size
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "caseCell", for: indexPath) as! CaseCell
        
        var titles = ["Case", "Single"]
        var details = ["1 Unit X 6", "1 Unit"]
        
        cell.caseTitleLbl.text = titles[indexPath.row]
        cell.caseSizeLbl.text = details[indexPath.row]
        
        
//        if let caseSize = productDetail[indexPath.row]?["type_size"] as? String {
//            cell.caseSizeLbl.text = caseSize
//        }
        
//        if let size = pricesData[indexPath.row]?["description"] as? String {
//            type = size
//        }
//        cell.caseSizeLbl.text = type
        
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 1
        cell.layer.cornerRadius = 5
        cell.backgroundColor = UIColor.white
        
        
        return cell
    }
    
//    MARK: - Collection View Delegate Flow Layout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: 165, height: 80)
        return dynamicCollectionCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
    }
    
//    MARK: - Collection View Delegate Methods
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.layer.borderColor = UIColor.greenNeolife().cgColor
    }
    
    //when user selects case or single, it does another check if user is a distributor
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let caseIndex = IndexPath(row: 0, section: 0)
        let singleIndex = IndexPath(row: 1, section: 0)
        let caseCell = self.caseCollectionView.cellForItem(at: caseIndex)
        let singleCell = self.caseCollectionView.cellForItem(at: singleIndex)
        
//        if user is guest disbale interaction
        if isMember == false && isDistributor == false  && indexPath.row == 0 {
            Alerts.alert(on: self, title: "Case prices unavailable for guest", and: "Please select single")
            caseCell?.isUserInteractionEnabled = false
            caseCell?.alpha = 0.2
        }
        
        if isMember == false && isDistributor == false && indexPath.row == 0 {
            Alerts.alert(on: self, title: "Case prices unavailable for guest", and: "Please select single")
            caseCell?.isUserInteractionEnabled = false
            caseCell?.alpha = 0.2
        } else {
            if indexPath.row == 0 && quantityTextField.text == "" {
                print("selected 0")
                let casePrice = casePriceFromObject()
                
                if casePrice == "" {
                    Alerts.alert(on: self, title: "Case Unavailable", and: "Please select single")
                    caseCell?.isUserInteractionEnabled = false
                    caseCell?.alpha = 0.2
                } else {
                    self.priceMultiplierLbl.text = casePrice
                    self.type = "Case(s)"
                }
                
            } else if indexPath.row == 0 && quantityTextField.text != "" {
                
                let casePrice = casePriceFromObject()
                
                if casePrice == "" {
                    
                    Alerts.alert(on: self, title: "Case Unavailable", and: "Please select single")
                    caseCell?.isUserInteractionEnabled = false
                    caseCell?.alpha = 0.2
                } else {
                    self.priceMultiplierLbl.text = casePriceFromObject()
                    totalLbl.text = totalPrice(quantity: quantityTextField.text!)
                    self.type = "Case(s)"
                }
                
            } else if indexPath.row == 1 && quantityTextField.text == "" {
                let singlePriceString = singlePriceFromObject()
                
                if singlePriceString == "" {
                    Alerts.alert(on: self, title: "Case Unavailable", and: "Please select single")
                    singleCell?.isUserInteractionEnabled = false
                    singleCell?.alpha = 0.2
                } else {
                    self.priceMultiplierLbl.text = singlePriceString
                    self.type = "Single(s)"
                }
                
                
                
            } else if indexPath.row == 1 && quantityTextField.text != "" {
                
                let singlePriceString = singlePriceFromObject()
                
                if singlePriceString == "" {
                    
                    Alerts.alert(on: self, title: "Case Unavailable", and: "Please select Case")
                    singleCell?.isUserInteractionEnabled = false
                    singleCell?.alpha = 0.2
                } else {
                    self.priceMultiplierLbl.text = singlePriceString
                    totalLbl.text = totalPrice(quantity: quantityTextField.text!)
                    self.type = "Single(s)"
                }

            } else {
                print("Put in a value and/or select a type")
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        
        cell?.layer.borderColor = UIColor.lightGray.cgColor
        
        print("Deslected: \(indexPath)")
    }

//    MARK: - Text Field Delegate
    @IBAction func textChanged(_ sender: UITextField) {
        if quantityTextField.text == "" {
            print("please select case or single")
            totalLbl.text = "0"
        } else if quantityTextField.text != "" && priceMultiplierLbl.text == "0" {
            Alerts.alert(on: self, title: "Please Select", and: "You didn't select Case Or Single")
        } else {
           totalLbl.text = totalPrice(quantity: quantityTextField.text!)
        }
    }
}
    
// add firebase pricelist in extension
extension MoreDetailTVC {
    //    MARK: - My Functions
    
    //outputs the prices from distrubtor as string if user a distributor
    func casePriceFromObject() -> String {
        var casePrice = ""
        
        if prices.count != 0 {
            if isMember == true && isDistributor == false {
                let price = self.prices[0].caseMp
                
                if price != nil{
                    casePrice = price!
                    print("case price member: \(price!)")
                }else if price == nil {
                    Alerts.alert(on: self, title: "Currently Unavailable", and: "You will be updated on this product soon")
                }
                
            } else if isDistributor == true {
                let price = self.prices[0].caseDp
                if price != nil{
                    casePrice = price!
                    print("case price distr: \(price!)")
                } else if price == nil {
                    Alerts.alert(on: self, title: "Currently Unavailable", and: "You will be updated on this product soon")
                }
            } else {
                let price = self.prices[0].caseSrp
                if price != nil{
                    casePrice = price!
                } else if price == nil {
                    Alerts.alert(on: self, title: "Currently Unavailable", and: "You will be updated on this product soon")
                }
            }
        } else {
            Alerts.dismissAlert(on: self, title: "Currently Unavailable", and: "This product will soon be available")
        }
        
        return casePrice
    }
    
    func singlePriceFromObject() -> String {
        var singlePrice = ""
        
        if prices.count != 0 {
            if isMember == true && isDistributor == false {
                let price = self.prices[0].singleMp
                if let num = price {
                    singlePrice = num
                 } else {
                    Alerts.alert(on: self, title: "Currently Unavailable", and: "You will be updated on this product soon")
                }
            } else if isDistributor == true {
                if prices.count == 0 {
                    Alerts.alert(on: self, title: "Currently Unavailable", and: "You will be updated on this product soon")
                } else {
                    let price = self.prices[0].singleDp
                    if let num = price {
                        singlePrice = num
                        print("Single price distr: \(num)")
                     } else {
                        Alerts.alert(on: self, title: "Currently Unavailable", and: "You will be updated on this product soon")
                    }
                }
            } else {
                if prices.count == 0 {
                    Alerts.alert(on: self, title: "Currently Unavailable", and: "You will be updated on this product soon")
                } else {
                    let price = self.prices[0].singleSrp
                    if let num = price {
                        singlePrice = num
                    }
                }
            }
        } else {
            Alerts.dismissAlert(on: self, title: "Currently Unavailable", and: "This product will soon be available")
        }
        return singlePrice
    }
    
    
    
    func totalPrice(quantity : String) -> String {
        var calc : Int = 0
        var casePrice = ""
        
        if quantity == "" {
//            print("enter quantity")
            Alerts.alert(on: self, title: "No Quantity", and: "Did you say how much you wanted?")
        } else if quantity != "" && priceMultiplierLbl.text == "" {
            Alerts.alert(on: self, title: "No case available", and: "Please select Single")
        } else {
            
            casePrice = priceMultiplierLbl.text!
            
            let noComma = casePrice.replacingOccurrences(of: ",", with: "", options: .regularExpression, range: nil)
            
            calc = Int(quantity)! * Int(noComma)!
        }
        
        //        return "R" + toString(integer: calc)
        return "R" + String.fromInt(integer: calc)
        //        return AppDelegate().getStringValueFormattedAsCurrency(String.fromInt(integer: calc))
    }
    
    @objc func dismissKey() {
        quantityTextField.endEditing(true)
    }
    
    
    //    save to CD
    /*func save(name :String, numOfItems: String, total: String, type: String, image: UIImage, price: String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "ShoppingBag", in: managedContext)
        
        //        let pack = NSManagedObject(entity: entity!, insertInto: managedContext)
//        let cart = Cart(entity: entity!, insertInto: managedContext)
        //        let shoppingBag = ShoppingBag(entity: entity!, insertInto: managedContext)
        let cart = ShoppingBag(entity: entity!, insertInto: managedContext)
        
        cart.name = name
        cart.numItems = numOfItems
        cart.type = type
        cart.total = total
//        cart.isSent = false
        cart.price = price
        //
        let image = image
        //        let photoData = UIImagePNGRepresentation(image)
        let photoData = UIImageJPEGRepresentation(image, 1.0)
        cart.image = NSData(data: photoData!)
        
        do {
            try managedContext.save()
            
            //            itemCount = itemCount + 1
            //            countString = "\(packCount())"
            //            print(countString)
            //            addBadge()
            
            
            //            tableView.reloadData()
            self.dismiss(animated: true, completion: nil)
            print("Data Saved")
        } catch let error as NSError {
            print("Could not save data \(error), \(error.userInfo)")
        }
    }*/
    
    func dynamicCollectionCell() -> CGSize {
        //sets the cell sizes according to the screen size, so it can dynamically adjust
        //        let cellNum : CGFloat = CGFloat(self.productDetail.count)
        let cellNum : CGFloat = 2
        let sidePadding : CGFloat = 13
        //        let iphoneSeScreenWidth : CGFloat = 320
        //        let screenWidth = caseCollectionView?.frame.width
        //        var width: CGFloat = 0
        //        var height: CGFloat = 0
        
        let width = ((caseCollectionView?.frame.width)! - sidePadding) / cellNum
        //        let height = width - heightDiff
        
        return CGSize(width: width, height: 65)
    }
    
    func isObjectInCD(withName name: String) -> Bool {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let prodFetch : NSFetchRequest<ShoppingBag> = ShoppingBag.fetchRequest()
        
        prodFetch.predicate = NSPredicate(format: "name == %@", name)
        
        let item = try! managedContext.fetch(prodFetch)
        
        if item.count > 0 {
            return true
        } else {
            return false
        }
    }
    
    func saveBagItems(_ code: Int, _ name: String, _ type: String?, _ quantity: String, _ total: String, _ imageUrl: String, itemPrice: String){
        
        if isObjectInCD(withName: productTitle) == true {
            Alerts.alert(on: self, title: "Already In Cart", and: "Go to cart to edit")
        } else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            
            let managedContext = appDelegate.persistentContainer.viewContext
            
            let entity = NSEntityDescription.entity(forEntityName: "ShoppingBag", in: managedContext)
            
            let shoppingBag = ShoppingBag(entity: entity!, insertInto: managedContext)
            
            shoppingBag.code = Int32(code)
            shoppingBag.name = name
            shoppingBag.numItems = quantity
            shoppingBag.type = type
            shoppingBag.price = itemPrice
            shoppingBag.total = total
            shoppingBag.bv = bvPoints
            shoppingBag.pv = pvPoints
            shoppingBag.imageUrl = imageUrl
            
//            let image = image
//            let photoData = UIImageJPEGRepresentation(image, 1.0)
//            shoppingBag.image = NSData(data: photoData!)
            
            do {
                try managedContext.save()
                if isFromHome == false {
                    self.dismiss(animated: true, completion: nil)
                } else {
                    self.navigationController?.popViewController(animated: true)
                }
            } catch let error as NSError {
                print("Error: \(error), \(error.localizedDescription)")
            }
        }
    }
    
    //    MARK: - Backend Functions
//    Keeps prices synced
    func getPriceUsingCode(code: String) {
        self.prices.removeAll()
        let pricelistRef = ref.child("pricelist")
        pricelistRef.keepSynced(true)
        pricelistRef.queryOrdered(byChild: "code").queryEqual(toValue: code).observe(.childAdded, with: { (snapshot) in
            if let dict = snapshot.value as? [String: AnyObject] {
                print("NEW PRICELIST: \(dict)")
//                print("isDistributor: \(self.isUserDistributor())")
                let pricelist = Price()
                pricelist.caseBv = dict["caseBv"] as? String
                pricelist.casePv = dict["casePv"] as? String
                pricelist.caseDp = dict["caseDp"] as? String
                pricelist.caseMp = dict["caseMp"] as? String
                pricelist.caseSrp = dict["caseSrp"] as? String
                pricelist.singleBv = dict["singleBv"] as? String
                pricelist.singlePv = dict["singlePv"] as? String
                pricelist.singleDp = dict["singleDp"] as? String
                pricelist.singleMp = dict["singleMp"] as? String
                pricelist.singleSrp = dict["singleSrp"] as? String
                pricelist.units = dict["units"] as? String
                
                //logic for prices here
//                This enables the selection of case or single prices, if unavailable then its grayed out or if user is not a distributor, etc
                DispatchQueue.main.async {
                    self.caseCollectionView.layoutIfNeeded()
                    let caseIndex = IndexPath(row: 0, section: 0)
                    let singleIndex = IndexPath(row: 1, section: 0)
                    let cell = self.caseCollectionView.cellForItem(at: caseIndex) as? CaseCell
                    let singleCell = self.caseCollectionView.cellForItem(at: singleIndex) as? CaseCell
                   
                    if self.isDistributor == true {
                        if let price = dict["caseDp"] as? String {
                            if price == "" {
                                cell?.isUserInteractionEnabled = false
                                cell?.caseSizeLbl.isEnabled = false
                                cell?.caseTitleLbl.isEnabled = false
                            }
                            
                            if pricelist.singleDp == "" {
                                singleCell?.isUserInteractionEnabled = false
                                singleCell?.caseSizeLbl.isEnabled = false
                                singleCell?.caseTitleLbl.isEnabled = false
                            }
                        }
                    }
                    
                    if self.isMember == true && self.isMember == false {
                        //                        let price = dict["caseMp"] as? String
                        if pricelist.caseMp == "" {
                            cell?.isUserInteractionEnabled = false
                            //                            cell?.alpha = 0.2
                            cell?.caseSizeLbl.isEnabled = false
                            cell?.caseTitleLbl.isEnabled = false
                        }
                        
                        if pricelist.singleMp == "" {
                            singleCell?.isUserInteractionEnabled = false
                            //                            cell?.alpha = 0.2
                            singleCell?.caseSizeLbl.isEnabled = false
                            singleCell?.caseTitleLbl.isEnabled = false
                        }
                        
                    } else if (self.isMember == false && self.isDistributor == false) {
                        cell?.isUserInteractionEnabled = false
                        cell?.alpha = 0.2
                        cell?.caseSizeLbl.isEnabled = false
                        cell?.caseTitleLbl.isEnabled = false
                    } else if (pricelist.caseSrp == "") {
                        cell?.isUserInteractionEnabled = false
                        cell?.alpha = 0.2
                        cell?.caseSizeLbl.isEnabled = false
                        cell?.caseTitleLbl.isEnabled = false
                    }
                }
                
                //adds prices to an array after loading them up
                DispatchQueue.main.async {
                    self.prices.append(pricelist)
                }
                
                DispatchQueue.main.async {
                    self.caseCollectionView.reloadData()
                    self.tableView.reloadData()
                }
            }
        }, withCancel: nil)
    }
    
    func getNewProductDetails() {
        ref.child("product_details").queryOrdered(byChild: "code").queryEqual(toValue: String.toInt(string: productCode)).observe(.childAdded, with: { (snapshot) in
            if let dict = snapshot.value as? [String: AnyObject] {
                print("PRODUCT DETAILS: \(dict)")
                let benefits = dict["key_benefits_details"] as? String
                let overview = dict["overview"] as? String
                let image = dict["image_url"] as? String
                //                            let type = object["type_size"] as? String
                let pdfLabel = dict["pdf_label"] as? String
                
                DispatchQueue.main.async {
                    self.overviewLbl.text = overview
                    self.benefitsLbl.text = benefits
                    
                    if let productImage = image {
                        self.imgUrl = productImage
                        self.productImage.loadImageFromCacheWithUrl(productImage)
                    }

                    if pdfLabel != "" {
                        self.pdfUrl = pdfLabel
                    } else {
                        self.labelBtn.isEnabled =  false
                        self.labelBtn.layer.borderColor = UIColor.init(white: 0.7, alpha: 0.2).cgColor
                        self.labelBtn.layer.backgroundColor = UIColor.init(white: 0.7, alpha: 0.2).cgColor
                    }
                }
                
                DispatchQueue.main.async {
                    self.tableView?.reloadData()
                    self.caseCollectionView.reloadData()
                }
            }
        }, withCancel: nil)
    }
    
    
    /// gets the images and key details off the product
    /*func getProductDetails(){
        let query = PFQuery(className: "product_details")
        query.whereKey("product_name", equalTo: self.productTitle)
        query.fromLocalDatastore()
        query.findObjectsInBackground { (object: [PFObject]?, error) -> Void in
            if error == nil {
//                print("Product details class: \(String(describing: object))")
                DispatchQueue.global(qos: .userInitiated).async {
                    if let objects = object {
                        for object in objects {
                            let benefits = object["key_benefits_details"] as? String
                            let overview = object["overview"] as? String
                            let image = object["image_url"] as? String
                            //                            let type = object["type_size"] as? String
                            let pdfLabel = object["pdf_label"] as? PFFile
                            
                            DispatchQueue.main.async {
                                self.overviewLbl.text = overview
                                self.benefitsLbl.text = benefits
                                self.api.loadImages(image!, imageView: self.productImage)
                                //                                self.type = type!
                                
                                if pdfLabel != nil {
                                    self.prodFile = pdfLabel
                                } else {
                                    self.labelBtn.isEnabled =  false
                                }
                                
                            }
                        }
                        
                        DispatchQueue.main.async {
                            self.productDetail = Array(objects.makeIterator())
                        }
                    }
                    DispatchQueue.main.async {
                        self.tableView?.reloadData()
                        self.caseCollectionView.reloadData()
                    }
                }
            }else{
                //                print("Error: \(error), \(error?.localizedDescription)")
                print("LOCALIZED DESCR: \(String(describing: error?.localizedDescription))")
                
                if error?.localizedDescription == "The Internet connection appears to be offline." {
                    let alertController = UIAlertController(title: "No Internet Connection", message: "Please connect to internet and try again", preferredStyle: .alert)
                    
                    let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        self.dismiss(animated: true, completion: nil)
                    })
                    
                    alertController.addAction(ok)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                }
                
            }
        }
    }*/
    
    
    /// gets the product pricing depnding on guest, member, distri
    /*func getProductPrice(){
        let query = PFQuery(className: "products")
        query.whereKey("code", equalTo: /*self.productCode*/ Int(self.productCode)!)
        query.fromLocalDatastore()
        query.findObjectsInBackground { (object: [PFObject]?, error) -> Void in
            if error == nil && object != nil {
                print("Product PRICING: \(String(describing: object))")
                
                DispatchQueue.global(qos: .userInitiated).async {
                    if let objects = object {
                        for object in objects {
                            let price = object["case_price"] as? String
                            DispatchQueue.main.async {
                                //                                self.caseCollectionView.isPrefetchingEnabled = true
                                self.caseCollectionView.layoutIfNeeded()
                                let caseIndex = IndexPath(row: 0, section: 0)
                                let cell = self.caseCollectionView.cellForItem(at: caseIndex)
                                //                                print("IndexPath: \(caseIndex)")
                                //                                print("CEll: \(String(describing: cell))")
                                
                                
                                if self.isUserDistributor() == true {
                                    if let price = object["distributor_case_pricing"] as? String {
                                        //                                        print("DISTRIBUTOR PRICE String: \(price)")
                                        
                                        if price == "" {
                                            cell?.isUserInteractionEnabled = false
                                            cell?.alpha = 0.2
                                        }
                                        
                                    } else if let price = object["distributor_case_pricing"] as? Int {
                                        //                                        print("Distrib int: \(price)")
                                        
                                        if price == 0 {
                                            cell?.isUserInteractionEnabled = false
                                            cell?.alpha = 0.2
                                        }
                                    }
                                }
                                
                                
                                if self.isUserMember() == true && self.isUserDistributor() == false {
                                    let price = object["member_case_pricing"] as? String
                                    
                                    if price == "" || price == nil {
                                        cell?.isUserInteractionEnabled = false
                                        cell?.alpha = 0.2
                                    }
                                    
                                    
                                }
                                    /*else if self.isUserMember() == false && self.isUserDistributor() == true {
                                     
                                     let price = object["distributor_case_pricing"] as? String
                                     
                                     if price == "" || price == nil {
                                     cell?.isUserInteractionEnabled = false
                                     cell?.alpha = 0.2
                                     }
                                     
                                     
                                 } */else if (self.isUserMember() == false && self.isUserDistributor() == false) {
                                    cell?.isUserInteractionEnabled = false
                                    cell?.alpha = 0.2
                                } else if (price == "" || price == nil) {
                                    cell?.isUserInteractionEnabled = false
                                    cell?.alpha = 0.2
                                }
                                
                            }
                        }
                        DispatchQueue.main.async {
                            self.pricesData = Array(object!.makeIterator())
                            
                        }
                    }
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        self.caseCollectionView.reloadData()
                        //                        print("HOW MANY: \(self.pricesData.count)")
                        //                        self.caseCollectionView.layoutIfNeeded()
                    }
                }
            } else{
                print("Error: \(String(describing: error)), \(String(describing: error?.localizedDescription))")
            }
        }
        
    }*/
    
    //for bv and pv points
    /*
    func newProductPrice() {
        let query = PFQuery(className: "pricing")
        query.fromLocalDatastore()
        query.whereKey("code", equalTo: /*self.productCode*/ Int(self.productCode)!)
        query.findObjectsInBackground { (objects, error) in
            if error == nil {
                //                print("NEW PRICING: \(objects!)")
                if let object = objects {
                    for objects in object {
                        let pricing = objects as! Pricing
                        DispatchQueue.main.async {
                            self.pvPoints = pricing.point_volume!
                            self.bvPoints = pricing.batch_volume!
//                            print("BV: \(pricing.batch_volume!) & PV: \(pricing.point_volume!)")
                        }
                    }
                }
            }
        }
    }*/
    
    /// determines whether the user is a mmember, distrib or guest
    ///
    /// - Returns: guest returns false on both because it would be nil
    func typeUser() {
        let neolifeID = UserFunctions.getNeolifeId()
        
        if Auth.auth().currentUser != nil {
            let ref = Database.database().reference(fromURL: "https://neolife-d9754.firebaseio.com/")
            
            if neolifeID != ""{
                ref.child("users").child(neolifeID).observe(.value, with: { (snap) in
                    if let value = snap.value as? [String : AnyObject] {
                        let distributor = value["is_distributor"] as? Bool
                        let member = value["is_member"] as? Bool
                        
                        if distributor == true && member == false {
                            self.isDistributor = true
                            self.isMember = false
                        } else if distributor == false && member == true {
                            self.isDistributor = false
                            self.isMember = true
                        } else {
                            self.isDistributor = false
                            self.isMember = false
                        }
                    }
                }, withCancel: nil)
            } else {
                print("NO ID STORED")
            }
        }
    }
}
