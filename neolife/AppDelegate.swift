//
//  AppDelegate.swift
//  neolife
//
//  Created by Karabo Moloi on 2016/12/19.
//  Copyright © 2016 Karabo Moloi. All rights reserved.
//

// PDF integration BETA

import UIKit
import CoreData
import Parse
import Bolts
//import Fabric
import Crashlytics
import Firebase
import SendGrid

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let currencyCode = "R"
    var activityIndicator = UIActivityIndicatorView()
    let itunesAppLink = "https://apps.apple.com/za/app/neolife-sa/id1225526810?mt=8"

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        Database.database().isPersistenceEnabled = true
        
        Parse.enableLocalDatastore()
//        Distributors.registerSubclass()
        
        let parseConfiguration = ParseClientConfiguration(block: { (ParseMutableClientConfiguration) -> Void in
            
            ParseMutableClientConfiguration.isLocalDatastoreEnabled = true
            ParseMutableClientConfiguration.applicationId = "47ce8e1c-d130-4a97-97e5-b62ef779cb83"
            ParseMutableClientConfiguration.clientKey = "47ce8e1c-d130-4a97-97e5-b62ef779cb83"
            ParseMutableClientConfiguration.server = "http://neolife.herokuapp.com/parse"
            
        })
        
        Parse.initialize(with: parseConfiguration)

        //style
        let navAppearance = UINavigationBar.appearance()
        let tabAppearance = UITabBar.appearance()
        
        navAppearance.barStyle = .black
        navAppearance.barTintColor = UIColor.greenNeolife()
        navAppearance.tintColor = UIColor.white
        tabAppearance.tintColor = UIColor.greenNeolife()
        
        // check if first time use, updated or same version
        
        let standardUserDefaults = UserDefaults.standard
        let appVersionKey = "CFBundleShortVersionString"
        let currentVersion = Bundle.main.infoDictionary![appVersionKey] as! String
        let prevVersion = standardUserDefaults.string(forKey: "appVersion")
        createIndicator()
        
        let catalogClass = CallFirebaseNode("product_catalog", storedIn: nil)
        let productClass = CallFirebaseNode("product_details", storedIn: nil)
        let pricelistClass = CallFirebaseNode("pricelist", storedIn: nil)
        let userClass = CallFirebaseNode("users", storedIn: nil)
        let distributor = CallFirebaseNode("distributors", storedIn: nil)
        
        if prevVersion == nil {
            //first install
            showIndicator()
            showVC()
            catalogClass.getClass()
            productClass.getClass()
            pricelistClass.getClass()
            userClass.getClass()
            distributor.getClass()
            removeAllFromCart()

        } else if prevVersion != currentVersion {
            // app update
            removeAllFromCart()
            distributor.getClass()
            userClass.getClass()
            // paper onboarding or alert will tell the user the updates if exciting
        } else if prevVersion == currentVersion {
            // no update
            //keep user logged in
            let firebaseAuth = Auth.auth()
            
            if firebaseAuth.currentUser != nil {
                let mainSb = UIStoryboard(name: "Main", bundle: nil)
                let homeVC = mainSb.instantiateViewController(withIdentifier: "homeVC")
                self.window?.rootViewController = homeVC
            } else {
                let mainSb = UIStoryboard(name: "Main", bundle: nil)
                let loginVC = mainSb.instantiateViewController(withIdentifier: "loginVC")
                self.window?.rootViewController = loginVC
            }
        }
        
        standardUserDefaults.set(currentVersion, forKey: "appVersion")
        standardUserDefaults.synchronize()
        
//        let sg_api_key = "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"
        let sg_api_key = "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"
//        guard let myApiKey = ProcessInfo.processInfo.environment[sg_api_key] else {
//            print("Unable to retrieve API key")
//            return
//        }
        
        Session.shared.authentication = Authentication.apiKey(sg_api_key)
        
        Fabric.with([Crashlytics.self])

        return true
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        // Make sure that URL scheme is identical to the registered one
        if url.scheme?.localizedCaseInsensitiveCompare(GatewayConfig.schemeURL) == .orderedSame {
            
            // Send notification to handle result in the view controller.
            NotificationCenter.default.post(name: Notification.Name(rawValue: GatewayConfig.asyncPaymentCompletedNotificationKey), object: nil)
            return true
        }
        return false
    }
    
    func createIndicator()  {
        activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.height)) as UIActivityIndicatorView
        
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = .white
        activityIndicator.backgroundColor = UIColor.lightGray
    }
    
    func showIndicator()  {
        activityIndicator.startAnimating()
        window?.rootViewController?.view.addSubview(activityIndicator)
    }
    
    func hideIndicator() {
        activityIndicator.stopAnimating()
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "neolife")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
//                fatalError("Unresolved error \(error), \(error.userInfo)")
                print("Error in the persistent container: \(error), \(error.localizedDescription)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                print("There's an error in the save context: \(nserror), \(nserror.localizedDescription)")
//                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

//    MARK: - Additional Functions
    func showVC() {
        var storyboard: UIStoryboard?
        storyboard = UIStoryboard(name: "Main", bundle: nil)
        let rootVC = storyboard!.instantiateViewController(withIdentifier: "initialVC")
        
        if let window = self.window{
            window.rootViewController = rootVC
        }
    }
    
//    Currency String Val
    func getStringValueFormattedAsCurrency(_ value: String) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.currency
        numberFormatter.currencyCode = currencyCode
        numberFormatter.maximumFractionDigits = 0
        
        let formattedValue = numberFormatter.string(from: NumberFormatter().number(from: value)!)
        return formattedValue!
    }
    
    func currentDate() -> String {
        let formatter = DateFormatter()
        let currentDate = Date()
        
        formatter.dateFormat = "dd.MM.yyyy"
//        formatter.locale = Locale(identifier: "en_ZA")
        
        let stringDate = formatter.string(from: currentDate)
        
        return stringDate
    }
    

    class func getAppDelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    //    shortcut to direcptry
    func getDocDir() -> String {
        return NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    }
    
    func removeAllFromCart() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let prodFetch : NSFetchRequest<ShoppingBag> = ShoppingBag.fetchRequest()
        
        let batchDelete = NSBatchDeleteRequest(fetchRequest: prodFetch as! NSFetchRequest<NSFetchRequestResult>)
        
        do {
            try managedContext.execute(batchDelete)
        } catch let error as NSError {
            print("couldnt delete, error: \(error), \(error.userInfo)")
        }
    }
    
    class func getCurrentAppVersion() -> String {
        let appVersionKey = "CFBundleShortVersionString"
        return Bundle.main.infoDictionary![appVersionKey] as! String
    }
    
    func getPrevAppVersion() -> String {
        let standardUserDefaults = UserDefaults.standard
        var version = ""
        if let prevVersion = standardUserDefaults.string(forKey: "appVersion") {
            version = prevVersion
        }
        return version
    }
    
    func getAppStoreVersion(completion: @escaping (String) -> ()) {
        let appId = "1225526810"
        let itunesUrl = "http://itunes.apple.com/lookup?id="
        
        guard let url = URL(string: itunesUrl + appId) else {
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                print("URL Error: ", error?.localizedDescription ?? "Response Error")
            } else {
                if let data = data {
                    do {
                        let jsonResponse = try JSONSerialization.jsonObject(with: data, options: [])
                        
                        guard let dict = jsonResponse as? [String: Any] else {
                            return
                        }
                        
                        if dict["resultCount"] as? Int == 1 {
                            guard let info = dict["results"] as? [[String: Any]] else {
                                return
                            }
                            
                            guard let version = info[0]["version"] as? String else {
                                return
                            }
                            completion(version)
                        }
                        
                    } catch let parseError {
                        print("JSON Error: ", parseError)
                    }
                }
            }
        }
        task.resume()
    }
    
    
}

