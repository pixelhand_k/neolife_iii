//
//  Distributor.swift
//  neolife
//
//  Created by Karabo Moloi on 2019/01/30.
//  Copyright © 2019 Karabo Moloi. All rights reserved.
//

import Foundation

class Distributor: NSObject {
    var name: String?
    var id_number: Int?
    var gpv: Int?
    var ppv: Int?
    var qpv: Int?
    var cc: Int?
}
