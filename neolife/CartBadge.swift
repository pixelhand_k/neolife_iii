//
//  BadgeButton.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/06/22.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//

import UIKit
import CoreData
import MIBadgeButton_Swift

class CartBadge {
    
    var toCartBtn : UIBarButtonItem! = nil
    var countString : String = ""
    var itemCount : Int = 0
    var controller : UIViewController?
    var segueId : String = ""
    
    func cartCount() -> Int {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchReq = NSFetchRequest<NSFetchRequestResult>(entityName: "ShoppingBag")
        
        var count : Int = 0
        
        do {
            let results = try managedContext.fetch(fetchReq)
            count = results.count
        } catch let error as NSError {
            print("fetch error \(error), \(error.userInfo)")
        }
        
        return count
    }
    
    func addBadgeToBtn(){
        let button = MIBadgeButton(type: .custom)
        button.badgeString = countString
        //        button.frame = CGRect(0, 0, 70, 40)
        button.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        button.badgeEdgeInsets = UIEdgeInsets.init(top: 15, left: 0, bottom: 0, right: 0)
        button.setImage(#imageLiteral(resourceName: "cart"), for: .normal)
        
        if segueId == "" {
            button.addTarget(self, action: #selector(altToCart), for: .touchUpInside)
        } else {
            button.addTarget(self, action: #selector(toCart), for: .touchUpInside)
        }

        self.toCartBtn = UIBarButtonItem(customView: button)
        
        
        controller?.navigationItem.rightBarButtonItem = toCartBtn
    }
    
    @objc func toCart(){
        controller?.performSegue(withIdentifier: segueId, sender: nil)
    }
    
    @objc func altToCart() {
        guard let vc = UIStoryboard(name: "Product", bundle: nil).instantiateViewController(withIdentifier: "cartVC") as? UINavigationController else {
            print("didnt work")
            return
        }
        controller?.present(vc, animated: true, completion: nil)
    }
    
}
