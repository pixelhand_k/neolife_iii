//
//  HistoryCell.swift
//  neolife
//
//  Created by Karabo Moloi on 2018/09/01.
//  Copyright © 2018 Karabo Moloi. All rights reserved.
//

import UIKit

class HistoryCell: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var docImg: UIImageView!
    
    
}
