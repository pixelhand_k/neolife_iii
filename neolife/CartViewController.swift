//
//  CartViewController.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/06/08.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//

//    MARK: This Controller is extended

import UIKit
import CoreData
import Firebase
import SendGrid

class CartViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIPopoverPresentationControllerDelegate {
    
    var cart = [NSManagedObject]()
//    var cartItems = [PFObject]()
    //    MARK: - Gateway Properties
    var checkoutProvider: OPPCheckoutProvider? //chekcout page
    var transaction: OPPTransaction?
    var paymentProvider: OPPPaymentProvider?
    var checkoutID = ""
    
    @IBOutlet weak var cartTableView: UITableView!
    @IBOutlet weak var totalTableView: UITableView!
    @IBOutlet weak var totalBtn: UIButton!
    
    //    MARK: - CSV PDF Properties
    var senderInfo = ""
    var distributor = "Guest"
    var idNumber = ""
    var userEmail = ""
    var invoiceNumber: String = ""
    var invoiceCount: Int?
    
    //    var addressNoBreaks = ""
    var addLineOne = ""
    var addLineTwo = ""
    var addLineThree = ""
    var postcode = ""
    var deliveryCost = 60
    var userDeliveryAddress = ""
    var userName : String!
    var userSurname : String!
    var uneditedAddress = String()
    
    let indexPath: IndexPath = IndexPath(row: 0, section: 0)

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getInvoiceCount()
        makeInvoiceNum()
        userAddress()
//        getCurrentInvoiceNumber()
        cartTableView.reloadData()
        totalTableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.cartTableView.reloadData()
        self.totalTableView.reloadData()
        cartTableView.delegate = self
        cartTableView.dataSource = self
        totalTableView.delegate = self
        totalTableView.dataSource = self
        
        self.title = "Cart"
        load() // loads CD
        totalBtn.layer.borderWidth = 1.0
        totalBtn.layer.borderColor = UIColor.paleGrey().cgColor
        totalBtn.layer.cornerRadius = 5.0
        totalBtn.layer.backgroundColor = UIColor.white.cgColor
        totalBtn.tintColor = UIColor.greenNeolife()
        
        self.navigationItem.prompt = "Swipe to delete"
        
        totalTableView.allowsSelection = false
    }
    
    struct totaliser {
        let items : String
    }

    let totalData = [totaliser(items: "Items in cart: ")]
    
//    MARK: - Bar button actions
    
    @IBAction func totalInfoBtn(_ sender: UIBarButtonItem) {
        let message = "All orders R1000 and above qualify for free delivery"
        HelpPopup.helpInfoWith(message, sender, in: self)
    }
    
    
    @IBAction func closeBtn(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
//    MARK: - Checkout Button
    @IBAction func checkoutBtn(_ sender: Any) {
        /*bagItems()*/ // v2 will be enabled

//        sendTestMail()
        
        var cost = ""
        let indexPath: IndexPath = IndexPath(row: 0, section: 0)
        let cell = totalTableView.cellForRow(at: indexPath) as! TotalCell
        
        let totalLabel = cell.totalLbl.text

        let subTotal = String.fromCurrency(totalLabel!)

        if subTotal > 0 {
            if cell.pickSwitch.isOn {
                cost = String.fromInt(integer: subTotal)
                print("total Cost: \(cost)")
            } else {
                let deliveryInt = deliveryCost
                let totalInt = subTotal
                cost = String(deliveryInt + totalInt)
                print("Total Cost: \(cost)")
            }
            paymentMethodActionSheet(cost)
        } else {
            Alerts.alert(on: self, title: "Cart Empty", and: "Please put at least one item in the cart to proceed")
        }
    }
    
//    MARK: - Switch
    @IBAction func pickupSiwtchPressed(_ sender: UISwitch) {
        let indexPath: IndexPath = IndexPath(row: 0, section: 0)
        let cell = totalTableView.cellForRow(at: indexPath) as! TotalCell
        
        if sender.isOn {
            //            cell.freightCostLbl.isEnabled = false
            deliveryCost = 0
            cell.freightCostLbl.text = AppDelegate().getStringValueFormattedAsCurrency(String.fromInt(integer: deliveryCost))
        } else {
            //            cell.freightCostLbl.isEnabled = true
            deliveryCost = 60
            cell.freightCostLbl.text = deliveryCalc(with: initGrossPrice())
        }
    }
    
//    MARK: - TableView Data Source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var rows : Int?
        
        if tableView == self.cartTableView {
            rows = cart.count
        }
        
        if tableView == self.totalTableView {
            rows = totalData.count
        }
        
        return rows!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.cartTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cartCell", for: indexPath) as! CartCell
            
            //setting what the delegate is going to control
            cell.delegate = self
            
            cell.selectionStyle = .none
            
            cell.addQuantityButton.layer.borderColor = UIColor.paleGrey().cgColor
            cell.addQuantityButton.layer.borderWidth = 0.5
            cell.addQuantityButton.layer.cornerRadius = cell.addQuantityButton.frame.height / 2
            
            cell.subtractQuantityButton.layer.borderColor = UIColor.paleGrey().cgColor
            cell.subtractQuantityButton.layer.borderWidth = 0.5
            cell.subtractQuantityButton.layer.cornerRadius = cell.addQuantityButton.frame.height / 2
            
            cell.prodPriceLbl.layer.borderColor = UIColor.greenNeolife().cgColor
            cell.prodPriceLbl.layer.borderWidth = 1.0
            cell.prodPriceLbl.layer.cornerRadius = 11.0
            cell.prodPriceLbl.textColor = .white
            cell.prodPriceLbl.layer.backgroundColor = UIColor.greenNeolife().cgColor
            
            cell.prodName.textColor = UIColor.darkGray
            cell.numberOfItems.textColor = UIColor.darkGray
            cell.numberOfItems.tintColor = UIColor.darkGray
            
            cell.prodType.textColor = .darkGray
            
            let myArray = cart[indexPath.row]
            
            cell.prodName.text = myArray.value(forKey: "name") as? String
            
            let numOfItems = myArray.value(forKey: "numItems") as? String
            
            let price = myArray.value(forKey: "price") as? String
            
            cell.prodType.text = myArray.value(forKey: "type") as? String
            
//            print("BV: \(myArray.value(forKey: "bv") as? String)")
//            print("BV: \(totalBv())")
            
            cell.unitPriceLbl.text = price
            
            if numOfItems != nil {
                cell.numberOfItems.text = numOfItems
            } else {
                cell.numberOfItems.text = nil
            }
            
//            let myImage = UIImage(data: (myArray.value(forKey: "Image") as? Data)!)
            if let imageUrl = myArray.value(forKey: "imageUrl") as? String {
                cell.productImage.loadImageFromCacheWithUrl(imageUrl)
            }

            let total = myArray.value(forKey: "total") as? String
            let rawValueTotal = total?.replacingOccurrences(of: "R", with: "")
            
            if rawValueTotal?.contains("ZA") == true {
                cell.prodPriceLbl.text = rawValueTotal!
            } else {
                cell.prodPriceLbl.text = AppDelegate().getStringValueFormattedAsCurrency(rawValueTotal!)
            }
            
            return cell
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "totalCell", for: indexPath) as! TotalCell
            
            cell.totalLbl.textColor = .greenNeolife()
            cell.freightCostLbl.textColor = .greenNeolife()
 
            cell.numberOfItemsLbl.text = totalData[indexPath.row].items + "\(initGrossItems())"
//            cell.totalLbl.text = "R" + initGrossPrice()
            cell.totalLbl.text = AppDelegate.getAppDelegate().getStringValueFormattedAsCurrency(String.fromInt(integer: initGrossPrice()))
            
//            cell.freightCostLbl.text = deliveryCalc()
            
            let pickSwitch = cell.pickSwitch
            
            if (pickSwitch?.isOn)! {
                //                cell.freightCostLbl.isEnabled = false
                deliveryCost = 0
                cell.freightCostLbl.text = AppDelegate().getStringValueFormattedAsCurrency(String.fromInt(integer: deliveryCost))
            } else {
                //                cell.freightCostLbl.isEnabled = true
                deliveryCost = 60
                cell.freightCostLbl.text = deliveryCalc(with: initGrossPrice())
            }
            
            return cell
        }
     }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == cartTableView {
            return 160.0
        } else {
            return 120.0
        }
    }
    
    //swipe to show delete
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if tableView == cartTableView {
            if editingStyle == .delete{
                let myArray = cart[indexPath.row]
                let name = myArray.value(forKey: "name") as? String
                
                deleteSelectedFromCD(name!)
                cart.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .top)
                self.cartTableView.reloadData()
                self.totalTableView.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        if tableView == totalTableView {
            return .none
        } else if tableView == cartTableView {
            return .delete
        }
        
        return .none
    }
    
//    MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let addressRaw = sender as! String
        var addressSplitCommas = addressRaw.components(separatedBy: ",")
        
        if segue.identifier == "depositSegue" {
            let destinationNavController = segue.destination as! UINavigationController
            let photoVC = destinationNavController.topViewController as! PhotoSlipVC
            let cell = totalTableView.cellForRow(at: indexPath) as! TotalCell
            
            if cell.pickSwitch.isOn {
                photoVC.grossTotal = AppDelegate.getAppDelegate().getStringValueFormattedAsCurrency(String.fromInt(integer: initGrossPrice() + deliveryCost))
                photoVC.senderInfo = sender as! String + " (PICKUP)"
                photoVC.recipientInfo = distributor
                photoVC.invoiceNumber = /*generateInvoiceNum()*/ self.invoiceNumber
                photoVC.userID = idNumber
                photoVC.userEmail = userEmail
                photoVC.addressOne = sender as! String + " (PICKUP)"
                photoVC.addressTwo = ""
                photoVC.addressThree = ""
                photoVC.code = ""
                photoVC.method = "Bank Deposit"
                photoVC.subTotal = String.fromInt(integer: initGrossPrice())
            } else {
                photoVC.grossTotal = AppDelegate.getAppDelegate().getStringValueFormattedAsCurrency(String.fromInt(integer: initGrossPrice() + deliveryCost))
                photoVC.senderInfo = addressSplitCommas[0] + "<br>" + addressSplitCommas[1] + "<br>" + addressSplitCommas[2] + "<br>" + addressSplitCommas[3]
                photoVC.recipientInfo = distributor
                photoVC.invoiceNumber = self.invoiceNumber
                photoVC.userID = idNumber
                photoVC.userEmail = userEmail
                //            previewVC.addressNoBreaks = addressNoBreaks
                //            previewVC.addressOne = addressRaw
                //            previewVC.addressTwo = addLineTwo
                //            previewVC.addressThree = addLineThree
                //            previewVC.code = postcode
                print("Address: " + addressSplitCommas[0] + addressSplitCommas[1] + addressSplitCommas[2] + addressSplitCommas[3])
                photoVC.addressOne = addressSplitCommas[0]
                photoVC.addressTwo = addressSplitCommas[1]
                photoVC.addressThree = addressSplitCommas[2]
                photoVC.code = addressSplitCommas[3]
                photoVC.method = "Bank Deposit"
                photoVC.subTotal = String.fromInt(integer: initGrossPrice())
                self.saveDeliverItemToCD()
            }
        }  else if segue.identifier == "pdfPreviewSegue" {
            
            let destinationNavController = segue.destination as! UINavigationController
            let pvc = destinationNavController.topViewController as! PreviewVC
            let cell = totalTableView.cellForRow(at: indexPath) as! TotalCell
            
            if cell.pickSwitch.isOn {
                pvc.invoiceNumber = self.invoiceNumber
                pvc.grossTotal = AppDelegate.getAppDelegate().getStringValueFormattedAsCurrency(String.fromInt(integer: initGrossPrice() + deliveryCost))
                pvc.senderInfo = sender as! String + " (PICKUP)"
                pvc.recipientInfo = distributor
                pvc.invoiceNumber = self.invoiceNumber
                pvc.userID = idNumber
                pvc.userEmail = userEmail
                //            pvc.addressNoBreaks = addressNoBreaks
                pvc.addressOne = sender as! String
                pvc.addressTwo = ""
                pvc.addressThree = ""
                pvc.code = postcode
                pvc.method = "EFT"
                pvc.subTotal = String.fromInt(integer: initGrossPrice())
                print("FRIEGHT")
            } else {
                print("NO Freight")
                pvc.invoiceNumber = self.invoiceNumber
                pvc.grossTotal = AppDelegate.getAppDelegate().getStringValueFormattedAsCurrency(String.fromInt(integer: initGrossPrice() + deliveryCost))
                pvc.senderInfo = addressSplitCommas[0] + "<br>" + addressSplitCommas[1] + "<br>" + addressSplitCommas[2] + "<br>" + addressSplitCommas[3]
                pvc.recipientInfo = distributor
                pvc.invoiceNumber = self.invoiceNumber
                pvc.userID = idNumber
                pvc.userEmail = userEmail
                pvc.addressOne = addressSplitCommas[0]
                pvc.addressTwo = addressSplitCommas[1]
                pvc.addressThree = addressSplitCommas[2]
                pvc.code = addressSplitCommas[3]
                pvc.method = "EFT"
                pvc.subTotal = String.fromInt(integer: initGrossPrice())
                saveDeliverItemToCD()
            }
        }
    }
    
    
    /// When order is complete, this is the fucntion that clears the cart from Core Data and dismisses the cart to go back to product menu
    ///
    /// - Parameter segue: segue from the PDF controller
    @IBAction func unwindToCart(segue: UIStoryboardSegue) {
        dismiss(animated: true, completion: nil)
        print("BACK TO CART")
        removeAllFromCart()
        cartTableView.reloadData()
        totalTableView.reloadData()
    }
    
    //    MARK: - Core Data Functions
    /// loads cart from core data
    func load() {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchReq = NSFetchRequest<NSFetchRequestResult>(entityName: "ShoppingBag")
        
        do {
            let results = try managedContext.fetch(fetchReq)
            cart = results as! [NSManagedObject]
            
            /*if results.count > 0 {
                let item = results[0] as! NSManagedObject
                print("1 - ITEM: \(item)")
                
                if let first = item.value(forKey: "name"){
                    print("\(first)")
                }
                
//                print("2 - ITEM: \(item)")
            }*/
            
            cartTableView.reloadData()
            totalTableView.reloadData()
        } catch let error as NSError {
            print("Could not load: \(error), \(error.userInfo)")
        }
    }
    
    //delete selected item from Core Data
    func deleteSelectedFromCD(_ name: String){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let prodFetch : NSFetchRequest<ShoppingBag> = ShoppingBag.fetchRequest()
        
        prodFetch.predicate = NSPredicate(format: "name == %@", name)
        
        let item = try! managedContext.fetch(prodFetch)
        
        for object in item {
            managedContext.delete(object)
        }
        
        do {
            try managedContext.save()
//            self.tableView.reloadData()
            self.cartTableView.reloadData()
            self.totalTableView.reloadData()
        } catch let error as NSError {
            print("couldnt delete, error: \(error), \(error.userInfo)")
            Alerts.alert(on: self, title: "Error", and: "\(String(describing: error.localizedFailureReason))")
        }
    }
    
    //delete all from Core Data
    func removeAllFromCart() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let prodFetch : NSFetchRequest<ShoppingBag> = ShoppingBag.fetchRequest()
        
        let batchDelete = NSBatchDeleteRequest(fetchRequest: prodFetch as! NSFetchRequest<NSFetchRequestResult>)
        
        
        do {
            try managedContext.execute(batchDelete)
//            self.tableView.reloadData()
            self.cartTableView.reloadData()
        } catch let error as NSError {
            print("couldnt delete, error: \(error), \(error.userInfo)")
        }
        
        print("Order Made")

//        Alerts.alert(on: self, title: "order complete", and: "it finished")
        //        checkoutAlert(title: "Checkout", message: "You have successfully checked out, thank you")
//        AlertFunctions().dismissAlert(self, title: "Checkout", message: "You have successfully checked out, Thank You")
    }
    
    func totalBv() -> String {
        let array = cart
        var points = 0
        
        for item in array {
            let bv = item.value(forKey: "bv") as! String
            let bvInt = String.toInt(string: bv)
            points += bvInt
        }
        
        return String.fromInt(integer: points)
    }
    
    func initGrossPrice() -> Int {
        let array = cart
        var total = 0
        
        for item in array {            
            let priceTotal = item.value(forKey: "total") as! String
            let noStringTotal = priceTotal.replacingOccurrences(of: "R", with: "")
            let totalInt = String.toInt(string: noStringTotal)
            total += totalInt
        }
        return total
    }
    
    func initGrossItems() -> String {
        let cartArray = cart
        var totalItems = 0
        
        for items in cartArray {
            let itemCountString = items.value(forKey: "numItems") as! String
            let itemToInt = String.toInt(string: itemCountString)
            totalItems += itemToInt
        }
        
        return String.fromInt(integer: totalItems)
    }
    
    func deliveryCalc() -> String {
        var totalPv = 0
        var stringVal = ""
        for index in 0..<cart.count {
            let itemArray = cart[index]
            
            let pv = itemArray.value(forKey: "pv") as? String
            let pvInt = String.toInt(string: pv!)
            
            totalPv += pvInt
            
        }
        print("PV: \(totalPv)")
        if totalPv >= 100 {
            deliveryCost = 0
            stringVal = String.fromInt(integer: deliveryCost)
        } else {
            deliveryCost = 60
            stringVal = String.fromInt(integer: deliveryCost)
        }
        
        return "+" + AppDelegate().getStringValueFormattedAsCurrency(stringVal)
    }
    
    func deliveryCalc(with total: Int) -> String {
        
        var cost = ""
        
        if total >= 1000 {
            deliveryCost = 0
            cost = String.fromInt(integer: deliveryCost)
        } else {
            deliveryCost = 60
            cost = String.fromInt(integer: deliveryCost)
        }
        
        return "+" + AppDelegate().getStringValueFormattedAsCurrency(cost)
    }
    
    func saveDeliverItemToCD() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "ShoppingBag", in: managedContext)
        
        let shoppingBag = ShoppingBag(entity: entity!, insertInto: managedContext)
        
        shoppingBag.code = 0
        shoppingBag.name = "Delivery"
        shoppingBag.numItems = "1"
        shoppingBag.bv = "0"
        shoppingBag.pv = "0"
        shoppingBag.total = String.fromInt(integer: deliveryCost)
        shoppingBag.type = "Single"
        shoppingBag.price = "60"
        
        do {
            try managedContext.save()
            print("Added delivery to CD")
        } catch let error as NSError {
            print("Error: \(error), \(error.localizedDescription)")
        }
    }
    
//    MARK: - Backend functions
    func userAddress(){
        
        let neolifeID = UserFunctions.getNeolifeId()
        
        if Auth.auth().currentUser != nil {
            
            if neolifeID != "" {
                let ref = Database.database().reference(fromURL: "https://neolife-d9754.firebaseio.com/")
                ref.child("users").child(neolifeID).observeSingleEvent(of: .value, with: { (snap) in
                    if let value = snap.value as? [String : AnyObject] {
                        let email = value["email"] as? String
                        let name = value["name"] as? String
                        let address = value["address"] as? String
                        let idNumber = value["id_number"] as? Int
//                        let countryCode = value["cc"] as? String
                        
                        let addressArr = address?.components(separatedBy: ",")
                        
                        var addressOne = ""
                        var addressTwo = ""
                        var city = ""
                        var code = ""
                        
                        if let addressArray = addressArr {
                            if addressArray.count > 1 {
                                addressOne = addressArray[0]
                                addressTwo = addressArray[1]
                                city = addressArray[2]
                                code = addressArray[3]
                            } else if addressArray.count == 0 {
                                Alerts.alert(on: self, title: "No Address Found", and: "Please go to profile and enter a valid address or enter a delivery address")
                            } else {
                                addressOne = addressArray[0]
                            }
                        }
                        
                        var firstname = ""
                        var surname = ""
                        
                        if let unwrappedName = name {
                            if unwrappedName.wordCount > 2 {
                                let usename = unwrappedName.firstWords(2)
                                firstname = usename[0] + "&" + usename[1]
                                surname = unwrappedName.lastWord
                            } else {
                                let usename = unwrappedName.firstWords(1)
                                firstname = usename[0]
                                surname = unwrappedName.lastWord
                            }
                        }
                        
                        if addressArr == nil {
                            self.senderInfo = "Please login to place your address here"
                            Alerts.alert(on: self, title: "Update your address", and: "Update your address in your profile from home")
                        }
                        
                        DispatchQueue.main.async {
                            //                "Distributer Name<br>123 Somewhere Str.<br>10000 - MyCity<br>MyCountry"
                            self.senderInfo = "\(name!)<br>\(addressOne), \(addressTwo)<br>\(city)<br>\(code)"
                            self.idNumber = "\(idNumber!)"
                            //                        self.invoiceNumber = String.fromInt(integer: idNumber!) + "" + countryCode!
                            self.userEmail = email!
                            self.distributor = name!
                            //                self.addressNoBreaks = "\(addressOne!), \(addressTwo!), \(city!), \(code!)"
                            self.userName = firstname
                            self.userSurname = surname
                            
                            self.addLineOne = "\(addressOne)"
                            self.addLineTwo = "\(addressTwo)"
                            self.addLineThree = "\(city)"
                            self.postcode = "\(code)"
                        }
                    }
                }, withCancel: nil)
            } else {
                Alerts.idAlert(on: self)
            }
        }
        
        
        
        
        
        /*if PFUser.current() != nil {
            let currentUser = PFUser.current()
            
//            print("USER: \(String(describing: currentUser))")
            let email = currentUser?["email"] as? String
            let name = currentUser?["Name"] as? String
            let address = currentUser?["address"] as? String
            let idNumber = currentUser?["id_number"] as? String
            let countryCode = currentUser?["cc"] as? String
            
            let addressArr = address?.components(separatedBy: ",")
            
            var addressOne = ""
            var addressTwo = ""
            var city = ""
            var code = ""
            
            if let addressArray = addressArr {
                if addressArray.count > 1 {
                    addressOne = addressArray[0]
                    addressTwo = addressArray[1]
                    city = addressArray[2]
                    code = addressArray[3]
                } else if addressArray.count == 0 {
                    Alerts.alert(on: self, title: "No Address Found", and: "Please go to profile and enter a valid address or enter a delivery address")
                } else {
                    addressOne = addressArray[0]
                }
            }
            
            let fullname = name?.components(separatedBy: " ")
            let firstname = fullname![0]
            let surname = fullname![1]

//            let addressOne = addressArr?[0]
//            let addressTwo = addressArr?[1]
//            let city = addressArr?[2]
//            let code = addressArr?[3]
            
            if addressArr == nil {
                self.senderInfo = "Please login to place your address here"
                Alerts.alert(on: self, title: "Update your address", and: "Update your address in your profile from home")
            }
            
            DispatchQueue.main.async {
                //                "Distributer Name<br>123 Somewhere Str.<br>10000 - MyCity<br>MyCountry"
                self.senderInfo = "\(name!)<br>\(addressOne), \(addressTwo)<br>\(city)<br>\(code)"
                self.idNumber = "\(idNumber!)"
                self.invoiceNumber = idNumber! + "" + countryCode!
                self.userEmail = email!
                self.distributor = name!
//                self.addressNoBreaks = "\(addressOne!), \(addressTwo!), \(city!), \(code!)"
                self.userName = firstname
                self.userSurname = surname
                
                self.addLineOne = "\(addressOne)"
                self.addLineTwo = "\(addressTwo)"
                self.addLineThree = "\(city)"
                self.postcode = "\(code)"
                
            }
        } else {
            self.senderInfo = "Please login to place your address here"
            Alerts.alert(on: self, title: "Login to enter address", and: "Login and update your address in your profile from home ")
        }*/
    }
    
    
    func getInvoiceCount() {
        let neolifeID = UserFunctions.getNeolifeId()
        
        if Auth.auth().currentUser != nil {
            if neolifeID != "" {
                let ref = Database.database().reference(fromURL: "https://neolife-d9754.firebaseio.com/")
                ref.child("users").child(neolifeID).observeSingleEvent(of: .value, with: { (snap) in
                    if let value = snap.value as? [String: AnyObject] {
                        let count = value["invoiceCount"] as? Int
                        
                        if let ivCount = count {
                            self.invoiceCount = ivCount
                        }
                    }
                }, withCancel: nil)
            }
        }
    }
    
    func makeInvoiceNum() {
        let neolifeID = UserFunctions.getNeolifeId()
        
        if Auth.auth().currentUser != nil {
            
            if neolifeID != "" {
                let ref = Database.database().reference(fromURL: "https://neolife-d9754.firebaseio.com/")
                ref.child("users").child(neolifeID).observeSingleEvent(of: .value, with: { (snap) in
                    if let value = snap.value as? [String: AnyObject] {
                        let count = value["invoiceCount"] as? Int
                        let id = value["id_number"] as? Int
                        let email = value["email"] as? String
                        
                        if id != nil{
                            if let ic = count {
                                if let id = id {
                                    let invoiceNum = "\(id)" + "0" + "\(ic)"
                                    self.invoiceNumber = invoiceNum
                                }
                            }
                        } else {
                            if let email = email {
                                if let count = count {
                                    let invoiceNum = "1234" + "\(email)" + "0" + "\(count)"
//                                    print("INVOICE: \(invoiceNum)")
                                    self.invoiceNumber = invoiceNum
                                }
                            }
                        }
                    }
                }, withCancel: nil)
            } else {
                print("NO ID PRESENT")
            }
        }
    }

    
    /*func getCurrentInvoiceNumber() -> Int {
        //invoice number

        if PFUser.current() != nil {
            let currentUser = PFUser.current()
            
            
            let invoice = currentUser?["iphone_invoice_count"] as? Int
//            let id = currentUser?["id_number"] as? String
//            let email = currentUser?["email"] as? String
            
            if invoice == nil{
                currentUser?["iphone_invoice_count"] = 0
                currentUser?.saveInBackground()
                print("SAVED")
                return 0
            } else {
                /*if id != nil{
                    let invoiceNum = "\(id!)" + "0" + "\(String.fromInt(integer: invoice!))"
                    print("INVOICE: \(invoiceNum)")
                    return invoiceNum
                } else {
                    let invoiceNum = "1234" + "\(email!)" + "0" + "\(String.fromInt(integer: invoice!))"
                    print("INVOICE: \(invoiceNum)")
                    return invoiceNum
                }*/
                return invoice!
            }
            
        }
        
        return 0
    }*/
    
    /*func generateInvoiceNum() -> String {
        if PFUser.current() != nil {
            let currentUser = PFUser.current()
            
            let id = currentUser?["id_number"] as? String
            let email = currentUser?["email"] as? String
            
//            incrementInvoiceNumber()
            
            if id != nil{
                let invoiceNum = "\(id!)" + "0" + "\(getCurrentInvoiceNumber())"
                print("INVOICE: \(invoiceNum)")
                return invoiceNum
            } else {
                let invoiceNum = "1234" + "\(email!)" + "0" + "\(getCurrentInvoiceNumber())"
                print("INVOICE: \(invoiceNum)")
                return invoiceNum
            }
        }
        
        return ""
        
    }*/
    
}

