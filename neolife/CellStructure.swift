//
//  CellStructure.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/06/22.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//

import UIKit
import Parse
import CoreData

class CellStructure {
    
    let iphoneSeScreenWidth : CGFloat = 320
    let iphonePlusWidth : CGFloat = 414
    let iphoneWidth : CGFloat = 375
    
    func greyBorder(_ cell: UICollectionViewCell) {
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.cornerRadius = 5
    }
    
    func dynamicCollectionCell(_ collectionView: UICollectionView) -> CGSize {
        //sets the cell sizes according to the screen size, so it can dynamically adjust
        var cellNum : CGFloat = 0
        let heightDiff : CGFloat = 30
        let sidePadding : CGFloat = 30
        
        let screenWidth = collectionView.frame.width
        var width: CGFloat = 0
        var height: CGFloat = 0
        //        let width = ((collectionView?.frame.width)! - sidePadding) / numberOfCells
        //        let height = width + heightDifference
        if screenWidth <= iphoneSeScreenWidth {
            cellNum = 2.0
            width = (screenWidth - sidePadding) / cellNum
            height = width + heightDiff
            //            result = CGSize(width: width, height: height)
            return CGSize(width: width, height: height)
        } else {
            cellNum = 3.0
            width = (screenWidth - sidePadding) / cellNum
            height = width + heightDiff
            //            result = CGSize(width: width, height: height)
            return CGSize(width: width, height: height)
        }
    }
    
    func caseSelecetionCellSize(_ collectionView: UICollectionView) -> CGSize {
        //sets the cell sizes according to the screen size, so it can dynamically adjust
        //        let cellNum : CGFloat = CGFloat(self.productDetail.count)
        let cellNum : CGFloat = 2
        let sidePadding : CGFloat = 20
        //        let iphoneSeScreenWidth : CGFloat = 320
        //        let screenWidth = caseCollectionView?.frame.width
        //        var width: CGFloat = 0
        //        var height: CGFloat = 0
        
        let width = ((collectionView.frame.width) - sidePadding) / cellNum
        //        let height = width - heightDiff
        
        return CGSize(width: width, height: 80)
    }
    
    func homeCollectionCellSize(_ collectionView: UICollectionView) -> CGSize {
        let screenWidth = collectionView.frame.width
        
        if screenWidth <= iphoneSeScreenWidth {
            return CGSize(width: collectionView.frame.width - 10, height: 180)
        } else if  screenWidth == iphoneWidth {
            return CGSize(width: collectionView.frame.width - 10, height: 200)
        } else {
            return CGSize(width: collectionView.frame.width - 10, height: 230)
        }
        
    }
}
