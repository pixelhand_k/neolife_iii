//
//  PromotionItems.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/02/24.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//

import UIKit

class PromotionItem {
    var title : String
    var coverImage : UIImage
    
    init(title: String, coverImage: UIImage) {
        self.title = title
        self.coverImage = coverImage
    }
}
