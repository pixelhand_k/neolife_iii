//
//  User.swift
//  neolife
//
//  Created by Karabo Moloi on 2018/06/20.
//  Copyright © 2018 Karabo Moloi. All rights reserved.
//

import UIKit

class User: NSObject {
    var username : String!
    var email : String!
    var profileImgUrl : String?
    var cc = "26"
    var invoiceCount : Int?
    var name : String?
    var gpv : Int?
    var id : Int!
    var is_member : Bool?
    var is_distributor : Bool?
    var address : String?
    var ppv : Int?
    var is_profile_set : Bool?
    var qpv : Int!
    var title : Int!
    
}
