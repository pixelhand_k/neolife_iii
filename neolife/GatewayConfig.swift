//
//  GatewayConfig.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/11/05.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//

import UIKit

class GatewayConfig: NSObject {
    
    static let asyncPaymentCompletedNotificationKey = "AsyncPaymentCompletedNotificationKey"
    static let schemeURL = "com.neolifeafrica.app"
//    static let schemeURL = "msdk.demo.async"
    
    
    static func configureCheckoutSettings() -> OPPCheckoutSettings {
        let checkoutSettings = OPPCheckoutSettings.init()
        let paymentbrands = ["VISA","MASTER"]
        checkoutSettings.paymentBrands = paymentbrands
        checkoutSettings.schemeURL = schemeURL
        checkoutSettings.storePaymentDetails = .never
        
        
        checkoutSettings.theme.navigationBarBackgroundColor = .greenNeolife()
        checkoutSettings.theme.confirmationButtonColor = .greenNeolife()
        checkoutSettings.theme.cellHighlightedBackgroundColor = .greenNeolife()
        checkoutSettings.theme.sectionBackgroundColor = UIColor.greenNeolife().withAlphaComponent(0.05)
        
        return checkoutSettings
    }
}
