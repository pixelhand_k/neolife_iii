//
//  Distributor.swift
//  neolife
//
//  Created by Karabo Moloi on 2019/01/30.
//  Copyright © 2019 Karabo Moloi. All rights reserved.
//

import Foundation
import Firebase

class DistributorList {
    static func getDistributors(id: String) {
        let ref = Database.database().reference(fromURL: "https://neolife-d9754.firebaseio.com/")
        let distributor = Distributor()
        ref.child("distributors").queryOrdered(byChild: "id_number").queryEqual(toValue: String.toInt(string: id)).queryLimited(toFirst: 1).observeSingleEvent(of: .childAdded, with: { (snap) in
            if let dict = snap.value as? [String: AnyObject] {
                distributor.cc = dict["cc"] as? Int
                distributor.ppv = dict["ppv"] as? Int
                distributor.qpv = dict["qpv"] as? Int
                distributor.gpv = dict["gpv"] as? Int
                distributor.name = dict["Name"] as? String
                distributor.id_number = dict["id_number"] as? Int
            }
            
            print("Distributor: ", distributor.name)
        }, withCancel: nil)
    }
}
