//
//  Request.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/11/04.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//

import UIKit

class Request: NSObject {
    
    // Test merchant server domain, replace with ours
    static let serverDomain = "https://us-central1-neolife-d9754.cloudfunctions.net/"
    
    static func requestCheckoutID (controller: UIViewController, amount: String, email: String, name: String, neolifeId: String, invoiceNum: String, completion: @escaping (String?) -> Void) {
//        let body = "amount=\(amount)" + "&currency=\(AppDelegate().ISOCurrencyCode)" + "&shopperResultUrl=\(GatewayConfig.schemeURL)://payment" + "&paymentType=DB"
        let body = "getMultipleParamsCheckoutID?text=" + "\(amount)" + "&merchantTransactionId=\(invoiceNum)" + "&neolifeId=\(neolifeId)" + "&name=\(name)" + "&email=\(email)"

//        let body = "getMultipleParamsCheckoutIDTest?text=" + "\(amount)" + "&merchantTransactionId=\(invoiceNum)" + "&neolifeId=\(neolifeId)" + "&name=\(name)" + "&email=\(email)"
//        print("Parameters: \(body)")
//        let url = serverDomain + "/token?" + body
        let url = serverDomain + body
        let request = NSURLRequest(url: URL(string: url)!)
        URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            print("Response Data : \(String(describing: response))")
            
            if error != nil {
                print("Error: \(String(describing: error?.localizedDescription))")
                
                Alerts.alert(controller, "Error", "\(String(describing: error?.localizedDescription))", {
                    controller.dismiss(animated: true, completion: nil)
                })
            } else {
                if let data = data , let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                    //                print("json DATA: \(String(describing: json))")
//                    let checkoutID = json?["checkoutId"] as? String
                    let checkoutID = json?["checkoutId"] as? String
                    print("JSON: \(String(describing: checkoutID))")
                    completion(checkoutID)
                } else {
                    completion(nil)
                }
            }
        }.resume()
    }
    
    static func requestPaymentStatus(resourcePath: String, completion: @escaping (Bool) -> Void) {
//        let url = serverDomain + "checkPaymentStatusTest?text=\(resourcePath)"
        let url = serverDomain + "checkPaymentStatus?text=\(resourcePath)"
        let request = NSURLRequest(url: URL(string: url)!)
        URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            print("RESPONSE request payment: \(String(describing: response))")
            if let data = data, let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                let transactionStatus = json?["paymentResult"] as? String
                print("JSOON: \(String(describing: json))")
                completion(transactionStatus == "OK")
                
            } else {
                completion(false)
            }
            }.resume()
    }
}
