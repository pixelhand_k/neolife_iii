//
//  PhotoSlipVC.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/07/11.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//

import UIKit
import MessageUI
import CoreData
import Firebase
import Crashlytics
import SendGrid

class PhotoSlipVC: UIViewController, MFMailComposeViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var promptLbl: UILabel!
    @IBOutlet weak var photoView: UIImageView!
    var cartInfo = [NSManagedObject]()
    
    var composer : InvoiceComposer!
    var csvComposer : CsvComposer!
    
    var htmlContent : String!
    
    //pass on total in segue to vc
    var grossTotal = ""
    var senderInfo = ""
    var recipientInfo = ""
    var invoiceNumber = ""
    var fullinvoiceNum = ""
    var method = ""
    var csvContent = ""
    var userEmail = ""
    var userID = ""
    var addressNoBreaks = ""
    var subTotal = ""
    var addressOne = ""
    var addressTwo = ""
    var addressThree = ""
    var code = ""    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        load()
        invoiceToHTML()
        invoiceToCSV()
    }
    
    @IBAction func cameraOptions(_ sender: UIBarButtonItem) {
        cameraActionSheet()
    }
    
    //    MARK: - Send Order
    func sendOrderWith(_ image: UIImage) {
        print("Sending...")
        
//        let personalization = Personalization(recipients: "ordera@za.neolife.com")
        let personalization = Personalization(recipients: "karabo93@hotmail.co.uk")
        let plainText = Content(contentType: ContentType.plainText, value: "Order Number: \(invoiceNumber)")
        let htmlText = Content(contentType: ContentType.htmlText, value: "<h1>Order Number: \(invoiceNumber)</h1>")
        
        let email = Email(
            personalizations: [personalization],
            from: Address(email: "moloikarabo3@gmail.com"),
            content: [plainText, htmlText],
            subject: "NeoApp" + "\(invoiceNumber)"
        )
        
        photoView.image = image
        let photoData = photoView.image!.jpegData(compressionQuality: 0.5)! as NSData
        
//        get pdf, csv then photo
        composer.exportHTMLContentToPDF(HTMLContent: htmlContent) // converst html string to pdf and saves it
        if let invoiceData = NSData(contentsOfFile: composer.pdfFileName) {
            if let csvData = NSData(contentsOf: csvComposer.exportCSV(csvContent) as URL) {
                photoView.image = image
//                let photoData = photoView.image!.jpegData(compressionQuality: 0.5)! as NSData
                
                let pdfAttachment = Attachment(filename: "order\(invoiceNumber).pdf", content: invoiceData as Data, disposition: .attachment, type: .pdf, contentID: nil)
                
                let csvAttachment = Attachment(filename: csvComposer.fileName, content: csvData as Data, disposition: .attachment, type: .csv, contentID: nil)
                
                let imgAttachment = Attachment(filename: "slip.jpg", content: photoData as Data, disposition: .attachment, type: .jpeg, contentID: nil)
                
                email.attachments = [pdfAttachment, csvAttachment, imgAttachment]
            }
        }
        
        do {
            try Session.shared.send(request: email, completionHandler: { (response) in
                print("Email Repsonse: \(response?.httpUrlResponse.debugDescription)")
            })
//            try Session.shared.send(request: email)
            uploadUserOrderIf(isSent: true, photoData)
            Alerts.orderComplete(on: self, with: {
                print("Sent")
                self.performSegue(withIdentifier: "unwindToCartSegue", sender: nil)
            })
            
        } catch {
            print(error)
            uploadUserOrderIf(isSent: false, photoData)
            CLSLogv("%@", getVaList(["Send Grid error: \(error)"]))
            self.dismiss(animated: true) {
                Alerts.alert(on: self, title: "Order failed to complete", and: "The invoice failed to send, please check order history or email support@neolifeafrica.co.za")
            }
        }
    }
    
    
//    to email function attaching image
    func toEmailWithImage(_ image: UIImage){
        if MFMailComposeViewController.canSendMail() {
            let mailComposerVC = MFMailComposeViewController()
            mailComposerVC.setSubject("NeoApp" + "\(invoiceNumber)")
            mailComposerVC.mailComposeDelegate = self
            mailComposerVC.setToRecipients(["ordera@za.neolife.com"])
            do {
                composer.exportHTMLContentToPDF(HTMLContent: htmlContent) // converst html string to pdf and saves it
                try mailComposerVC.addAttachmentData(NSData(contentsOfFile: composer.pdfFileName) as Data, mimeType: "application/pdf", fileName: "Order.pdf") // attaches pdf
                photoView.image = image
//                let photoSlip : NSData = UIImagePNGRepresentation(photoView.image!)! as NSData
                
                let photoSlip : NSData = photoView.image!.jpegData(compressionQuality: 0.5)! as NSData
                mailComposerVC.addAttachmentData(photoSlip as Data, mimeType: "image/jpg", fileName: "slip.jpg") // attaches images
                if let csvData = NSData(contentsOf: csvComposer.exportCSV(csvContent) as URL) {
                    print("CSV URL: \(csvData)")
                    mailComposerVC.addAttachmentData(csvData as Data, mimeType: "txt/text", fileName: csvComposer.fileName)
                    print("CSV filename: \(csvComposer.fileName)")
                }
            } catch {
                print("Not Found")
            }
            present(mailComposerVC, animated: true, completion: nil)
        } else {
//            Add alert telling them to add email account to Mail App
            print("Fail to send")
            Alerts.alert(on: self, title: "Email account not setup", and: "Please go to Setting -> Accounts & Passwords and add a valid email account to the Mail app, then add a photo again to complete your order.")
        }
    }
    
//    MARK: - Close
    @IBAction func cancelPhoto(_ sender: UIBarButtonItem) {
//        dismiss(animated: true, completion: nil)
        deleteSelectedFromCD("Delivery")

    }
    
//    MARK: - Email Delegate method
//    mail composer controller finish method (when sent successful run checkut function)
    /*
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {

        switch result {
        case .sent:
            self.photoView.isHidden = true
            self.promptLbl.isHidden = false
            
            uploadUserOrderIf(isSent: true)
            controller.dismiss(animated: true) {
                Alerts.orderComplete(on: self, with: {
                    print("Sent")
                    self.performSegue(withIdentifier: "unwindToCartSegue", sender: nil)
                })
            }
        case .cancelled:
            print("Cancelled")
            controller.dismiss(animated: true) {
                Alerts.alert(on: self, title: "Cancel?", and: "Select a different photo to proceed with your order or press the X to cancel your order.")
            }
            
        case .failed:
            print("failed")
            uploadUserOrderIf(isSent: false)
            //            Add failed orders to core data and alert to tell them what to do
            controller.dismiss(animated: true) {
                 Alerts.alert(on: controller, title: "Failed", and: "The invoice failed to send, please check order history or email support@neolifeafrica.co.za")
            }

        case .saved:
            print("Saved")
            controller.dismiss(animated: true) {
                self.uploadUserOrderIf(isSent: true)
                Alerts.saveOrderAlert(in: self, completion: {
                    print("Saved")
                    self.performSegue(withIdentifier: "unwindToCartSegue", sender: nil)
                })
            }
        }
        
        if error != nil {
            uploadUserOrderIf(isSent: false)
            CLSLogv("%@", getVaList(["Mail compose error: \(error.debugDescription)"]))
            controller.dismiss(animated: true) {
                Alerts.alert(on: self, title: "Email failed to load", and: error?.localizedDescription)
            }
        }
    }
    */
    
//  MARK: -  CD functions
    func load() {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchReq = NSFetchRequest<NSFetchRequestResult>(entityName: "ShoppingBag")
        
        //        let req = Cart.fetchRequest()
        
        do {
            let results = try managedContext.fetch(fetchReq)
            cartInfo = results as! [NSManagedObject]
        } catch let error as NSError {
            print("Could not load: \(error), \(error.userInfo)")
        }
        
    }
    
    //delete selected item from Core Data
    func deleteSelectedFromCD(_ name: String){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let prodFetch : NSFetchRequest<ShoppingBag> = ShoppingBag.fetchRequest()
        
        prodFetch.predicate = NSPredicate(format: "name == %@", name)
        
        let item = try! managedContext.fetch(prodFetch)
        
        for object in item {
            managedContext.delete(object)
        }
        
        do {
            try managedContext.save()
            print("removed \(name) form CD")
            self.dismiss(animated: true, completion: nil)
        } catch let error as NSError {
            print("couldnt delete, error: \(error), \(error.userInfo)")
            Alerts.alert(on: self, title: "Error", and: "\(String(describing: error.localizedFailureReason))")
        }
    }
    
//    MARK: - invoice generation
    func invoiceToHTML() {
        composer = InvoiceComposer()
        
        let date = AppDelegate.getAppDelegate().currentDate()
        
        if let invoiceHTML = composer.infoToInvoice(invoiceNum: invoiceNumber,
                                                    date: date,
                                                    address: senderInfo,
                                                    recipientInfo: recipientInfo,
                                                    items: cartInfo,
                                                    totalAmount: grossTotal,
                                                    method: method) {
            
//            pdfPreviewWebView.loadHTMLString(invoiceHTML, baseURL: NSURL(string: composer.invoiceHTMLPath!)! as URL)
            htmlContent = invoiceHTML
        }
    }
    
    func invoiceToCSV() {
        csvComposer = CsvComposer()
        
        csvContent = csvComposer.createCSV(with: cartInfo, addressOne: addressOne, addressTwo: addressTwo, addressThree: addressThree, code: code, DistributorName: recipientInfo, neolifeID: userID, email: userEmail, invoiceNum: invoiceNumber, subtotal: subTotal, referenceNum: "NeoApp" + invoiceNumber, paymentType: method)
    }
    
//    MARK: - Camera/Image Picker functions
    func cameraActionSheet() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        let cameraAlert = UIAlertController(title: "Photo from?", message: "Choose where you getting your from", preferredStyle: .actionSheet)
        
        let library = UIAlertAction(title: "Photo Library", style: .default) { (action) in
            imagePicker.sourceType = .photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
            self.photoView.isHidden = false
            self.promptLbl.isHidden = true
        }
        
        let camera = UIAlertAction(title: "Camera", style: .default) { (action) in
            imagePicker.sourceType = .camera
            imagePicker.cameraFlashMode = .off
            imagePicker.cameraCaptureMode = .photo
            self.present(imagePicker, animated: true, completion: nil)
            self.photoView.isHidden = false
            self.promptLbl.isHidden = true
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        cameraAlert.addAction(library)
        cameraAlert.addAction(camera)
        cameraAlert.addAction(cancel)
        cameraAlert.view.tintColor = UIColor.greenNeolife()
        self.present(cameraAlert, animated: true, completion: nil)
    }
    
    // didfinishpacking.. here with to email function
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        if let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            picker.dismiss(animated: true, completion: nil)
            sendOrderWith(image)
        } else {
            CLSLogv("Image: %@", getVaList(["Nil Value"]))
        }
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

//Extended

//Add to separate extention swift file. Potentially its own class for online payment
extension PhotoSlipVC {
//    func saveOrder(withTotal total: String, time: String, date: String, isSent: Bool, neolifeID: String, csv: String, pdf: String, image: String) {
//        let ref = Database.database().reference(fromURL: "https://neolife-d9754.firebaseio.com/")
//        let values = ["orderNum": Date.toMilliseconds(), "invoice": pdf, "total": total, "time": time, "date": date, "isSent": isSent, "csv": csv, "neolifeID": neolifeID, "paymentType": "Bank Deposit", "popImage": image] as [String : Any]
//
//        ref.child("orders").child("\(neolifeID)").child("\(Date.toMilliseconds())").updateChildValues(values)
//        print("Saved To Store")
//    }
    
    //    MARK: - Storage to Firebase
    func uploadUserOrderIf(isSent: Bool, _ image: NSData) {
        
        //        let orderName = invoiceNumber
        let neolifeId = UserFunctions.getNeolifeId()
        
        let pdfRef = Storage.storage().reference().child("orders").child("\(Date.toMilliseconds()).pdf")
        let csvRef = Storage.storage().reference().child("orders").child("\(Date.toMilliseconds()).csv")
        let imgRef = Storage.storage().reference().child("orders").child("\(Date.toMilliseconds()).")
        
        // time and date
        let date = Date()
        let dateFormatter = DateFormatter()
        let timeFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        dateFormatter.timeZone = TimeZone(identifier: "en_ZA")
        dateFormatter.locale = Locale(identifier: "en_ZA")
        timeFormatter.timeStyle = .medium
        timeFormatter.locale = Locale(identifier: "en_ZA")
        timeFormatter.timeZone = TimeZone(identifier: "en_ZA")
        
        let time = timeFormatter.string(from: date)
        let today = dateFormatter.string(from: date)
        
        composer.exportHTMLContentToPDF(HTMLContent: htmlContent) // converst html string to pdf and saves it
        
        //reduce the if lets and add the image ref
        
        guard let orderPdf = try? NSData(contentsOfFile: composer.pdfFileName) as Data else {
            return
        }
        
        guard let csvData = try? NSData(contentsOf: csvComposer.exportCSV(csvContent) as URL) as Data else {
            return
        }
        
        pdfRef.putData(orderPdf, metadata: nil) { (pdfMeta, error) in
            if error != nil {
                print("PDF SAVE Error: \(String(describing: error))")
                return
            }
            
            csvRef.putData(csvData, metadata: nil, completion: { (csvMeta, error) in
                if error != nil {
                    print("CSV SAVE Error: \(String(describing: error))")
                    return
                }
                
                imgRef.putData(image as Data, metadata: nil, completion: { (imgMeta, error) in
                    if error != nil {
                        print("image SAVE Error: \(String(describing: error))")
                        return
                    }
                    
                    pdfRef.downloadURL(completion: { (pdfUrl, pdfError) in
                        csvRef.downloadURL(completion: { (csvUrl, csvError) in
                            imgRef.downloadURL(completion: { (imgUrl, imgError) in
                                
                                if pdfError == nil && csvError == nil && imgError == nil {
                                    guard let pdfPath = pdfUrl?.absoluteString else {
                                        return
                                    }
                                    
                                    guard let csvPath = csvUrl?.absoluteString else {
                                        return
                                    }
                                    
                                    guard let imgPath = imgUrl?.absoluteString else {
                                        return
                                    }
                                    
//                                    self.saveOrder(withTotal: self.grossTotal, time: time, date: today, isSent: isSent, neolifeID: neolifeId, csv: csvPath, pdf: pdfPath, image: imgPath)
                                    SaveOrder.saveOrder(withTotal: self.grossTotal, time: time, date: today, isSent: isSent, neolifeID: neolifeId, csv: csvPath, pdf: pdfPath, image: imgPath)
                                    
                                } else {
                                    print("pdf error: \(pdfError.debugDescription)\ncsv error: \(csvError.debugDescription)\nimg error: \(imgError.debugDescription)")
                                }
                                
                            })
                        })
                    })
                })
            })
        }
    }
}






// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
