//
//  LoginVC.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/07/02.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//

import UIKit
import Firebase

class LoginVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var registerBtn: UIButton!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var passwordTxtField: UITextField!
    @IBOutlet weak var promptLbl: UILabel!
    var distributorID = ""
    var distributorName = ""
    var emailString = ""
    

    override func viewDidLoad() {
        super.viewDidLoad()

        passwordTxtField.delegate = self
        
        emailTxtField.text = emailString
        promptLbl.text = "Hello \(distributorName), You have registered with the app before, enter your password to login. \n\nYour username is: \(emailString)"
        
        //add to view extension
        let tap : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(LoginVC.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        passwordTxtField.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        loginUser()
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "startAppSegue" {
            let tabController = segue.destination as! UITabBarController
            let navController = tabController.viewControllers?[0] as! UINavigationController
            let homeVC = navController.topViewController as! HomeCollectionViewController
            
            homeVC.isFirstTime = true
        }
    }
    
    @IBAction func loginBtn(_ sender: UIButton) {
        loginUser()
    }
    
    func loginUser() {
        Auth.auth().signIn(withEmail: emailTxtField.text!, password: passwordTxtField.text!) { (user, error) in
            
            if error == nil {
                print("SUCCESS")
                self.performSegue(withIdentifier: "startAppSegue", sender: nil)
            } else {
                Alerts.alert(on: self, title: "Password Incorrect", and: error?.localizedDescription)
            }
        }
    }

    
    @IBAction func forgotPassword(_ sender: UIButton) {
        let ac = UIAlertController(title: "You want to reset password?", message: "please enter your email", preferredStyle: .alert)
        
        ac.addTextField { (textfield: UITextField) in
            textfield.placeholder = "Email"
        }
        
        let reset = UIAlertAction(title: "Reset", style: .default) { (action) in
            let emailField = ac.textFields?.first?.text
            
            if let userMail = emailField {
                if userMail != "" {
                    //reset password via email
                    Auth.auth().sendPasswordReset(withEmail: userMail, completion: { (error) in
                        if error != nil {
                            Alerts.alert(on: self, title: "Error Resetting Password", and: error?.localizedDescription)
                        } else {
                            Alerts.alert(on: self, title: "Link Sent", and: "Reset password link has been sent to your email, please check your inbox.")
                        }
                    })
                } else {
                    Alerts.alert(on: self, title: "Email Field Empty", and: "Please enter a valid email.")
                }
            }
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        
        ac.addAction(reset)
        ac.addAction(cancel)
        
        self.present(ac, animated: true, completion: nil)
    }
    
}
