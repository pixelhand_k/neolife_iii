//
//  Distributors.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/05/08.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//

import UIKit
import Parse
import Firebase

class Distributors: PFObject {
    
    static func parseClassName() -> String {
        return "distributors"
    }
    
    override class func query() -> PFQuery<PFObject>? {
        let query = PFQuery(className: Distributors.parseClassName())
        //        query.order(byDescending: "createdAt")
        return query
    }
    
    func isDistributor(withId id: Int) {
        let query = Distributors.query()
        query?.whereKey("id_number", equalTo: id)
        
        query?.findObjectsInBackground(block: { (objects, error) in
            
            if error == nil {
                
                print("Distributor: \(String(describing: objects))")
//                return objects
                /*if let object = objects {
                    for objects in object {
                        let name = objects["name"] as? String
                    }
                }*/
            } else {
                print("ERROR: \(String(describing: error?.localizedDescription))")
            }
        })
    }
    
//    func isUserDistributer() -> Bool {
//        let user = PFUser.current()
//
//        if user?["is_distributor"] as? Bool == true {
//            return true
//        } else {
//            return false
//        }
//    }
    

    func isUserMember() -> Bool {
        let user = PFUser.current()
        
        if user?["is_distributor"] as? Bool == false {
            return true
        } else {
            return false
        }
    }
    
    func allDistributors() {
        let query = PFQuery(className: Distributors.parseClassName())
        
        query.findObjectsInBackground { (object: [PFObject]?, error) -> Void in
            if error == nil {
//                print("All Distributors: \(object!)")
                print("Found All Distributors")
            } else {
                print("Error: \(String(describing: error)), \(String(describing: error?.localizedDescription))")
            }
        }
    }
    
//    do this if the new list isnt loaded
//    func refreshDistributorList() {
//        //query details of cached data from local data store
//        let query = PFQuery(className: Distributors.parseClassName())
//
//        //unpin cached data
//
////        query details of new data
//
////        pin new data
//
//    }
    
    
    func findDistributor(withID id: String) {
        let query = PFQuery(className: "distributors")
        query.whereKey("id_number", equalTo: String.toInt(string: id))
        
        query.findObjectsInBackground { (objects, error) in
            if error == nil {
                print("Found DISTRIB: \(String(describing: objects))")
                
                for object in objects! {
                    let name = object["name"] as! String
//                    self.username = name
                    print("Distributor \(name) Found")
                }
                
            } else {
                print("Error: \(String(describing: error?.localizedDescription))")
            }
        }
    }
    
    func isUserRegistered(withID id: String) {
        let query = PFUser.query()
        query?.whereKey("id_number", equalTo: id)
        
        query?.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                print("USER: \(String(describing: objects))")
                /*if let object = objects {
                    for objects in object {
//                        let name = objects["Name"] as! String
//                        let email = objects["username"] as! String
//                        print("IS Registered as: \(email)")
                    }
                }*/
            } else {
                print("Error: \(String(describing: error)), \(String(describing: error?.localizedDescription))")
                
            }
        })
        
    }
}
