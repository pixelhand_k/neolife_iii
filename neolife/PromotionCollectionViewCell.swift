//
//  PromotionCollectionViewCell.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/02/24.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//

import UIKit

class PromotionCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var promoImageView: UIImageView!
    @IBOutlet weak var visualEffectView: UIVisualEffectView!
    
    @IBOutlet weak var titleLbl: UILabel!
    
}
