//
//  UserFunctions.swift
//  neolife
//
//  Created by Karabo Moloi on 2018/06/24.
//  Copyright © 2018 Karabo Moloi. All rights reserved.
//

import UIKit


class UserFunctions {
    static func getNeolifeId() -> String {
        let userDefaults = UserDefaults.standard
        let id = userDefaults.object(forKey: "neolifeId") as? String ?? String()
        
        return id
    }
    
    static func saveIdToUserDefaultsWith(id: String) {
        let standardUserDefaults = UserDefaults.standard
        let currentId = id
        let previousId = standardUserDefaults.string(forKey: "neolifeId")
        
        if previousId != currentId {
            standardUserDefaults.set(currentId, forKey: "neolifeId")
        } else if previousId == nil {
            standardUserDefaults.set(currentId, forKey: "neolifeId")
        }
        standardUserDefaults.synchronize()
    }
}
