//
//  Category.swift
//  neolife
//
//  Created by Karabo Moloi on 2018/10/30.
//  Copyright © 2018 Karabo Moloi. All rights reserved.
//

import UIKit
import Firebase

/// Class that calls the various nodes that are on the server via the parameter and can store it in an array
class CallFirebaseNode {
    let ref = Database.database().reference(fromURL: "https://neolife-d9754.firebaseio.com/")
    var path : String
    var node : [AnyObject]
    
    init(_ name: String, storedIn array: [AnyObject]?) {
        path = name
        node = array ?? [AnyObject]()
    }
    
    func getClass() {
        ref.child(path).observe(.childAdded, with: { (snap) in
            if let dict = snap.value as? [String : AnyObject] {
//                print("\(self.path) loaded")
            }
        }, withCancel: nil)
    }
    
    func productCatalog(in view: UICollectionViewController) {
        node.removeAll()
        ref.child(path).observe(.childAdded, with: { (snap) in
            if let dict = snap.value as? [String : AnyObject] {
                let category = Category()
                
                category.catalog_identifier = dict["catalog_identifier"] as? String
                category.catalog_image = dict["catalog_image"] as? String
                category.catalog_name = dict["catalog_name"] as? String
                
                self.node.append(category)

                view.collectionView?.reloadData()
            }
            
        }, withCancel: nil)
    }

}
