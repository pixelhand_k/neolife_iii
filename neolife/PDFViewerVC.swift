//
//  PDFViewerVC.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/06/07.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//

import UIKit
//import Parse

class PDFViewerVC: UIViewController, UIWebViewDelegate {
    
    var pdfContent = ""
//    var prodFile: PFFile?
    var pdfUrl : String?
    var prodTitle: String!
    
    @IBOutlet weak var pdfWebView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.hidesBarsOnSwipe = true
        pdfWebView.scalesPageToFit = true
        
        pdfWebView.delegate = self
        
        if pdfContent != "" {
            let path = NSURL(fileURLWithPath: Bundle.main.path(forResource: pdfContent, ofType: "pdf")!)
            let request = NSURLRequest(url: path as URL)
            pdfWebView.loadRequest(request as URLRequest)
        } else {
//            print("pdf from detail view \(pdfUrl)")
            if let path = pdfUrl {
                let url = URL(string: path)
                if let label = url {
                    pdfWebView.loadRequest(URLRequest(url: label))
                }
            }
        }
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        if pdfContent != "" {
            self.pdfWebView.scrollView.minimumZoomScale = 1.0
            self.pdfWebView.scrollView.maximumZoomScale = 1.5
        }
        
    }
    
    func dataToPdf(_ data: NSData) /*-> NSURL */{
        // loacation where the file is saved
        let pdfFileName = "\(AppDelegate.getAppDelegate().getDocDir())/\(prodTitle)_label.pdf"
        data.write(toFile: pdfFileName, atomically: true)
        print("LABEL Location: \(pdfFileName)")
        
        //loaded to view
        viewPdf(pdfFileName)
    }

    func viewPdf(_ path: String) {
        let url = URL(fileURLWithPath: path)
        let request = URLRequest(url: url)
        pdfWebView.loadRequest(request)
    }


}
