//
//  Category.swift
//  neolife
//
//  Created by Karabo Moloi on 2018/10/31.
//  Copyright © 2018 Karabo Moloi. All rights reserved.
//

import UIKit

class Category: NSObject {
    var catalog_identifier : String!
    var catalog_image : String!
    var catalog_name : String!
    var order : Int!
}
