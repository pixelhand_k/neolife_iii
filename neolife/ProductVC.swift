//
//  ProductVC.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/01/20.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//

import UIKit
import Parse
import Firebase

@IBDesignable

class ProductVC: UIViewController, UIPopoverPresentationControllerDelegate, UITableViewDataSource,UITableViewDelegate, UIGestureRecognizerDelegate {

//    MARK: - Properties
    @IBOutlet weak var productTable: UITableView!
    @IBOutlet var dismissPanGesture: UIPanGestureRecognizer!
    var isTrackingPanLocation = false
    var categoryName = String()
    var categoryId = String()
//    var productsData = [PFObject?]()
    var products = [Product]()
    var api = APIService()
    var isFromHome = false
    let cartBadge = CartBadge()
    let ref = Database.database().reference(fromURL: "https://neolife-d9754.firebaseio.com/")
    let childRef = "product_details"
//    for indexing a-z
    var prodDictionary = [String : [String]]()
    var prodSectionTitles = [String]()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        
        self.title = categoryName
        if categoryId == "" {
//            getAllProducts()
            getProducts()
        } else {
//            getProductsByIdentifier()
            getProductsById()
            print("ID: ", categoryId)
        }
        
        cartBadge.controller = self
        cartBadge.countString = "\(cartBadge.cartCount())"
        cartBadge.segueId = "prodToCartSegue"
        cartBadge.addBadgeToBtn()
    }

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        for prod in productsData {
//            let keys = String(describing: prod?.allKeys)
//            print("KEYS: \(keys)")
//        }
        
        productTable.delegate = self
        productTable.dataSource = self
        
//        productTable.scrollToRow(at: selectedIndex, at: .middle, animated: true)
//        self.productTable.addSubview(refreshControl)
        
//        showPrompt()
        
//        dismissPanGesture = UIPanGestureRecognizer(target: self, action: #selector(ProductVC.dismissRecogniser(recogniser:)))
//        dismissPanGesture.delegate = self
//        productTable.addGestureRecognizer(dismissPanGesture)
        
    }
    
    
//    lazy var refreshControl: UIRefreshControl = {
//        let refreshControl = UIRefreshControl()
//        refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControlEvents.valueChanged)
//        return refreshControl
//    }()
    
    /*
    func refresh(refreshControl: UIRefreshControl) {
        refreshControl.beginRefreshing()
        
        let timer = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: timer) { 
            if self.categoryId == "" {
                self.getNewProducts()
            } else {
                self.getNewProductsWithID()
            }
            self.productTable?.reloadData()
            self.navigationItem.prompt = nil
            
            refreshControl.endRefreshing()
        }
    }*/

    
    /*func showPrompt() {
        self.navigationItem.prompt = "Pull to Refresh"
    }*/
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

//    MARK: - Gesture Methods
    
    func dismissRecogniser(recogniser: UIPanGestureRecognizer) {
        if recogniser.state == .began && productTable.contentOffset.y == 0 {
            recogniser.setTranslation(CGPoint.zero, in: productTable)
            isTrackingPanLocation = true
        } else if recogniser.state == .recognized {
            let panOffset = recogniser.translation(in: productTable)
            
            let panRangeToDismiss = panOffset.y > 400
            
            if panRangeToDismiss {
                recogniser.isEnabled = true
                
                if isFromHome == false {
                    self.dismiss(animated: true, completion: nil)
                } else {
                    self.navigationController?.popViewController(animated: true)
                }
            }
            
            if panOffset.y < 0 {
                isTrackingPanLocation = false
            }
        } else {
            isTrackingPanLocation = false
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        

        if segue.identifier == "detailSegue" {
            let navController = segue.destination as! UINavigationController
            let detailTVC: MoreDetailTVC = navController.topViewController as! MoreDetailTVC
//            getting selected indexpath
            
            navController.transitioningDelegate = self

            if let indexPath = productTable.indexPath(for: sender as! UITableViewCell){
                
                var productsList = [Product]()
                if categoryId == "" {
                    productsList = products
                } else {
                    productsList = products.sorted(by: {$0.name < $1.name}) // sorts array by name
                }
                detailTVC.productTitle = productsList[indexPath.row].name
                detailTVC.productCode = String.fromInt(integer: productsList[indexPath.row].code)
                detailTVC.selectedIndex = indexPath.row
//                if let name = productsData[indexPath.row]?["product_name"] as? String {
//                    detailTVC.productTitle = name
//                }
//
//                //send code here
//                if let code = productsData[indexPath.row]?["code"] as? String {
//                    detailTVC.productCode = code
//                }
            }
        }
        
    }

//    MARK: - Table Data Source
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return self.prodSectionTitles.count
//    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return productsData.count

        return products.count
//        let keys = self.prodSectionTitles[section]
//        if let values = prodDictionary[keys] {
//            return values.count
//        }
//
//        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ProductCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ProductCell
        
//        var prodName = ""
        var productsList = [Product]()
        if categoryId == "" {
            productsList = products
        } else {
            productsList = products.sorted(by: {$0.name < $1.name}) // sorts array by name
        }
        
        
        cell.uspLbl.numberOfLines = 0
        cell.uspLbl.font = UIFont.helveticaNeueContentDetail()
        cell.uspLbl.sizeToFit()
        cell.productNameLbl.numberOfLines = 0
        cell.productNameLbl.sizeToFit()
        cell.productNameLbl.font = UIFont.helveticaNeueContentTitle()
        
        cell.productNameLbl.text = productsList[indexPath.row].name
        
        
//        if let name = productsData[indexPath.row]?["product_name"] as? String {
//            //                gets rid of whitespace and line breaks
//            prodName = name.trimmingCharacters(in: .whitespacesAndNewlines)
//        }

//        cell.productNameLbl.text = prodName
        
        //indexing
//        let key = prodSectionTitles[indexPath.section]
//        if let values = prodDictionary[key] {
//            cell.productNameLbl.text = values[indexPath.row]
//        }
        
        
//        if let usp = productsData[indexPath.row]?["unique_selling_properties"] as? String {
//            cell.uspLbl.text = usp
//        }
        
        cell.uspLbl.text = productsList[indexPath.row].usp
        
        let image = productsList[indexPath.row].imageUrl
        cell.productImage.loadImageFromCacheWithUrl(image!)
        
//        if let image = productsData[indexPath.row]?["image_url"] as? String {
//            //                loadImages(y, imageView: cell.subMenuImage)
////            api.loadImages(image, imageView: cell.productImage)
//            cell.productImage.loadImageFromCacheWithUrl(image)
//        } else {
//            cell.productImage.image = #imageLiteral(resourceName: "carotenoid")
//        }
        
        
            
//            cell.productNameLbl.text = packs[indexPath.row].name
//            cell.detailLbl.text = packs[indexPath.row].detail
//            cell.rdpValueLbl.text = "R " + toString(price: packs[indexPath.row].distributerCasePrice)
//            cell.rrpValueLbl.text = "R " + toString(price: packs[indexPath.row].retailCasePrice)
        /* else if selectedSegment == 1 {
            cell.productImage.image = individual[indexPath.row].productImage
            cell.productNameLbl.text = individual[indexPath.row].name
            cell.detailLbl.text = individual[indexPath.row].detail
            cell.rdpValueLbl.text = individual[indexPath.row].rdp
            cell.rrpValueLbl.text = individual[indexPath.row].rrp
        } else if selectedSegment == 2 {
            cell.productImage.image = accesories[indexPath.row].productImage
            cell.productNameLbl.text = accesories[indexPath.row].name
            cell.detailLbl.text = accesories[indexPath.row].detail
            cell.rdpValueLbl.text = accesories[indexPath.row].rdp
            cell.rrpValueLbl.text = accesories[indexPath.row].rrp
        }*/
        
//        cell.addProductBtn.layer.borderColor = UIColor(colorLiteralRed: 153/255, green: 153/255, blue: 153/255, alpha: 1.0).cgColor
        
        
        
        return cell
    }
    
//    MARK: - Table Delgate
    
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return prodSectionTitles[section]
//    }
//    
//    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
//        return prodSectionTitles
//    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        productTable.deselectRow(at: indexPath, animated: true)
    }
    
    
    
    @IBAction func closeAction(_ sender: UIBarButtonItem) {
        
        if isFromHome == false {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    //MARK: - Teble A-Z Index generator
    
    /*
    func extractFirstLetterIndex() {
        for prod in self.productsData {
            let obj = prod!["product_name"] as! String
            //                            print("NAME: \(String(describing: obj))")
            let keys = String(describing: obj.prefix(1))
            
            if var products = self.prodDictionary[keys] {
                products.append(obj)
                self.prodDictionary[keys] = products
            } else {
                self.prodDictionary[keys] = [obj]
            }
        }
        
        self.prodSectionTitles = [String](self.prodDictionary.keys)
        self.prodSectionTitles = self.prodSectionTitles.sorted()
    }
    */
    
//    MARK: - Backend Function
    func getProducts() {
        self.products.removeAll()
        ref.child(childRef).queryOrdered(byChild: "product_name").observe(.childAdded, with: { (snap) in
            if let dict = snap.value as? [String : AnyObject] {
                let product = Product()
                product.name = dict["product_name"] as? String
                product.usp = dict["unique_selling_properties"] as? String
                product.imageUrl = dict["image_url"] as? String
                product.code = dict["code"] as? Int
                
                DispatchQueue.main.async {
                    self.products.append(product)
                }
                
                DispatchQueue.main.async {
                    self.productTable.reloadData()
                }
            }
        }, withCancel: nil)
    }
    
    func getProductsById() {
        self.products.removeAll()
        ref.child(childRef).queryOrdered(byChild: categoryId).queryEqual(toValue: "yes").observe(.childAdded, with: { (snap) in
            if let dict = snap.value as? [String : AnyObject] {
                let product = Product()
                product.name = dict["product_name"] as? String
                product.usp = dict["unique_selling_properties"] as? String
                product.imageUrl = dict["image_url"] as? String
                product.code = dict["code"] as? Int
                
                DispatchQueue.main.async {
                    self.products.append(product)
                }
                
                DispatchQueue.main.async {
                    self.productTable.reloadData()
                }
            }
        }, withCancel: nil)
    }
    
    /*
    func getProductsByIdentifier(){
        let query = PFQuery(className: "product_details")
        query.whereKey(categoryId, equalTo: "yes")
        query.addAscendingOrder("product_name")
        query.fromLocalDatastore()
        query.findObjectsInBackground { (object: [PFObject]?, error) -> Void in
            if error == nil && object?.count != 0 {
                
//                print("Successfully found: \(object) items")
                
                DispatchQueue.global(qos: .userInitiated).async {
                
                    if let objects = object {
                        //                    for obj in objects {
                        //                        let code = obj["code"] as? String
                        //                        print("Code: \(code!)")
                        //                    }
                        DispatchQueue.main.async {
                            self.productsData = Array(objects.makeIterator())
                        }
                    }
                    
                    DispatchQueue.main.async {
//                        self.extractFirstLetterIndex()
                        self.productTable.reloadData()
                    }
                }
            } else if error == nil && object?.count == 0 {
                let alertController = UIAlertController(title: "No products", message: "There's are currently no products but will update you on availability", preferredStyle: .alert)
                
                let ok = UIAlertAction(title: "Ok, Take me back", style: .default, handler: { (action) in
                    self.dismiss(animated: true, completion: nil)
                })
                
                alertController.addAction(ok)
                
                self.present(alertController, animated: true, completion: nil)
                
            } else {
//                print("Error: \(error), \(error?.localizedDescription)")
                print("LOCALIZED DESCR: \(String(describing: error?.localizedDescription))")
                
                Alerts.alert(on: self, title: "", and: (error?.localizedDescription)!)
                
//                if error?.localizedDescription == "The Internet connection appears to be offline." {
//                    let alertController = UIAlertController(title: "No Internet Connection", message: "Please connect to internet and try again", preferredStyle: .alert)
//                    
//                    let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) in
//                        self.dismiss(animated: true, completion: nil)
//                    })
//                    
//                    alertController.addAction(ok)
//                    
//                    self.present(alertController, animated: true, completion: nil)
//                    
//                }
            }
        }
    }
    */
    
    /*
    func getAllProducts(){
        let query = PFQuery(className: "product_details")
        query.addAscendingOrder("product_name")
        query.fromLocalDatastore()
        
        query.findObjectsInBackground { (objects, error) in
            if error == nil {
                DispatchQueue.global(qos: .userInitiated).async {
                    if let object = objects {
//                        print("PRODUCTS: \(String(describing: objects))\n")
                        self.productsData = Array(object.makeIterator())
                    }
                    
                    DispatchQueue.main.async {
//                        self.extractFirstLetterIndex()
                        self.productTable.reloadData()
                    }
                }
            } else {
                Alerts.alert(on: self, title: "", and: (error?.localizedDescription)!)
            }
        }
    }
    */
    
//    func toString(price: Int?) -> String {
//        var priceString = ""
//        
//        if let val = price {
//            priceString = "\(val)"
//        }
//        
//        return priceString
//    }
    
    
}

extension ProductVC {
    /*func getNewProductsWithID() {
        let query = PFQuery(className: "product_details")
        query.addAscendingOrder("product_name")
        query.whereKey(categoryId, equalTo: "yes")
        query.fromLocalDatastore()
        
        //unpinning old content from the local datastore
        query.findObjectsInBackground { (objects, error) in
            if error == nil {
                
                PFObject.unpinAll(inBackground: objects)
            }
        }
        
        let nQuery = PFQuery(className: "product_details")
        nQuery.addAscendingOrder("product_name")
        nQuery.whereKey(categoryId, equalTo: "yes")
        
        nQuery.findObjectsInBackground { (objects, error) in
            if error == nil {
                self.productsData.removeAll()
                PFObject.pinAll(inBackground: objects)
                
//                print("Num OBJECTS: \(objects?.count)")
                
                if let objects = objects {
                    DispatchQueue.main.async {
                        self.productsData = Array(objects.makeIterator())
                    }
                }
                
                DispatchQueue.main.async {
                    self.productTable.reloadData()
                }
                
                print("Loaded new content")
            } else {
                print("Error: \(String(describing: error)), \(String(describing: error?.localizedDescription))")
            }
        }
        
        self.productTable.reloadData()
    }*/
    
    /*func getNewProducts() {
        let query = PFQuery(className: "product_details")
        query.addAscendingOrder("product_name")
        query.fromLocalDatastore()
        
        self.productsData.removeAll()
        
        //unpinning old content from the local datastore
        query.findObjectsInBackground { (objects, error) in
            if error == nil {
                PFObject.unpinAll(inBackground: objects)
                
            }
        }
        
        let nQuery = PFQuery(className: "product_details")
        nQuery.addAscendingOrder("product_name")
        
        nQuery.findObjectsInBackground { (objects, error) in
            if error == nil {
                
                PFObject.pinAll(inBackground: objects)
                
//                print("OBJECTS: \(String(describing: objects))\n")
                if let objects = objects {
                    DispatchQueue.main.async {
                        self.productsData = Array(objects.makeIterator())
                    }
                }
                
                DispatchQueue.main.async {
                    self.productTable.reloadData()
                }

                print("Loaded new content")
            } else {
                print("Error: \(String(describing: error)), \(String(describing: error?.localizedDescription))")
            }
        }
        
        self.productTable.reloadData()
    }*/
}

extension ProductVC: UIViewControllerTransitioningDelegate {
//    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        return DissmissAnimator()
//    }
}

