//
//  PrintRenderer.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/07/05.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//

import UIKit

class PrintRenderer: UIPrintPageRenderer {
    
    let a4Width : CGFloat = 595.2
    let a4Height : CGFloat = 841.8
    
    override init() {
        super.init()
        
        // Specify the frame of the A4 page.
        let pageFrame = CGRect(x: 0.0, y: 0.0, width: a4Width, height: a4Height)
        
        // Set the page frame. READ ONLY
        self.setValue(NSValue(cgRect: pageFrame), forKey: "paperRect")
        
        
        // Set the horizontal and vertical insets (that's optional). READ ONLY
        //        self.setValue(NSValue(cgRect: pageFrame), forKey: "printableRect")
        //        The above adds an offset of 10 points to both horizontal and vertical axis. Note that the configuration made in this part should be done even if you don’t subclass the UIPrintPageRenderer. In other words, you should never forget to set the paper and printable area of your printing object.
        self.setValue(NSValue(cgRect: pageFrame.insetBy(dx: 10.0, dy: 10.0)), forKey: "printableRect")

    }

}
