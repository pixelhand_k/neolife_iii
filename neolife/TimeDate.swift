//
//  TimeDate.swift
//  neolife
//
//  Created by Karabo Moloi on 2018/09/06.
//  Copyright © 2018 Karabo Moloi. All rights reserved.
//

import UIKit

class TimeDate {
    let date = Date()
    let dateFormatter = DateFormatter()
    let timeFormatter = DateFormatter()
    
    init() {
        dateFormatter.dateFormat = "yyyy/MM/dd"
        dateFormatter.timeZone = TimeZone(identifier: "en_ZA")
        dateFormatter.locale = Locale(identifier: "en_ZA")
        timeFormatter.timeStyle = .short
        timeFormatter.locale = Locale(identifier: "en_ZA")
        timeFormatter.timeZone = TimeZone(identifier: "en_ZA")
    }
    
    func getTime() -> String {
        return timeFormatter.string(from: date)
    }
    
    func getDate() -> String {
        return dateFormatter.string(from: date)
    }

}
