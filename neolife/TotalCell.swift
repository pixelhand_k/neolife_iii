//
//  TotalCell.swift
//  neolife
//
//  Created by Karabo Moloi on 2017/06/19.
//  Copyright © 2017 Karabo Moloi. All rights reserved.
//

import UIKit

class TotalCell: UITableViewCell {
    
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var numberOfItemsLbl: UILabel!
    @IBOutlet weak var freightCostLbl: UILabel!
    @IBOutlet weak var pickSwitch: UISwitch!
    
}
