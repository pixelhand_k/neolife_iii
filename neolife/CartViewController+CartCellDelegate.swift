//
//  CartViewController+CartCellDelegate.swift
//  neolife
//
//  Created by Karabo Moloi on 2018/02/13.
//  Copyright © 2018 Karabo Moloi. All rights reserved.
//

import UIKit
import CoreData

extension CartViewController: CartCellDelegate {
    
    func recalculateItemTotal(_ numItems: Int, _ price: Int) -> String {
        let newTotal = numItems * price
        
        return String.fromInt(integer: newTotal)
    }
    
    func recalculateTotal(_ currentTotal: Int, _ itemPrice: Int) -> String {
        let newTotal = currentTotal + itemPrice
        
        return String.fromInt(integer: newTotal)
    }
    
    func didAdd(sender: UIButton) {
        var indexPath = NSIndexPath()
        let totalIndexPath = IndexPath(row: 0, section: 0)
        
        if let cell = sender.superview?.superview as? CartCell {
            indexPath = cartTableView.indexPath(for: cell)! as NSIndexPath
        }
        
        print("CELL INDEX PATH: \(indexPath.row)")
        
        let cell = cartTableView.cellForRow(at: indexPath as IndexPath) as! CartCell
        let tCell = totalTableView.cellForRow(at: totalIndexPath) as! TotalCell
        
        //total cell
        let totalNumString = tCell.numberOfItemsLbl.text
        let totalNumNoWords = totalNumString?.replacingOccurrences(of: "Items in cart: ", with: "")
        var totalNumItems = String.toInt(string: totalNumNoWords!)
        let totalPrice = String.fromCurrency(tCell.totalLbl.text!)
        totalNumItems = totalNumItems + 1
        tCell.numberOfItemsLbl.text = "Items in cart: " + String.fromInt(integer: totalNumItems)
        
        //item cell
        var numItems = String.toInt(string: cell.numberOfItems.text!)
        let price = String.toInt(string: cell.unitPriceLbl.text!)
        numItems = numItems + 1
        
        let newTotal = recalculateTotal(totalPrice, price)
        print("NEW TOTAL: \(newTotal)")
        
        let formatedTotalPrice = AppDelegate().getStringValueFormattedAsCurrency(newTotal)
        tCell.totalLbl.text = formatedTotalPrice
        
        cell.numberOfItems.text = String.fromInt(integer: numItems)
        let newVal = recalculateItemTotal(numItems, price)
        cell.prodPriceLbl.text = AppDelegate().getStringValueFormattedAsCurrency(newVal)
        
        updateQty(at: indexPath, newQty: String.fromInt(integer: numItems), newTotal: newVal)
        
        let pickSwitch = tCell.pickSwitch
        
        if String.toInt(string: newTotal) >= 1000 && !(pickSwitch?.isOn)! {
            print("pickswitch off")
            tCell.freightCostLbl.text = deliveryCalc(with: initGrossPrice())
        } else if String.toInt(string: newTotal) >= 1000 && (pickSwitch?.isOn)!{
            tCell.freightCostLbl.text = AppDelegate().getStringValueFormattedAsCurrency("0")
        }
    }
    
    func didSubtract(sender: UIButton) {
        var indexPath = NSIndexPath()
        let totalIndexPath = IndexPath(row: 0, section: 0)
        
        if let cellPath = sender.superview?.superview as? CartCell {
            indexPath = cartTableView.indexPath(for: cellPath)! as NSIndexPath
        }
        
        let cell = cartTableView.cellForRow(at: indexPath as IndexPath) as! CartCell
        let tCell = totalTableView.cellForRow(at: totalIndexPath) as! TotalCell
        
        //total cell
        let totalNumString = tCell.numberOfItemsLbl.text
        let totalNumNoWords = totalNumString?.replacingOccurrences(of: "Items in cart: ", with: "")
        var totalNumItems = String.toInt(string: totalNumNoWords!)
        var totalPrice = String.fromCurrency(tCell.totalLbl.text!)
        
        //item cell
        var numItems = String.toInt(string: cell.numberOfItems.text!)
        let price = String.toInt(string: cell.unitPriceLbl.text!)
        
        if numItems > 1 {
            numItems = numItems - 1
            totalNumItems = totalNumItems - 1
            totalPrice = totalPrice - price
        } else {
            numItems = 1
        }
        
        print("TOTAL Price sub: \(totalPrice)")
        
        
        let formatedTotalPrice = AppDelegate().getStringValueFormattedAsCurrency(String.fromInt(integer: totalPrice))
        
        tCell.totalLbl.text = formatedTotalPrice
        tCell.numberOfItemsLbl.text = "Items in cart: " + String.fromInt(integer: totalNumItems)
        
        cell.numberOfItems.text = String.fromInt(integer: numItems)
        let newVal = recalculateItemTotal(numItems, price)
        //        cell.prodPriceLbl.text = "R" + newVal
        cell.prodPriceLbl.text = AppDelegate().getStringValueFormattedAsCurrency(newVal)
        updateQty(at: indexPath, newQty: String.fromInt(integer: numItems), newTotal: newVal)
        
        let pickSwitch = tCell.pickSwitch
        
        if totalPrice <= 1000 && !(pickSwitch?.isOn)! {
            tCell.freightCostLbl.text = deliveryCalc(with: initGrossPrice())
        } else if totalPrice <= 1000 && (pickSwitch?.isOn)!{
            //            tCell.freightCostLbl.text = deliveryCalc(with: initGrossPrice())
            tCell.freightCostLbl.text = AppDelegate().getStringValueFormattedAsCurrency("0")
        }
    }
    
    func updateQty(at indexPath: NSIndexPath, newQty qty: String, newTotal total: String){
        
        let item = cart[indexPath.row]
        
        item.setValue(qty, forKey: "numItems")
        item.setValue(total, forKey: "total")
        
        do {
            try item.managedObjectContext?.save()
            print("ITEM UPDATED")
        } catch let error as NSError {
            print("Couldn't update record: \(error.localizedDescription)")
        }
    }
    
    func saveNewQty(at name: String, with num: String, _ total: String){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let prodFetch : NSFetchRequest<ShoppingBag> = ShoppingBag.fetchRequest()
        
        //        prodFetch.predicate = NSPredicate(format: "name == %@", name)
        /*let entity = NSEntityDescription.entity(forEntityName: "ShoppingBag", in: managedContext)
         
         let shoppingBag = ShoppingBag(entity: entity!, insertInto: managedContext)
         
         shoppingBag.numItems = num*/
        
        
        
        do {
            let results = try managedContext.fetch(prodFetch)
            let prod = results[0]
            
            prod.setValue(num, forKey: "numItems")
            prod.setValue(total, forKey: "total")
            
            
            do {
                try managedContext.save()
                self.cartTableView.reloadData()
                self.totalTableView.reloadData()
            } catch let error as NSError {
                print("Error: \(error), \(error.localizedDescription)")
            }
            
        } catch let error as NSError {
            print("Error for fetch req: \(error)")
        }
        
    }
}

