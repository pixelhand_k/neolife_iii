//
//  HistoryTVC.swift
//  neolife
//
//  Created by Karabo Moloi on 2018/09/01.
//  Copyright © 2018 Karabo Moloi. All rights reserved.
//

import UIKit
import Firebase
import MessageUI
import Crashlytics

class HistoryTVC: UITableViewController, UIPopoverPresentationControllerDelegate, MFMailComposeViewControllerDelegate, UIImagePickerControllerDelegate {

    var orders = [Order]()
    var selectedCellIndexPath = IndexPath()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if Auth.auth().currentUser != nil {
            loadOrders()
        } else {
            Alerts.alert(on: self, title: "Users only", and: "Only users who have registered with the application can view their order history.")
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "History"
        
    }
    
//    MARK: - Bar Button
    
    @IBAction func helpButtonAction(_ sender: UIBarButtonItem) {
        let message = "This is where your order history is placed. When orders are not complete and being processed, orders show up here with an ❌. Please contact us at the support email. Orders that are sent are indicated by a ✅. Those can be reviewed."
        
        helpInfoWith(message, button: sender)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HistoryCell
        let myOrder = orders[indexPath.row]
        
//        add all these into the history cell file for cleaner code
        let colourimg = cell.docImg.image?.withRenderingMode(.alwaysTemplate)
        cell.docImg.image = colourimg
        cell.docImg.tintColor = UIColor.greenNeolife()
        
        cell.tintColor = UIColor.greenNeolife()
        
        if let userOrderNum = myOrder.orderNum {
            if myOrder.isSent != true {
                cell.titleLbl.text = "Order No: " + userOrderNum + " ❌"
            } else {
                cell.titleLbl.text = "Order No: " + userOrderNum + " ✅"
            }
            
        } else {
            CLSLogv("orderNumError: %@", getVaList(["Order failed to load due to nil order number"]))
        }
        
        if let orderDate = myOrder.date {
            if let orderTime = myOrder.time {
                cell.dateLbl.text = orderDate + " " + orderTime
            }
        } else {
            cell.titleLbl.text = "Unavailable"
            cell.dateLbl.text = "Unavailable"
            cell.selectionStyle = .none
            CLSLogv("order warning: %@", getVaList(["Order date is nil"]))
        }

        

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let myOrder = orders[indexPath.row]
        
//        if order is sent preview the pdf
        let invoiceNum = myOrder.orderNum
        if let pdfUrl = myOrder.invoice {
            if let num = invoiceNum {
                if myOrder.isSent == true {
                    openInvoiceWith(invoice: pdfUrl, invoiceNum: num)
                } else {
                    Alerts.alert(on: self, title: "Incomplete order", and: "Please contact NeoLife")
                }
            }
        }
        
    }
    
    override func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        
        let myOrder = orders[indexPath.row]
        let cell = tableView.cellForRow(at: indexPath)

        if myOrder.isSent != true {
//            display info that it wasnt sent and they need to try again to send it by tapping cell
            displayInfo(with: "❌ indicates that your order has not been sent and is not being processed ye. Please contact NeoLife for assistance. Tap anywhere to dismiss", from: cell!)
        } else {
//            display info that it was sent they can tap cell to display order
            displayInfo(with: "✅ indicates that your order has been sent and is being processed. Press your order number to review your order. Tap anywhere to dismiss", from: cell!)
        }
    }

    func loadOrders() {
        let userId = UserFunctions.getNeolifeId()
        
        self.orders.removeAll()
        let ref = Database.database().reference()
        ref.child("orders").child(userId).observe(.childAdded, with: { (snap) in
            let order = Order()
            if let dict = snap.value as? [String : AnyObject] {
                order.invoice = dict["invoice"] as? String
                order.orderNum = dict["orderNum"] as? String
                order.isSent = dict["isSent"] as? Bool
                order.total = dict["total"] as? String
                order.date = dict["date"] as? String
                order.csv = dict["csv"] as? String
                order.time = dict["time"] as? String
                order.paymentType = dict["paymentType"] as? String
                
                DispatchQueue.main.async {
                    self.orders.append(order)
                }
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }, withCancel: nil)
    }
    
//    MARK: Invoice Management
    
    func openInvoiceWith(invoice url: String, invoiceNum: String) {
        guard let vc = UIStoryboard(name: "Product", bundle: nil).instantiateViewController(withIdentifier: "pdfViewer") as? PDFViewerVC else {
            print("didnt work")
            return
        }
        vc.title = "Order #" + invoiceNum
        vc.pdfContent = ""
        vc.pdfUrl = url
        vc.prodTitle = "order" + invoiceNum
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //    MARK: - Email Delegate
    func sendOrderWith(_ invoiceNum: String, _ invoice: String, _ csv: String, paymentType: String, andImage: UIImage){
        if MFMailComposeViewController.canSendMail() {
            print("pdfUrl: \(invoice), csv: \(csv)")
            let mailComposerVC = MFMailComposeViewController()
            mailComposerVC.setSubject("NeoApp" + "\(invoiceNum)")
            mailComposerVC.mailComposeDelegate = self
            mailComposerVC.setToRecipients(["ordera@za.neolife.com"])
            
            if paymentType == "Bank Deposit" {
                //atttach photo
                mailComposerVC.setMessageBody("Use your order number: \(invoiceNum) as refrerence to make your payment", isHTML: false)
                
                let slip = andImage.jpegData(compressionQuality: 0.5)! as NSData
                mailComposerVC.addAttachmentData(slip as Data, mimeType: "image/jpg", fileName: "slip.jpg")
                
                Downloader.fileFromUrl(url: invoice, completion: { (data) in
                    
                    mailComposerVC.addAttachmentData(data, mimeType: "application/pdf", fileName: "Invoice.pdf")
                })
                
                Downloader.fileFromUrl(url: csv) { (data) in
                    
                    DispatchQueue.main.async {
                        mailComposerVC.addAttachmentData(data, mimeType: "txt/text", fileName: "invoice.csv")
                    }
                }

            } else {
                // normal attach
                
                Downloader.fileFromUrl(url: invoice, completion: { (data) in
                    mailComposerVC.addAttachmentData(data, mimeType: "application/pdf", fileName: "Invoice.pdf")
                })
                
                Downloader.fileFromUrl(url: csv) { (data) in
                    mailComposerVC.addAttachmentData(data, mimeType: "txt/text", fileName: "invoice.csv")
                }

            }
            
            let loadingView = Loading.init(fromView: self)
            loadingView.createIndicator()
            
            loadingView.showIndicator()
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
                loadingView.hideIndicator()
                self.present(mailComposerVC, animated: true, completion: nil)
            }
            
            
        } else {
            print("Fail to send")
            
            Alerts.alert(on: self, title: "Order failed to complete", and: "Please go to 'Settings > Accounts & Passwords' and enable Mail in your accounts or 'Add Account' to add an Email account and return to the app to complete the order")
            //log this to firebase
        }
    }
    
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        let myOrder = orders[self.selectedCellIndexPath.row]
        
        guard let invoiceNum = myOrder.orderNum else {
            CLSLogv("%@", getVaList(["Order Number was not loaded"]))
            return
        }
        
        guard let pdfURL = myOrder.invoice else {
            CLSLogv("%@", getVaList(["PDF URL was not loaded"]))
            return
        }
        
        guard let csvURL = myOrder.csv else {
            CLSLogv("%@", getVaList(["csv URL was not loaded"]))
            return
        }
        
        let ID = UserFunctions.getNeolifeId()
        let currentDate = TimeDate().getDate()
        let currentTime = TimeDate().getTime()
        
        switch result {
        case .sent:
            controller.dismiss(animated: true) {
//                change isSent on Firebase to true here
//                SaveOrder.saveOrder(invoiceNum: invoiceNum, total: myOrder.total, time: currentTime, date: currentDate, isSent: true, neolifeID: ID, csv: csvURL, pdf: pdfURL)
                
                SaveOrder.saveOrder(withTotal: myOrder.total, time: currentTime, date: currentDate, isSent: true, neolifeID: ID, csv: csvURL, pdf: pdfURL, image: "")
                
                Alerts.alert(on: self, title: "Order Complete", and: "Please check your outbox in the Mail app to ensure your order has been sent.")
                
                self.loadOrders()
                print("Sent")
            }
            
        case .failed:
            
            controller.dismiss(animated: true) {
                Alerts.alert(on: self, title: "Order Completion Failed", and: "Is your email active?")
            }
            
            
        case .cancelled:
            Alerts.alert(on: self, title: "Cancelling", and: "You have cancelled completing your order")
            
        case .saved:
            controller.dismiss(animated: true) {
                //                change isSent on Firebase to true here
//                SaveOrder.saveOrder(invoiceNum: invoiceNum, total: myOrder.total, time: currentTime, date: currentDate, isSent: true, neolifeID: ID, csv: csvURL, pdf: pdfURL)
                
                SaveOrder.saveOrder(withTotal: myOrder.total, time: currentTime, date: currentDate, isSent: true, neolifeID: ID, csv: csvURL, pdf: pdfURL, image: "")
                self.loadOrders()
                Alerts.alert(on: self, title: "Order Saved in Drafts", and: "Your order is saved in drafts, please go to the Mail app > Drafts and send to the attached email link.")
            }
        }
    }
    
//    MARK: Cell Accessory Management
    
    func displayInfo(with message: String, from accessory: UIView) {
//        instantiate vc from storyboard
        guard let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "info") as? PopupInfoVC else {
            print("didnt work")
            return
        }
//        set content size
        vc.preferredContentSize = CGSize(width: accessory.bounds.width - 110, height: accessory.bounds.height + 40)
    
        //        set info
        vc.info = message
        vc.title = "Info"
        
//        root vc navigation controller
        let rootController = UINavigationController(rootViewController: vc)
        
//        set modal presentation style
        rootController.modalPresentationStyle = .popover
        
//        set self as delegate
        let popover = rootController.popoverPresentationController
        popover?.delegate = self
        popover?.sourceView = accessory
        popover?.sourceRect = CGRect(x: accessory.bounds.width - 75, y: accessory.bounds.height / 4 - 5, width: 50, height: 50)
        
//        present controller
        present(rootController, animated: true, completion: nil)
    }
    
    func helpInfoWith(_ message: String, button: UIBarButtonItem) {
        //        instantiate vc from storyboard
        guard let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "info") as? PopupInfoVC else {
            print("didnt work")
            return
        }
        
        //        set content size
        vc.preferredContentSize = CGSize(width: view.frame.width / 1.2, height: view.frame.height / 4 + 40)
        
        //        set info
        vc.info = message
        vc.title = "What happens here?"
        
        //        root vc navigation controller
        let rootController = UINavigationController(rootViewController: vc)
        
        //        set modal presentation style
        rootController.modalPresentationStyle = .popover
        
        //        set self as delegate
        let popover = rootController.popoverPresentationController
        popover?.delegate = self
        popover?.barButtonItem = button
        popover?.backgroundColor = UIColor.greenNeolife()
        
        //        present controller
        present(rootController, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
//    MARK: - Image Picker Delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        
//        load table and index path of selected cell
        let myOrder = orders[self.selectedCellIndexPath.row]

        //put in did select media
        let invoiceNum = myOrder.orderNum
        let pdfUrl = myOrder.invoice
        let csvUrl = myOrder.csv
        let paymentType = myOrder.paymentType
        
        let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as! UIImage
        picker.dismiss(animated: true, completion: nil)
        
        sendOrderWith(invoiceNum!, pdfUrl!, csvUrl!, paymentType: paymentType!, andImage: image)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }

}


extension HistoryTVC: UINavigationControllerDelegate {
    func cameraActionSheet() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        let cameraAlert = UIAlertController(title: "Photo from?", message: "Choose where you getting your from", preferredStyle: .actionSheet)
        
        let library = UIAlertAction(title: "Photo Library", style: .default) { (action) in
            imagePicker.sourceType = .photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
        
        let camera = UIAlertAction(title: "Camera", style: .default) { (action) in
            imagePicker.sourceType = .camera
            self.present(imagePicker, animated: true, completion: nil)
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        cameraAlert.addAction(library)
        cameraAlert.addAction(camera)
        cameraAlert.addAction(cancel)
        cameraAlert.view.tintColor = UIColor.greenNeolife()
        self.present(cameraAlert, animated: true, completion: nil)
    }
}

















// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
