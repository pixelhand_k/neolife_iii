//
//  ShoppingBag+CoreDataProperties.swift
//  
//
//  Created by Karabo Moloi on 2019/09/12.
//
//

import Foundation
import CoreData


extension ShoppingBag {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ShoppingBag> {
        return NSFetchRequest<ShoppingBag>(entityName: "ShoppingBag")
    }

    @NSManaged public var bv: String?
    @NSManaged public var code: Int32
    @NSManaged public var image: NSData?
    @NSManaged public var imageUrl: String?
    @NSManaged public var name: String?
    @NSManaged public var numItems: String?
    @NSManaged public var price: String?
    @NSManaged public var pv: String?
    @NSManaged public var total: String?
    @NSManaged public var type: String?

}
