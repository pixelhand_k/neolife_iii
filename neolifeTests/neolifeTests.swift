//
//  neolifeTests.swift
//  neolifeTests
//
//  Created by Karabo Moloi on 2018/08/21.
//  Copyright © 2018 Karabo Moloi. All rights reserved.
//

import XCTest
import CoreData
//@testable import neolife

class neolifeTests: XCTestCase {
    
    /*var shoppingBagManager : ShoppingBagManager!
    
    let url = "https://www.google.co.za/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png"
    let bv = "1"
    let pv = "30"
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        addItems()
        shoppingBagManager = ShoppingBagManager(container: self.mockPersistantContainer)
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        flushData()
        super.tearDown()
    }
    
    func testAddToCart() {
        let addToCart = shoppingBagManager.saveToCart(1004, "Item 4", "Case", "2", "200", url, itemPrice: "100", bv, pv)
        
        XCTAssertNil(addToCart)
    }
    
    
    
    //MARK: mock in-memory persistant store
    lazy var managedObjectModel: NSManagedObjectModel = {
        let managedObjectModel = NSManagedObjectModel.mergedModel(from: [Bundle(for: type(of: self))] )!
        return managedObjectModel
    }()
    
    lazy var mockPersistantContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "neolife", managedObjectModel: self.managedObjectModel)
        let description = NSPersistentStoreDescription()
        description.type = NSInMemoryStoreType
        description.shouldAddStoreAsynchronously = false // Make it simpler in test env
        
        container.persistentStoreDescriptions = [description]
        container.loadPersistentStores { (description, error) in
            // Check if the data store is in memory
            precondition( description.type == NSInMemoryStoreType )
            
            // Check if creating container wrong
            if let error = error {
                fatalError("Create an in-mem coordinator failed \(error)")
            }
        }
        return container
    }()*/
    
}

/*
extension neolifeTests {
    func addItems() {
        func addToCart(_ code: Int, _ name: String, _ type: String?, _ quantity: String, _ total: String, _ imageUrl: String, itemPrice: String, _ bvPoints: String, _ pvPoints: String ) -> ShoppingBag? {
            
            guard let shoppingBagItem = NSEntityDescription.insertNewObject(forEntityName: "ShoppingBag", into: mockPersistantContainer.viewContext) as? ShoppingBag else {
                return nil
            }
            
            shoppingBagItem.code = Int32(code)
            shoppingBagItem.name = name
            shoppingBagItem.numItems = quantity
            shoppingBagItem.type = type
            shoppingBagItem.price = itemPrice
            shoppingBagItem.total = total
            shoppingBagItem.bv = bvPoints
            shoppingBagItem.pv = pvPoints
            shoppingBagItem.imageUrl = imageUrl
            
            return shoppingBagItem
        }
        
        
        _ = addToCart(1001, "Item 1", "Single", "2", "400", url, itemPrice: "200", bv, pv)
        _ = addToCart(1002, "Item 2", "Single", "2", "800", url, itemPrice: "400", bv, pv)
        _ = addToCart(1003, "Item 3", "Case", "3", "600", url, itemPrice: "200", bv, pv)
        
        do {
            try self.mockPersistantContainer.viewContext.save()
        } catch {
            print("create fakes error: \(error.localizedDescription)")
        }
    }
    
    func flushData() {
        
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest<NSFetchRequestResult>(entityName: "ShoppingBag")
        let objs = try! self.mockPersistantContainer.viewContext.fetch(fetchRequest)
        for case let obj as NSManagedObject in objs {
            self.mockPersistantContainer.viewContext.delete(obj)
        }
        
        try! self.mockPersistantContainer.viewContext.save()
    }
}*/

